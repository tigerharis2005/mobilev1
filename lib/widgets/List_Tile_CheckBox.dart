import 'package:flutter/material.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/small_text.dart';

class ListTileWithCheckBox extends StatelessWidget {
  const ListTileWithCheckBox({
    super.key,
    required this.tileColor,
    required this.title,
    required this.checkbox,
  });

  final Color tileColor;
  final String title;
  final Checkbox checkbox;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 6),
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: Colors.black.withOpacity(0.2), width: 0.5),
        ),
        elevation: 2,
        child: ListTile(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(color: Colors.black.withOpacity(0.2), width: 0.5),
          ),
          dense: true,
          enableFeedback: true,
          minVerticalPadding: 10,
          tileColor: tileColor,
          leading: Transform.scale(
            scale: 1.2,
            child: CheckboxTheme(
              data: CheckboxThemeData(
                splashRadius: 5.0,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                side: const BorderSide(color: Colors.black, width: 2),
                checkColor: MaterialStateProperty.all(AppColors.white),
                fillColor:
                    MaterialStateProperty.all(AppColors.kButtonBackgroundColor),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              child: checkbox,
            ),
          ),
          title: Text(
            title,
            style: Theme.of(context).textTheme.displaySmall!.copyWith(
                  fontWeight: FontWeight.normal,
                ),
          ),
        ),
      ),
    );
  }
}
