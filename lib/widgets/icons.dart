import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobilev1/utils/colors.dart';

class MyIcon extends StatelessWidget {
  const MyIcon({
    super.key,
    required this.icon,
    required this.onPressed,
    required this.backgroundColor,
    this.iconColor = Colors.black,
  });

  final IconData? icon;
  final Color iconColor;
  final Color backgroundColor;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      decoration: BoxDecoration(
        color: backgroundColor,
        shape: BoxShape.circle,
        border: Border.fromBorderSide(
          BorderSide(
            color: Colors.black.withOpacity(0.1),
          ),
        ),
        boxShadow: const [
          BoxShadow(
            color: AppColors.kIconBackgroundColor,
            blurRadius: 5,
            offset: Offset(0, 5),
          ),
        ],
      ),
      child: Center(
        child: IconButton(
          iconSize: 22,
          onPressed: onPressed,
          icon: FaIcon(icon),
          color: iconColor,
        ),
      ),
    );
  }
}
