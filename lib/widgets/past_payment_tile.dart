import 'package:flutter/material.dart';
import 'package:mobilev1/widgets/small_text.dart';

class PastPaymentTile extends StatelessWidget {
  const PastPaymentTile({
    super.key,
    required this.date,
    required this.amount,
    required this.paidVia,
    required this.id,
  });

  final String date;
  final String amount;
  final String paidVia;
  final String id;

  static final List<DataColumn> _columns = [
    DataColumn(
      label: SmallText(text: 'Date'),
    ),
    DataColumn(
      label: SmallText(text: 'Amount'),
    ),
    DataColumn(
      label: SmallText(text: 'Paid Via'),
    ),
    DataColumn(
      label: SmallText(text: 'ID'),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        child: DataTable(
          border: TableBorder.all(
              color: Colors.grey.shade300,
              style: BorderStyle.solid,
              borderRadius: BorderRadius.circular(12)),
          columns: _columns,
          rows: [
            DataRow(
              cells: [
                DataCell(SmallText(text: date)),
                DataCell(SmallText(text: amount)),
                DataCell(SmallText(text: paidVia)),
                DataCell(SmallText(text: id)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
