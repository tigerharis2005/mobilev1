import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/small_text.dart';

class MyTextField extends StatelessWidget {
  MyTextField({
    super.key,
    this.labelText = "",
    this.hintText = "",
    required this.inputFormatters,
    required this.controller,
    this.height = 0,
    this.width = 0,
    this.keyboardType = TextInputType.text,
    this.maxLines = 1,
    this.prefixIcon,
    this.suffixIcon,
    this.obscureText = false,
    this.readOnly = false,
  });

  String labelText;
  String hintText;
  int maxLines = 1;
  TextInputType keyboardType = TextInputType.text;
  final List<TextInputFormatter> inputFormatters;
  final TextEditingController controller;
  double height = 0;
  double width = 0;
  Widget? prefixIcon;
  Widget? suffixIcon;
  bool readOnly;
  bool obscureText;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height == 0.0 ? Get.height * 0.06 : height,
      width: width == 0 ? Get.width / 2.5 : width,
      child: TextField(
        textAlign: TextAlign.justify,
        keyboardType: keyboardType,
        cursorColor: Colors.black,
        controller: controller,
        inputFormatters: inputFormatters,
        readOnly: readOnly,
        maxLines: maxLines,
        obscureText: obscureText,
        decoration: InputDecoration(
          label: labelText.isEmpty
              ? null
              : Text(
                  labelText,
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Colors.grey.shade600,
                      ),
                ),
          alignLabelWithHint: true,
          hintText: hintText,
          hintStyle: Theme.of(context).textTheme.bodyMedium!.copyWith(
                color: Colors.grey.shade600,
              ),
          prefixIcon: prefixIcon != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 12),
                  child: prefixIcon,
                )
              : null,
          suffixIcon: suffixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: const BorderSide(
              color: Colors.black,
              width: 2,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: const BorderSide(
              color: AppColors.kButtonBackgroundColor,
              width: 2,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Colors.grey.shade500,
            ),
          ),
        ),
      ),
    );
  }
}
