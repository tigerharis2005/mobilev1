import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';

class InvoiceItemTile extends StatelessWidget {
  const InvoiceItemTile({
    super.key,
    required this.itemName,
    required this.price,
    required this.quantity,
    required this.total,
  });

  final String itemName;
  final double price;
  final double quantity;
  final double total;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Wrap(
        children: [
          //* item name
          Container(
            width: Get.width * 0.28,
            height: Get.height * 0.05,
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                itemName,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: Dimensions.font15,
                  fontFamily: "Whitney",
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ),

          SizedBox(width: Dimensions.width5),

          //* item cost
          Container(
            width: Get.width * 0.16,
            height: Get.height * 0.05,
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text(
                price.toString(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: Dimensions.font15,
                  fontFamily: "Whitney",
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ),

          SizedBox(width: Dimensions.width5),

          //* item quantity
          Container(
            width: Get.width * 0.12,
            height: Get.height * 0.05,
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text(
                quantity.toString(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: Dimensions.font15,
                  fontFamily: "Whitney",
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ),

          SizedBox(width: Dimensions.width5),

          //* item total
          Container(
            width: Get.width * 0.16,
            height: Get.height * 0.05,
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text(
                total.toString(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: Dimensions.font15,
                  fontFamily: "Whitney",
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ),

          //* 3 do
          CircleAvatar(
            radius: 15,
            backgroundColor: AppColors.white,
            child: Center(
              child: IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.more_vert,
                  color: AppColors.black,
                  size: 20,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
