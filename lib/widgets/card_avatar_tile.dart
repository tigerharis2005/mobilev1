import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/small_text.dart';

Widget cardAvatarTile({
  required String text,
  required IconData icon,
  required Color iconColor,
}) {
  return Card(
    margin: const EdgeInsets.symmetric(horizontal: 2),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
      side: BorderSide(color: Colors.black.withOpacity(0.2), width: 0.5),
    ),
    elevation: 5.0,
    child: ListTile(
      title: Flexible(
        child: SmallText(
          text: text,
          color: Colors.black,
          size: Dimensions.font15,
        ),
      ),
      leading: CircleAvatar(
          radius: 17,
          backgroundColor: Colors.green,
          child: FaIcon(
            icon,
            size: 18,
            color: iconColor,
          )),
    ),
  );
}
