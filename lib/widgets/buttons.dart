import 'package:flutter/material.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';

class MyButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Size buttonSize;
  final TextStyle? textStyle;
  // double? width;
  // double? height;
  // double? radius;
  // Color? color;
  // double? textSize;
  // Color? textColor;
  // BoxBorder? boxBorder;
  // EdgeInsetsGeometry? margin;

  const MyButton({
    super.key,
    required this.text,
    required this.onPressed,
    this.textStyle,
    this.buttonSize = const Size(0, 0),
    // this.width,
    // this.height,
    // this.radius = 15,
    // this.color,
    // this.textSize,
    // this.textColor,
    // this.boxBorder,
    // this.margin,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        minimumSize: buttonSize == const Size(0, 0)
            ? Size(Dimensions.screenWidth * 0.9, Dimensions.screenHeight * 0.06)
            : buttonSize,
        elevation: 2,
        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        splashFactory: NoSplash.splashFactory,
        backgroundColor: AppColors.kButtonBackgroundColor,
        enableFeedback: true,
        textStyle: Theme.of(context).textTheme.bodyMedium!.copyWith(
              color: AppColors.white,
              fontWeight: FontWeight.w600,
            ),
      ),
      onPressed: onPressed,
      child: Text(
        text,
        style: textStyle,
      ),
    );
  }
}
