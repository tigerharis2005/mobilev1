import 'package:flutter/material.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/small_text.dart';

class CostServiceTile extends StatelessWidget {
  const CostServiceTile({
    super.key,
    required this.title,
    required this.subtitle,
    required this.price,
    required this.color
    
  });

  final String title;
  final String subtitle;
  final int price;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return CardView(
      cardColor: color,
        child: ListTile(
      title: SmallText(
        text: title,
        fontWeight: FontWeight.w700,
        color: AppColors.black,
      ),
      subtitle: SmallText(
        text: subtitle,
        fontWeight: FontWeight.w400,
        color: AppColors.grey,
      ),
      trailing: SmallText(
        text: '\$${price.toString()}',
        fontWeight: FontWeight.w700,
        color: AppColors.black,
      ),
    ));
  }
}
