// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import '../utils/colors.dart';

class DropDown extends StatelessWidget {
  DropDown(
      {required this.name,
      required this.value,
      required this.items,
      required this.onChanged,
      this.width,
      this.height,
      super.key});
  String name;
  Object value;
  double? width;
  double? height = 50;
  List<DropdownMenuItem<Object>> items;
  void Function(Object?) onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.maxFinite,
      height: height,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border:
              Border.all(color: AppColors.black.withOpacity(0.4), width: 1)),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
            borderRadius: BorderRadius.circular(4),
            value: value,
            icon: const Icon(Icons.keyboard_arrow_down),
            items: items,
            onChanged: onChanged),
      ),
    );
  }
}
