// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';

import '../utils/colors.dart';

class SmallText extends StatelessWidget {
  Color? color;
  final String text;
  double size;
  double height;
  FontWeight fontWeight;

  SmallText({
    Key? key,
    this.color = AppColors.black,
    required this.text,
    this.fontWeight = FontWeight.normal,
    this.height = 1.2,
    this.size = 12,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Whitney',
        color: color,
        fontSize: size,
        height: height,
        fontWeight:
            fontWeight == FontWeight.normal ? FontWeight.normal : fontWeight,
      ),
    );
  }
}
