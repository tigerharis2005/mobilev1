import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/small_text.dart';

class ListTileActions extends StatelessWidget {
  const ListTileActions({
    super.key,
    required this.tileColor,
    required this.dateTime,
    required this.acts,
    required this.price,
    this.editClicked,
    this.deletClicked,
  });

  final Color tileColor;
  final String dateTime;
  final String acts;
  final String price;
  final VoidCallback? editClicked;
  final VoidCallback? deletClicked;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2.0, vertical: 5),
      child: CardView(
        cardColor: tileColor,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          SizedBox(
            width: Dimensions.width15,
          ),
          SmallText(text: "$dateTime ||", color: Colors.black, size: 14),
          SmallText(text: " $acts || ", color: Colors.black, size: 14),
          SmallText(text: price, color: Colors.black, size: 14),
          // const Spacer(),
          // IconButton(
          //   onPressed: editClicked,
          //   icon: const Icon(
          //     Icons.edit,
          //     color: Colors.green,
          //   ),
          // ),
          // IconButton(
          //   onPressed: deletClicked,
          //   icon: const Icon(
          //     Icons.delete,
          //     color: Colors.red,
          //   ),
          // ),
        ]),
      ),
    );
  }
}
