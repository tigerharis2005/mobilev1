import 'package:flutter/material.dart';

class CardView extends StatelessWidget {
  CardView({
    super.key,
    this.margin = 0,
    this.cardColor = Colors.white,
    this.vertMargin = 5,
    required this.child,
  });

  final Widget child;
  double margin = 0;
  double vertMargin;
  Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: margin == 0
          ? const EdgeInsets.symmetric(horizontal: 2)
          : EdgeInsets.symmetric(horizontal: margin, vertical: vertMargin),
      elevation: 3,
      color: cardColor,
      shadowColor: const Color(0xFFA7A9AF),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
        side: BorderSide(
          color: Colors.black.withOpacity(0.3),
          width: 0.2,
        ),
      ),
      child: child,
    );
  }
}
