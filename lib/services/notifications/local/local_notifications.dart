import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

class LocalNotifications {
  //* basic notification
  static basicNotification() {
    AwesomeNotifications().createNotification(
        content: NotificationContent(
      id: 10,
      channelKey: "local_channel",
      title: "Basic Notification",
      body:
          "This is a basic notification || This is Test Notification|| Congratualtions ",
      notificationLayout: NotificationLayout.BigText,
    ));
  }

  //* Cancel Schedule Notification
  static cancelScheduleNotification(int id) async {
    await AwesomeNotifications().cancelSchedule(id);
  }
}
