import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

class NotificationController extends ChangeNotifier {
  // Singleton Patttern
  static final NotificationController _instance =
      NotificationController._internal();

  factory NotificationController() {
    return _instance;
  }

  NotificationController._internal();

  //* initialise the notification
  static Future<void> initialiseLocalNotification({required bool debug}) async {
    await AwesomeNotifications().initialize(
      null,
      [
        //* Basic Normal Notifications
        NotificationChannel(
          channelKey: 'local_channel',
          channelName: 'Basic notifications',
          channelDescription: 'Notification channel for basic tests',
          importance: NotificationImportance.Max,
          defaultPrivacy: NotificationPrivacy.Secret,
          playSound: true,
          channelShowBadge: true,
          enableLights: true,
          enableVibration: true,
        ),
      ],
      debug: debug,
    );
  }

  //* Event Listeners
  static Future<void> intialiseNotificationEventListerners() async {
    await AwesomeNotifications().setListeners(
      onActionReceivedMethod: NotificationController.onActionReceivedMethod,
      onNotificationCreatedMethod:
          NotificationController.onNotifcationCreatedMethod,
      onNotificationDisplayedMethod:
          NotificationController.onNotificationDisplayedMethod,
      onDismissActionReceivedMethod:
          NotificationController.onDismissActionReceivedMethod,
    );
  }

  // on action receieved notification
  static Future<void> onActionReceivedMethod(
      ReceivedAction receivedAction) async {
    bool isSilentAction =
        receivedAction.actionType == ActionType.SilentAction ||
            receivedAction.actionType == ActionType.SilentBackgroundAction;

    debugPrint(
        '${isSilentAction ? "Silent Action" : "Action"} Notification Received');

    // check details of recevied action
    debugPrint("receivedAction: ${receivedAction.toString()}");

    if (receivedAction.buttonKeyPressed == "SUBSCRIBE") {
      debugPrint("Subscribe Button Pressed");
    } else if (receivedAction.buttonKeyPressed == "DISMISS") {
      debugPrint("Unsubscribe Button Pressed");
    }
  }

  // dismissed action
  static Future<void> onDismissActionReceivedMethod(
      ReceivedAction receivedAction) async {
    debugPrint("Notification Dismissed");
  }

  // on creatting new notification
  static Future<void> onNotifcationCreatedMethod(
      ReceivedNotification receivedAction) async {
    debugPrint("Notifiation Created");
  }

  // on displaying new notification
  static Future<void> onNotificationDisplayedMethod(
      ReceivedNotification receivedAction) async {
    debugPrint("Notification Displayed");
  }
}
