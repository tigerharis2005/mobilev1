import 'package:get/get.dart';

import '../Controller/network_controller.dart';

class NetworkBinder implements Bindings {
  @override
  void dependencies() {
    Get.put<NetworkController>(NetworkController(), permanent: true);
  }
}
