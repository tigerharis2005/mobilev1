import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NetworkController extends GetxController {
  final Connectivity _connectivity = Connectivity();

  @override
  void onInit() {
    super.onInit();
    _connectivity.onConnectivityChanged.listen(checkConnection);
  }

  void checkConnection(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      Get.snackbar(
        'No Internet Connection',
        'Please check your internet connection',
        duration: const Duration(seconds: 4),
        isDismissible: true,
        snackPosition: SnackPosition.BOTTOM,
        icon: const Icon(Icons.wifi_off, color: Colors.white),
        backgroundColor: Colors.red.withOpacity(0.5),
        snackStyle: SnackStyle.FLOATING,
        colorText: Colors.white,
      );
    } else {
      if (Get.isSnackbarOpen) {
        Get.closeCurrentSnackbar();
      }
    }
  }
}
