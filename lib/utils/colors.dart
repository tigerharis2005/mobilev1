import 'package:flutter/material.dart';

class AppColors {
  static const white = Colors.white;
  static const black = Colors.black;
  static const orange = Color(0xFFFF7F30);
  static const lightGrey = Color(0xFFD5D4D4);
  static const grey = Color(0xFF747373);
  static const deepGreen = Color(0xFF205F20);
  static const blue = Colors.blue;

  static const pink = Color(0xFFFF3D62);

  static const kScaffoldBackgroundColor = Color(0xFFF2F5FC);
  static const kIconBackgroundColor = Color(0xFFDDDDDD);
  static const kButtonBackgroundColor = Color(0xFF183150);
  static const kDarkBackgroundColor = Color(0xFF141E1D);
}
