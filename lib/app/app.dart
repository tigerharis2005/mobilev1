import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/homepageview.dart';
import '../routes/routes.dart';
import '../utils/theme.dart';
import 'modules/Auth-Screens/loginpage/view/loginpageview.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // GetStorage box = GetStorage();
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      theme: MyTheme.lightTheme(context),
      getPages: AppRoutes.pages,
      // initialRoute: LoginPage.route,
      initialRoute: GetStorage().read('userId')!=null? HomePage.route:LoginPage.route,
    );
  }
}
