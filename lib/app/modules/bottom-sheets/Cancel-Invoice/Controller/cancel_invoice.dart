import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/get_appointment_by_notary_id_model.dart';
import 'package:mobilev1/app/data/repo/get_appointment_by_notary_repo.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/repo_response/repo_response.dart';

class CancelInvoiceController extends GetxController {
  final TextEditingController cancelInvoiceTextController =
      TextEditingController();
      var appointment;
  init(){
    appointment=GetStorage().read('selectedApt');
    update();
  }

  @override
  void dispose() {
    cancelInvoiceTextController.dispose();
    super.dispose();
  }

  @override
  void onClose() {
    cancelInvoiceTextController.dispose();
  }

  void cancelInvoice() async{
    LoadingUtils.showLoader();
    AppointmentRepository repo=AppointmentRepository();
    CancelAnAppointment data=CancelAnAppointment(aptId: appointment['_id'],notaryId: GetStorage().read('userId'),doesNeedCancellationInvoice: true,amount: int.parse(cancelInvoiceTextController.text));
    RepoResponse response = await repo.cancelAnAppointment(data);
    LoadingUtils.hideLoader();
    if (response.data!=null) {
      print('Updated successfully');
    }else{
      // print(response.error!.message);
    }
    

  }
}
