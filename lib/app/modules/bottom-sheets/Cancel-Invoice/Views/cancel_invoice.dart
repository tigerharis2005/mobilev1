import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Cancel-Invoice/Controller/cancel_invoice.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/my_text_field.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../widgets/button.dart';

class CancelInvoice {
  static Future<dynamic> openCancelInvoiceBottomSheet() {
    final CancelInvoiceController cancelInvoiceController =
        Get.put(CancelInvoiceController());
    cancelInvoiceController.init();
    return Get.bottomSheet(
      elevation: 10,
      backgroundColor: Colors.white,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      SizedBox(
        height: Dimensions.height10 * 40,
        child: SingleChildScrollView(
            child: Container(
          margin: const EdgeInsets.all(15),
          child: Column(
            children: [
              //* logo with Title & Sub-title
              Align(
                alignment: Alignment.center,
                child: SmallText(
                  text: "Cancel With Invoice",
                  size: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),

              SizedBox(
                height: Dimensions.height10,
              ),

              CardView(
                margin: 10,
                child: Column(
                  children: [
                    //* Cancel text and field
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          //* Cancel Text
                          Align(
                            alignment: Alignment.centerLeft,
                            child: SmallText(
                              text: "Cancel",
                              size: 16,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),

                          //* Cancel Amount
                          MyTextField(
                            width: Get.width * 0.8,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]')),
                            ],
                            controller: cancelInvoiceController
                                .cancelInvoiceTextController,
                            labelText: "Enter Cancel Amount",
                            keyboardType: TextInputType.number,
                          ),

                          SizedBox(
                            height: Dimensions.height20,
                          ),

                          Align(
                            alignment: Alignment.centerLeft,
                            child: Flexible(
                              child: SmallText(
                                text:
                                    "Email will be sent to (signer) or (customer), with cancellation invoice.",
                                size: 13,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Flexible(
                              child: SmallText(
                                text:
                                    "You will need to update payment entry for this once received.",
                                size: 13,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.height10,
                          ),

                          Button(
                            width: Get.width * 0.6,
                            //height: Get.height * 0.025,
                            on_pressed: () {
                              cancelInvoiceController.cancelInvoice();
                            },
                            text: "Cancel Issue Invoice",
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
