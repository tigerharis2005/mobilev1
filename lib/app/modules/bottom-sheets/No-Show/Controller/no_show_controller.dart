import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NoShowController extends GetxController {
  final TextEditingController chargeTextController = TextEditingController();

  final TextEditingController newDateTextController = TextEditingController();

  final TextEditingController newTimeTextController = TextEditingController();

  Rx<DateTime> selectedDate =
      DateTime.parse(DateFormat("yyyydd-MM-dd").format(DateTime.now())).obs;

  void pickDate() async {
    final DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year, 1), // current year
      lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
    );
    if (pickedDate != null && pickedDate != DateTime.now()) {
      selectedDate.value =
          DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
    }
  }

  var selectedTime = TimeOfDay.now().obs;

  void selectTime() async {
    final TimeOfDay? pickedTime = await showTimePicker(
      context: Get.context!,
      initialTime: TimeOfDay.now(),
    );
    if (pickedTime != null && pickedTime != TimeOfDay.now()) {
      selectedTime.value = pickedTime;
    }
  }

  @override
  void dispose() {
    chargeTextController.dispose();
    newDateTextController.dispose();
    newTimeTextController.dispose();
    super.dispose();
  }

  @override
  void onClose() {
    chargeTextController.dispose();
    newDateTextController.dispose();
    newTimeTextController.dispose();
  }
}
