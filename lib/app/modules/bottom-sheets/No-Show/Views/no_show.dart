import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/button.dart';
import 'package:mobilev1/widgets/buttons.dart';
import 'package:mobilev1/widgets/my_text_field.dart';

import '../../../../../widgets/card.dart';
import '../../../../../widgets/small_text.dart';
import '../Controller/no_show_controller.dart';

class NoShow {
  //* open bottom sheet
  static Future<dynamic> openNoShowBottomSheet(BuildContext context) {
    final noShowController = Get.put(NoShowController());
    return Get.bottomSheet(
      elevation: 10,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      backgroundColor: AppColors.kScaffoldBackgroundColor,
      Container(
        padding: const EdgeInsets.all(8),
        child: SizedBox(
          height: Dimensions.height10 * 60,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: Dimensions.height10,
                ),
                Text(
                  "No Show & Reschedule ",
                  style: Theme.of(context).textTheme.displayLarge,
                ),
                SizedBox(
                  height: Dimensions.height10,
                ),
                CardView(
                  margin: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* charge field
                        MyTextField(
                          width: Get.width * 0.9,
                          keyboardType: TextInputType.number,
                          labelText: "Charges for No show",
                          hintText: "50",
                          inputFormatters: const [],
                          controller: noShowController.chargeTextController,
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* new date
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: SmallText(
                                  color: Colors.black,
                                  text: "Select Date",
                                  size: Dimensions.font18,
                                ),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height10,
                            ),

                            //* date
                            Wrap(
                              spacing: 10,
                              children: [
                                Obx(
                                  () => MyTextField(
                                    width: Get.width * 0.70,
                                    keyboardType: TextInputType.text,
                                    readOnly: true,
                                    hintText:
                                        "${noShowController.selectedDate.value.year.toString()} - ${noShowController.selectedDate.value.month.toString()} - ${noShowController.selectedDate.value.day.toString()}",
                                    inputFormatters: const [],
                                    controller:
                                        noShowController.newDateTextController,
                                  ),
                                ),
                                IconButton(
                                  iconSize: 22,
                                  onPressed: () => noShowController.pickDate(),
                                  icon: const Icon(Icons.calendar_today,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* new time
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: SmallText(
                                  color: Colors.black,
                                  text: "Select Time",
                                  size: Dimensions.font18,
                                ),
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height10,
                            ),

                            //* new time
                            Wrap(
                              spacing: 10,
                              children: [
                                Obx(
                                  () => MyTextField(
                                    width: Get.width * 0.70,
                                    keyboardType: TextInputType.text,
                                    readOnly: true,
                                    hintText:
                                        "${noShowController.selectedTime.value.hour.toString()} Hrs ${noShowController.selectedTime.value.minute.toString()} Mins",
                                    inputFormatters: const [],
                                    controller:
                                        noShowController.newTimeTextController,
                                  ),
                                ),
                                IconButton(
                                  iconSize: Dimensions.iconSize28,
                                  onPressed: () =>
                                      noShowController.selectTime(),
                                  icon: const Icon(
                                    Icons.schedule_rounded,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* notes
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Flexible(
                                child: Text(
                                  "No Show will appear as Line item in your invoice.",
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Flexible(
                                child: Text(
                                  "Email will be sent to signers of new Date and Time.",
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height20,
                        ),

                        MyButton(text: "Mark as No Show", onPressed: () {}),

                        SizedBox(
                          height: Dimensions.height10,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
