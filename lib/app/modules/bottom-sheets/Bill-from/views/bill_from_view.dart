import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/small_text.dart';

class BillFrom {
  //* open sheet
  static Future<dynamic> openBillFromBottomSheet() {
    return Get.bottomSheet(
      elevation: 10,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      backgroundColor: Colors.white,
      SizedBox(
        height: Dimensions.height10 * 60,
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: BigText(
                      text: "Send From",
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: CardView(
                    child: Row(
                      children: [
                        //* icon
                        const Icon(
                          Icons.location_on,
                          color: Colors.red,
                          size: 32,
                        ),

                        SizedBox(width: Dimensions.width20),

                        //* details
                        Wrap(
                          direction: Axis.vertical,
                          children: [
                            //* title
                            BigText(text: "Notarised Docs LLC"),

                            SizedBox(height: Dimensions.height10),

                            //* phone and email
                            Wrap(
                              alignment: WrapAlignment.start,
                              children: [
                                SmallText(text: "+1 (123) 456 7890 "),
                                SmallText(text: "|| "),
                                SmallText(text: "email@gmail.com "),
                              ],
                            ),
                            SizedBox(height: Dimensions.height10),

                            //* address
                            Flexible(
                              child: SmallText(
                                text:
                                    "123, Main Street, New York, NY 10030, USA",
                              ),
                            ),
                            SizedBox(height: Dimensions.height10),

                            //* tax code
                            Wrap(
                              alignment: WrapAlignment.start,
                              children: [
                                SmallText(text: "Tax Code: "),
                                SmallText(text: "123123123 "),
                              ],
                            ),
                            SizedBox(height: Dimensions.height10),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: Dimensions.height15),

                //* warning card
                CardView(
                  margin: 10,
                  cardColor: Colors.amber.shade100,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        SmallText(
                          text: "You can edit these details in profile section",
                        ),
                        const Spacer(),
                        TextButton(
                          onPressed: () {},
                          child: SmallText(
                            text: "Edit",
                            size: Dimensions.font18,
                            color: Colors.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
