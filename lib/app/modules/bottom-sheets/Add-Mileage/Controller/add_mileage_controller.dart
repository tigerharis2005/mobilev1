import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/mileage_model.dart';
import 'package:mobilev1/app/data/repo/mileage_repo.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/repo_response/repo_response.dart';

class AddMileageController extends GetxController {
  RxBool isMileageValid = false.obs;

  TextEditingController mileageTextController = TextEditingController();

  void updateMileage(bool value) {
    isMileageValid.value = value;
    markNotRequired(value);
  }
  var appointment;
  init(){
    appointment=GetStorage().read('selectedApt');
    update();
  }

  addMileage()async{
    LoadingUtils.showLoader();
    MileageRepository repo=MileageRepository();
    UpdateMileage data=UpdateMileage(notaryId: GetStorage().read('userId'),aptId: appointment['_id'],milesDriven: int.parse(mileageTextController.text),perMileCost: GetStorage().read('profileConfigs')['perMileCost'],dateAdded: 'UTC TimeStamp',mileageEntrySource: 'Mobile');
    RepoResponse response=await repo.updateMileage(data);
    if(response.data!=null){
      print('success');
    }
    LoadingUtils.hideLoader();
  }

  markNotRequired(bool x)async{
    LoadingUtils.showLoader();
    MileageRepository repo=MileageRepository();
    MarkMileageNotRequired data=MarkMileageNotRequired(notaryId: GetStorage().read('userId'),aptId: appointment['_id'],mileageEntrySource: 'mobile/web',isMileageEntryRequired: x);
    LoadingUtils.hideLoader();
    RepoResponse response=await repo.markMileageNotRequired(data);
    if(response.data!=null){
      print('success');
    }
    
  }
}
