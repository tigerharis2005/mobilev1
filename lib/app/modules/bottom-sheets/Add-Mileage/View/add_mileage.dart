import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/buttons.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/icons.dart';
import 'package:mobilev1/widgets/my_text_field.dart';

import '../../../../../widgets/List_Tile_CheckBox.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/small_text.dart';
import '../Controller/add_mileage_controller.dart';

final AddMileageController addMileageController =
    Get.put(AddMileageController());

class AddMileage {
  //* Add Mileage bottom sheet
  static Future<dynamic> addMileageBottomSheet(BuildContext context) {
    addMileageController.init();
    return Get.bottomSheet(
      isScrollControlled: true,
      elevation: 10,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      barrierColor: Colors.black.withOpacity(0.5),
      backgroundColor: AppColors.kScaffoldBackgroundColor,
      Container(
        margin: const EdgeInsets.all(15),
        child: SizedBox(
          height: Dimensions.height10 * 50,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: Dimensions.height15,
                ),

                //* logo with Title & Sub-title
                SizedBox(
                  width: Dimensions.width15,
                ),

                // todo add title
                Column(
                  children: [
                    Text(
                      "Add Mileage",
                      style: Theme.of(context).textTheme.displayLarge,
                    ),

                    SizedBox(
                      height: Dimensions.height10,
                    ),

                    // todo add sub-title
                    Text(
                      "You haven't added any expenses yet",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //todo input and udpate
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MyTextField(
                              width: Get.width * 0.50,
                              keyboardType: TextInputType.number,
                              labelText: "Mileage",
                              hintText: "0",
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]'),
                                ),
                              ],
                              controller:
                                  addMileageController.mileageTextController),
                          SizedBox(
                            width: Dimensions.height10,
                          ),
                          MyButton(
                            buttonSize: Size(Dimensions.screenWidth * 0.30,
                                Dimensions.screenHeight * 0.06),
                            text: "UPDATE",
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: Dimensions.height15,
                    ),

                    Divider(
                      indent: 10,
                      endIndent: 10,
                      color: Colors.grey.shade300,
                      thickness: 1,
                    ),

                    SizedBox(
                      width: Dimensions.height10,
                    ),

                    Row(
                      children: [
                        Obx(
                          () => SizedBox(
                            width: Get.width * 0.90,
                            child: ListTileWithCheckBox(
                              tileColor: Colors.white,
                              title: "I dont have mileage for this signing",
                              checkbox: Checkbox(
                                onChanged: (value) =>
                                    addMileageController.updateMileage(value!),
                                value:
                                    addMileageController.isMileageValid.value,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(
                      height: Dimensions.height15,
                    ),

                    //* info card section
                    CardView(
                      cardColor: Colors.green.shade50,
                      margin: 2,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 15),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                MyIcon(
                                  icon: FontAwesomeIcons.circleInfo,
                                  onPressed: () {},
                                  backgroundColor: Colors.green.shade800,
                                  iconColor: Colors.white,
                                ),
                                SizedBox(
                                  width: Dimensions.width10,
                                ),
                                Flexible(
                                  child: Text(
                                    "You can use our inApp Tracking technology to automatically track your distance travelled.",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: Dimensions.height20,
                            ),
                            Row(
                              children: [
                                MyIcon(
                                  icon: FontAwesomeIcons.circleInfo,
                                  onPressed: () {},
                                  backgroundColor: Colors.green.shade800,
                                  iconColor: Colors.white,
                                ),
                                SizedBox(
                                  width: Dimensions.width10,
                                ),
                                Flexible(
                                  child: Text(
                                    "Enable them in your dashboard before going to appointment.",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
