import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Bill-To/controller/bill_to_controller.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/my_text_field.dart';

class BillTo {
  //* open sheet
  static Future<dynamic> openBillToBottomSheet() {
    final BillToController billToController = Get.put(BillToController());
    return Get.bottomSheet(
      elevation: 10,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      backgroundColor: Colors.white,
      SizedBox(
        height: Dimensions.height10 * 60,
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: BigText(text: "Recipient Details"),
                  ),
                ),
                SizedBox(height: Dimensions.height20),
                CardView(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18.0),
                  child: Column(
                    children: [
                      SizedBox(height: Dimensions.height10),

                      //* name
                      MyTextField(
                        width: Get.width * 0.7,
                        inputFormatters: const [],
                        hintText: "Full Name",
                        labelText: "Name",
                        maxLines: 1,
                        keyboardType: TextInputType.name,
                        controller: billToController.fullNameController,
                      ),
                      SizedBox(height: Dimensions.height20),

                      //* email
                      MyTextField(
                        width: Get.width * 0.7,
                        inputFormatters: const [],
                        hintText: "Email",
                        labelText: "Email Address",
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        controller: billToController.emailController,
                      ),

                      SizedBox(height: Dimensions.height20),

                      //* phone
                      MyTextField(
                        width: Get.width * 0.7,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(10),
                        ],
                        hintText: "Phone",
                        labelText: "Phone Number",
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        controller: billToController.emailController,
                      ),
                      SizedBox(height: Dimensions.height20),

                      //* address
                      MyTextField(
                        height: Get.height * 0.1,
                        width: Get.width * 0.7,
                        inputFormatters: const [],
                        hintText: "Address",
                        labelText: "Street Address",
                        maxLines: 3,
                        keyboardType: TextInputType.streetAddress,
                        controller: billToController.addressController,
                      ),
                      SizedBox(height: Dimensions.height10),

                      //* save button
                      Button(
                        text: "Submit",
                        height: Get.height * 0.05,
                        on_pressed: () {
                          billToController.submitButton();
                        },
                      ),
                      SizedBox(height: Dimensions.height20),
                    ],
                  ),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
