import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BillToController extends GetxController {
  //* full name controller
  final TextEditingController fullNameController = TextEditingController();

  //* email controller
  final TextEditingController emailController = TextEditingController();

  //* phone controller
  final TextEditingController phoneController = TextEditingController();

  //* address controller
  final TextEditingController addressController = TextEditingController();

  void submitButton() {}

  @override
  void dispose() {
    fullNameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    addressController.dispose();
    super.dispose();
  }
}
