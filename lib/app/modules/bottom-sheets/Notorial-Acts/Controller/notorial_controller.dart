import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class NotorialActController extends GetxController {
  //* notorial acts text controller
  final TextEditingController notorialActsTextController =
      TextEditingController();

  //* cost text controller
  final TextEditingController costPerActTextController =
      TextEditingController();

  //* date text controller
  final TextEditingController timeTextController = TextEditingController();

  //* edit date dialog text controller
  final TextEditingController editTimeTextController = TextEditingController();

  Rx<DateTime> selectedDate =
      DateTime.parse(DateFormat("yyyy-MM-dd").format(DateTime.now())).obs;

  RxBool isCheckBoxSelected = false.obs;

  void updateCheckBox(bool value) {
    isCheckBoxSelected.value = value;
  }

  var appointment;
  init(){
    appointment=GetStorage().read('selectedApt');
    update();
  }

  void pickDate() async {
    final DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year, 1), // current year
      lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
    );
    if (pickedDate != null) {
      selectedDate.value =
          DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
    }
  }
}
