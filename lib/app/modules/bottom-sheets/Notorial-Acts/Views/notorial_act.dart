import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Edit-Notorial/edit_notorial.dart';
import 'package:mobilev1/widgets/List_Tile_CheckBox.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/list_tile_actions.dart';
import '../../../../../widgets/my_text_field.dart';
import '../../../../../widgets/small_text.dart';
import '../Controller/notorial_controller.dart';

class NotorialAct {
  //* open sheet
  static Future<dynamic> openNotorialBottomSheet() {
    final NotorialActController notorialController =
        Get.put(NotorialActController());
    notorialController.init();
    return Get.bottomSheet(
      elevation: 10,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      backgroundColor: Colors.white,
      SizedBox(
        height: Dimensions.height10 * 60,
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: Dimensions.height15,
                ),

                //* logo with Title & Sub-title
                Row(
                  children: [
                    // todo: add logo
                    CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.white,
                      child: Icon(
                        size: Dimensions.radius30,
                        Icons.add_circle,
                        color: Colors.black,
                      ),
                    ),

                    SizedBox(
                      width: Dimensions.width15,
                    ),

                    // todo -> title
                    Column(
                      children: [
                        BigText(
                          text: "Notorial Acts",
                          fontWeight: FontWeight.bold,
                          size: Dimensions.font26,
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        // todo -> sub-title
                        SmallText(
                          text: "You haven't added any Notorial Acts yet",
                          color: Colors.black,
                          size: Dimensions.font15,
                        )
                      ],
                    ),
                  ],
                ),

                SizedBox(
                  height: Dimensions.height15,
                ),

                CardView(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            //* 2 text fields
                            MyTextField(
                              height: Get.height * 0.05,
                              width: Get.width * 0.38,
                              controller:
                                  notorialController.notorialActsTextController,
                              labelText: "Notorial Acts",
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[0-9]')),
                              ],
                              keyboardType: TextInputType.number,
                              hintText: "0",
                            ),
                            const Spacer(),
                            MyTextField(
                              height: Get.height * 0.05,
                              width: Get.width * 0.38,
                              controller:
                                  notorialController.costPerActTextController,
                              labelText: "Cost Per Act",
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[0-9]')),
                              ],
                              keyboardType: TextInputType.number,
                              hintText: "0",
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height15,
                        ),

                        Obx(
                          () => MyTextField(
                            width: Get.width * 0.80,
                            height: Get.height * 0.06,
                            suffixIcon: IconButton(
                              iconSize: 20,
                              onPressed: () => notorialController.pickDate(),
                              icon: const Icon(Icons.calendar_month_rounded,
                                  color: Colors.black),
                            ),
                            keyboardType: TextInputType.text,
                            labelText: "Schedule Date",
                            hintText:
                                "${notorialController.selectedDate.value.year.toString()} - ${notorialController.selectedDate.value.month.toString()} - ${notorialController.selectedDate.value.day.toString()}",
                            inputFormatters: const [],
                            controller: notorialController.timeTextController,
                          ),
                        ),

                        SizedBox(
                          height: Dimensions.height15,
                        ),

                        //* button
                        Button(
                          width: Dimensions.screenWidth * 0.45,
                          height: Dimensions.screenHeight * 0.05,
                          on_pressed: () {},
                          text: "Add",
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                Obx(
                  () => ListTileWithCheckBox(
                    tileColor: Colors.white,
                    title: "I do not have any acts for this signing",
                    checkbox: Checkbox(
                      value: notorialController.isCheckBoxSelected.value,
                      onChanged: (value) =>
                          notorialController.updateCheckBox(value!),
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: BigText(
                      text: "Previouslly Added Acts",
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      size: Dimensions.font20,
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height15,
                ),

                GetBuilder<NotorialActController>(
                  builder: (_) {
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: notorialController
                          .appointment['notarialActsHistory'].length,
                      itemBuilder: (context, index) {
                        return ListTileActions(
                          tileColor: Colors.white,
                          dateTime: "2023-12-12",
                          acts: "10",
                          price: "100",
                          editClicked: () =>
                              EditNotorial.openEditNotorialActDialogBox(
                                  notorialActs: 3, price: 5),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
