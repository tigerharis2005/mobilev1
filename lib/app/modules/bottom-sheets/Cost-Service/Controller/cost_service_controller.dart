import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/get_appointment_by_notary_id_model.dart';
import 'package:mobilev1/app/data/repo/get_appointment_by_notary_repo.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/repo_response/repo_response.dart';

class CostServiceController extends GetxController {
  int selectedServiceIndex = 0;
  var serviceData;
  selectService(int x) {
    selectedServiceIndex = x;
    update();
  }

  var appointment;
  init(){
    appointment=GetStorage().read('selectedApt');
    update();
  }

  List availableServices = [];
  @override
  void onInit() {
    super.onInit();
    // print(GetStorage().read('availableServices'));
    serviceData = GetStorage().read('availableServices');
    availableServices = GetStorage().read('availableServices')['services'];
    update();
  }

  //* cost text controller
  TextEditingController changeCostTextController = TextEditingController();

  //* search service text controller
  TextEditingController searchServiceTextController = TextEditingController();

  void updateCost() async{
    LoadingUtils.showLoader();
    AppointmentRepository repo=AppointmentRepository();
    ChangeServiceCost data=ChangeServiceCost(aptId: appointment['_id'],notaryId: serviceData['notaryId'],serviceIdDB: 'serviceId',newCost: changeCostTextController.text);
    RepoResponse response = await repo.changeServiceCost(data);
    LoadingUtils.hideLoader();
    if (response.data!=null) {
      print('Updated successfully');
    }else{
      // print(response.error!.message);
    }
    

  }

  void changeService() async {
    LoadingUtils.showLoader();
    AppointmentRepository repo = AppointmentRepository();
    ChangeServiceOfApt data = ChangeServiceOfApt(
        notaryId: serviceData['notaryId'],
        aptId: appointment['_id'],
        service: Service(
            sId: availableServices[selectedServiceIndex]['_id'],
            serviceName: availableServices[selectedServiceIndex]['serviceName'],
            serviceId: availableServices[selectedServiceIndex]['serviceId'],
            description: availableServices[selectedServiceIndex]['description'],
            cost: availableServices[selectedServiceIndex]['cost'].toString()));
    RepoResponse response = await repo.changeServiceOfApt(data);
    LoadingUtils.hideLoader();
    if (response.data['status'] == 1) {
      print('Updated successfully');
    }else{
      print(response.error!.message);
    }
    
  }

  @override
  void dispose() {
    changeCostTextController.dispose();
    searchServiceTextController.dispose();
    super.dispose();
  }
}
