import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/colors.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/cost_service_tile.dart';
import '../../../../../widgets/my_text_field.dart';
import '../../../../../widgets/small_text.dart';
import '../Controller/cost_service_controller.dart';

class ChangeServiceBottomSheet extends StatelessWidget {
  const ChangeServiceBottomSheet({
    super.key,
    required CostServiceController costServiceController,
  }) : _costServiceController = costServiceController;

  final CostServiceController _costServiceController;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Dimensions.height10 * 60, //600 pixels
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              //* title
              BigText(
                text: "Change Service",
                color: AppColors.black,
                fontWeight: FontWeight.bold,
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* current service
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SmallText(
                      text: "Current Service:",
                      color: AppColors.black,
                      size: Dimensions.font15,
                      fontWeight: FontWeight.w600),
                  SmallText(
                      text: " Seller",
                      color: AppColors.black,
                      size: Dimensions.font18,
                      fontWeight: FontWeight.bold),
                ],
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* cost text field with button
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyTextField(
                        width: Get.width * 0.35,
                        height: Get.height * 0.05,
                        labelText: "Cost",
                        hintText: "Enter Cost",
                        inputFormatters: const [],
                        controller:
                            _costServiceController.changeCostTextController),
                    const Spacer(),
                    Button(
                      width: Get.width * 0.25,
                      height: Get.height * 0.05,
                      on_pressed: () {},
                      text: "Update",
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* divider line
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: [
                    const Expanded(child: Divider(color: AppColors.black)),
                    SmallText(
                        text: " Or Replace Service ",
                        color: AppColors.black,
                        size: Dimensions.font15,
                        fontWeight: FontWeight.w600),
                    const Expanded(child: Divider(color: AppColors.black)),
                  ],
                ),
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* search service
              MyTextField(
                  width: Get.width * 0.80,
                  height: Get.height * 0.05,
                  labelText: "Search Service",
                  prefixIcon: const Icon(Icons.search, color: AppColors.black),
                  hintText: "Search",
                  inputFormatters: const [],
                  controller:
                      _costServiceController.searchServiceTextController),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* service list
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _costServiceController.availableServices.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: GestureDetector(
                      onTap: () {
                        _costServiceController.selectService(index);
                      },
                      child: GetBuilder<CostServiceController>(
                        builder: (_) {
                          return CostServiceTile(
                            title: _costServiceController
                                .availableServices[index]['serviceName'],
                            subtitle: _costServiceController
                                .availableServices[index]['serviceId'],
                            price: _costServiceController
                                .availableServices[index]['cost'],
                            color:
                                _costServiceController.selectedServiceIndex ==
                                        index
                                    ? const Color.fromARGB(255, 144, 166, 255)
                                    : Colors.white,
                          );
                        },
                      ),
                    ),
                  );
                },
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* add service button
              Button(
                width: Get.width * 0.25,
                height: Get.height * 0.05,
                on_pressed: () {
                  _costServiceController.changeService();
                },
                text: "Change",
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
