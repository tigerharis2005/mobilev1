// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Cost-Service/Controller/cost_service_controller.dart';
import 'package:mobilev1/utils/colors.dart';

import 'change_service_bottomSheetWidget.dart';

class CostService {
  static Future<void> openCostServiceBottomSheet() {
    final CostServiceController _costServiceController =
        Get.put(CostServiceController());
    _costServiceController.init();
    return Get.bottomSheet(
      isScrollControlled: true,
      elevation: 10,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      barrierColor: Colors.black.withOpacity(0.80),
      backgroundColor: AppColors.kScaffoldBackgroundColor,
      ChangeServiceBottomSheet(costServiceController: _costServiceController),
    );
  }
}
