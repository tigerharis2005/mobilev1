import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/buttons.dart';

import '../../../../../widgets/List_Tile_CheckBox.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/card_avatar_tile.dart';
import '../Controller/mark_complete_controller.dart';

class MarkComplete {
//* open bottom sheet
  static Future<dynamic> markCompleteBottomSheet(BuildContext context) {
    final MarkCompleteController markCompleteController =
        Get.put(MarkCompleteController());

    return Get.bottomSheet(
      isScrollControlled: true,
      elevation: 10,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      barrierColor: Colors.black.withOpacity(0.80),
      backgroundColor: Colors.white,
      SizedBox(
        height: Dimensions.height10 * 64,
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              children: [
                const SizedBox(height: 10),
                //todo title
                Text(
                  "Mark As Complete ?",
                  style: Theme.of(context).textTheme.displayLarge,
                ),
                SizedBox(height: Dimensions.height10),

                cardAvatarTile(
                  text:
                      "Marking as Signing Complete will send invoice to Customer or Signer based on order type.",
                  icon: FontAwesomeIcons.check,
                  iconColor: Colors.white,
                ),
                SizedBox(height: Dimensions.height10),

                cardAvatarTile(
                  text:
                      "Signers will receive Thank you & request review link on your SMS and Email.",
                  icon: Icons.check,
                  iconColor: Colors.white,
                ),

                SizedBox(height: Dimensions.height10),

                cardAvatarTile(
                  text: "You need to update payment.",
                  icon: Icons.warning,
                  iconColor: Colors.yellow,
                ),

                SizedBox(height: Dimensions.height15),

                //todo Settings
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Settings",
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                ),
                SizedBox(height: Dimensions.height10),
                Column(
                  children: [
                    //todo 3 check box with title
                    Obx(
                      () => ListTileWithCheckBox(
                        tileColor: Colors.white,
                        title: "Don't send invoice to customer.",
                        checkbox: Checkbox(
                          value: markCompleteController.noInvoice.value,
                          onChanged: (value) => markCompleteController
                              .updateNoInvoiceCheckBox(value!),
                        ),
                      ),
                    ),
                    Obx(
                      () => ListTileWithCheckBox(
                        tileColor: Colors.white,
                        title: "Don't send thank you email to signers.",
                        checkbox: Checkbox(
                          value: markCompleteController.noEmail.value,
                          onChanged: (value) => markCompleteController
                              .updateNoEmailCheckBox(value!),
                        ),
                      ),
                    ),
                    Obx(
                      () => ListTileWithCheckBox(
                        tileColor: Colors.white,
                        title: "Don't send request review link.",
                        checkbox: Checkbox(
                          value: markCompleteController.noReview.value,
                          onChanged: (value) => markCompleteController
                              .updateNoReviewCheckBox(value!),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    MyButton(
                      text: "Review & Send",
                      onPressed: () {
                        markCompleteController.reviewAndSend();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
