import 'package:get/get.dart';

import '../../../App-Screens/homepage/view/linkCompany.dart';

class MarkCompleteController extends GetxController {
  RxBool noInvoice = false.obs;
  RxBool noEmail = false.obs;
  RxBool noReview = true.obs;

  void updateNoInvoiceCheckBox(bool value) {
    noInvoice.value = value;
  }

  void updateNoEmailCheckBox(bool value) {
    noEmail.value = value;
  }

  void updateNoReviewCheckBox(bool value) {
    noReview.value = value;
  }

  void reviewAndSend() {
    //todo 4
    Get.to(const LinkCompanyScreen());
  }
}
