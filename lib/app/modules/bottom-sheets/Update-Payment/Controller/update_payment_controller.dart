import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:mobilev1/widgets/past_payment_tile.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/api_data_model/invoices_model.dart';
import '../../../../data/models/repo_response/repo_response.dart';
import '../../../../data/repo/invoices_repo.dart';
import '../payment_mode.dart';

class UpdatePaymentController extends GetxController {
  //* text editing controllers
  final TextEditingController transactionIdController = TextEditingController();

  //* text editing controllers
  final TextEditingController amountTextController = TextEditingController();

  Rx<DateTime> selectedDate =
      DateTime.parse(DateFormat("yyyydd-MM-dd").format(DateTime.now())).obs;

  void pickDate() async {
    final DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year, 1), // current year
      lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
    );
    if (pickedDate != null && pickedDate != DateTime.now()) {
      selectedDate.value =
          DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
    }
  }

  var selectedPaymentMethod = PaymentMethod.cheque.obs;

  void updatePaymentMethod(PaymentMethod? value) {
    selectedPaymentMethod.value = value!;
  }

  final paymentHistoryRows = <PastPaymentTile>[
    const PastPaymentTile(
        date: "2023-05-22", amount: "100.0", paidVia: "Credit Card", id: "1"),
    const PastPaymentTile(
        date: '2022-01-01', amount: "100.0", paidVia: 'Credit Card', id: "1"),
    const PastPaymentTile(
        date: '2022-01-02', amount: "200.0", paidVia: 'PayPal', id: "2"),
    const PastPaymentTile(
        date: '2022-01-03', amount: "150.0", paidVia: 'Bank Transfer', id: "3"),
  ].obs;

  var updatePaymentResponse = {};

  updatePayment() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    UpdatePayment data = UpdatePayment(
      notaryId: GetStorage().read('userId'),
      invoiceId: transactionIdController.value.text,
      amountPaid: amountTextController.text,
      datePaidOn: selectedDate.value.toString(),
      paidVia: selectedPaymentMethod.value.name,
    );
    RepoResponse response = await repo.updatePayment(data);
    if (response.data != null) {
      updatePaymentResponse = response.data;
    }
    LoadingUtils.hideLoader();
  }

}
