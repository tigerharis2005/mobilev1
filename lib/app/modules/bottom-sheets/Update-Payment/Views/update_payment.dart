import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Update-Payment/Controller/update_payment_controller.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Update-Payment/payment_mode.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/button.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/my_text_field.dart';
import 'package:mobilev1/widgets/past_payment_tile.dart';
import 'package:mobilev1/widgets/small_text.dart';

final UpdatePaymentController _updatePaymentController =
    Get.put(UpdatePaymentController());

class UpdatePayment {
  static Future<void> openUpdatePaymentBottomSheet() {
    return Get.bottomSheet(
      isScrollControlled: true,
      elevation: 10,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      barrierColor: Colors.black.withOpacity(0.80),
      backgroundColor: Colors.white,
      Container(
        margin: const EdgeInsets.all(4),
        height: Get.height * 0.70,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              children: [
                SizedBox(height: Dimensions.height10),

                //* title
                BigText(
                  text: "Update Payment",
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                ),

                SizedBox(height: Dimensions.height10),

                //* current service
                CardView(
                    margin: 4,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          // //todo edit button
                          // Align(
                          //   alignment: Alignment.centerRight,
                          //   child: TextButton(
                          //     onPressed: () {},
                          //     child: SmallText(
                          //       text: "Edit",
                          //       color: Colors.blue,
                          //       size: Dimensions.font18,
                          //     ),
                          //   ),
                          // ),

                          //todo invoice details
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  SmallText(
                                    text: "Invoice ID:",
                                    color: Colors.black,
                                    size: Dimensions.font15,
                                  ),
                                  SizedBox(height: Dimensions.height10),
                                  SmallText(
                                    text: "Invoice 4",
                                    color: Colors.black,
                                    size: Dimensions.font18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),

                              //todo invoice id
                              Column(
                                children: [
                                  SmallText(
                                    text: "Amount Payable",
                                    color: Colors.black,
                                    size: Dimensions.font15,
                                  ),
                                  SizedBox(height: Dimensions.height10),
                                  SmallText(
                                    text: "\$ 100",
                                    color: Colors.black,
                                    size: Dimensions.font18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),
                              //todo invoice id
                              Column(
                                children: [
                                  SmallText(
                                    text: "Invoice Date",
                                    color: Colors.black,
                                    size: Dimensions.font15,
                                  ),
                                  SizedBox(height: Dimensions.height10),
                                  SmallText(
                                    text: "12/12/2020",
                                    color: Colors.black,
                                    size: Dimensions.font18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),

                SizedBox(height: Dimensions.height10),

                CardView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 10),
                    child: Column(
                      children: [
                        //* amount paid
                        Align(
                          alignment: Alignment.centerLeft,
                          child: SmallText(
                            text: "Amount Paid",
                            color: Colors.black,
                            size: Dimensions.font15,
                            fontWeight: FontWeight.w600,
                          ),
                        ),

                        SizedBox(height: Dimensions.height10),

                        //* amount text field
                        MyTextField(
                            width: Get.width * 0.75,
                            height: Get.height * 0.05,
                            keyboardType: TextInputType.number,
                            hintText: "Amount",
                            labelText: "Amount",
                            inputFormatters: const [],
                            controller:
                                _updatePaymentController.amountTextController),

                        SizedBox(height: Dimensions.height10),

                        //* date
                        Row(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: SmallText(
                                text: "Date Received",
                                color: Colors.black,
                                size: Dimensions.font15,
                                fontWeight: FontWeight.w600,
                              ),
                            ),

                            const Spacer(),

                            //* date text
                            Obx(
                              () => Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(
                                    color: Colors.black,
                                    width: 0.3,
                                  ),
                                ),
                                width: Get.width * 0.33,
                                height: Get.height * 0.05,
                                child: Center(
                                  child: SmallText(
                                    color: AppColors.black,
                                    size: Dimensions.font15,
                                    text:
                                        "${_updatePaymentController.selectedDate.value.year.toString()} - ${_updatePaymentController.selectedDate.value.month.toString()} - ${_updatePaymentController.selectedDate.value.day.toString()}",
                                  ),
                                ),
                              ),
                            ),

                            SizedBox(
                              width: Dimensions.width10,
                            ),

                            //* date icon
                            Container(
                              height: Get.height * 0.05,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: Colors.black,
                                  width: 0.3,
                                ),
                              ),
                              child: IconButton(
                                iconSize: 20,
                                onPressed: () =>
                                    _updatePaymentController.pickDate(),
                                icon: const Icon(Icons.calendar_today,
                                    color: Colors.black),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: Dimensions.height10),

                        //* receieved via
                        Column(
                          children: [
                            //todo title
                            Align(
                              alignment: Alignment.centerLeft,
                              child: SmallText(
                                text: "Received Via",
                                color: Colors.black,
                                size: Dimensions.font15,
                                fontWeight: FontWeight.w600,
                              ),
                            ),

                            SizedBox(height: Dimensions.height10),

                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: PaymentMethod.values
                                    .map(
                                      (method) => Row(
                                        children: [
                                          Obx(
                                            () => Radio(
                                              activeColor: AppColors.black,
                                              value: method,
                                              groupValue:
                                                  _updatePaymentController
                                                      .selectedPaymentMethod
                                                      .value,
                                              onChanged: (value) =>
                                                  _updatePaymentController
                                                      .updatePaymentMethod(
                                                          value),
                                            ),
                                          ),
                                          SmallText(
                                            text: method.name.toString(),
                                            color: AppColors.black,
                                            size: Dimensions.font15,
                                          ),
                                        ],
                                      ),
                                    )
                                    .toList(),
                              ),
                            )
                          ],
                        ),

                        SizedBox(height: Dimensions.height10),

                        //* transaction id
                        Row(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: SmallText(
                                text: "Transaction ID/ \n Reference No.",
                                color: Colors.black,
                                size: Dimensions.font15,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: Dimensions.width20),
                            MyTextField(
                                width: Get.width * 0.45,
                                height: Get.height * 0.05,
                                hintText: "Transaction ID",
                                labelText: "Transaction ID",
                                inputFormatters: const [],
                                controller: _updatePaymentController
                                    .transactionIdController),
                          ],
                        ),

                        SizedBox(height: Dimensions.height20),

                        //* update button
                        Button(
                            width: Get.width * 0.7,
                            height: Get.height * 0.05,
                            on_pressed: () {},
                            text: "Update"),

                        SizedBox(height: Dimensions.height20),

                        //* Actions Buttons
                        Wrap(
                          direction: Axis.horizontal,
                          children: [
                            Button(
                                width: Get.width * 0.35,
                                height: Get.height * 0.05,
                                on_pressed: () {},
                                text: "Mark as Uncollectable"),
                            SizedBox(width: Dimensions.width10),
                            Button(
                                width: Get.width * 0.35,
                                height: Get.height * 0.05,
                                on_pressed: () {},
                                text: "Cancel Invoice"),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: Dimensions.height10),

                CardView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 5),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: SmallText(
                            text: "Past Payments",
                            color: Colors.black,
                            size: Dimensions.font18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),

                      SizedBox(height: Dimensions.height10),

                      //* past payments tiles
                      ListView.builder(
                        itemCount: 2,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          final results = _updatePaymentController
                              .paymentHistoryRows[index];
                          return PastPaymentTile(
                            date: results.date,
                            amount: results.amount,
                            paidVia: results.paidVia,
                            id: results.id,
                          );
                        },
                      ),
                    ],
                  ),
                ),

                SizedBox(height: Dimensions.height10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
