import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Custom-Expense/custom_expense.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/my_text_field.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/List_Tile_CheckBox.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/list_tile_actions.dart';
import '../../../../data/models/api_data_model/expense_model.dart';
import '../Controller/add_expense_controller.dart';

class AddExpenseSheet {
  //* open bottom sheet
  static Future<dynamic> addExpenseBottomSheet(BuildContext context) {
    final AddExpenseController addExpenseController =
        Get.put(AddExpenseController());
    addExpenseController.init();
    return Get.bottomSheet(
      elevation: 10,
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(0.80),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      backgroundColor: AppColors.kScaffoldBackgroundColor,
      Container(
        margin: const EdgeInsets.all(15),
        child: SizedBox(
          height: Dimensions.height10 * 60,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: Dimensions.height10,
                ),

                //* logo with Title & Sub-title
                Column(
                  children: [
                    Text(
                      "Add Expense",
                      style: Theme.of(context).textTheme.displayLarge,
                    ),

                    SizedBox(
                      height: Dimensions.height10,
                    ),

                    // todo -> sub-title
                    Text(
                      "You haven't added any expenses yet",
                      style: Theme.of(context).textTheme.displayMedium,
                    )
                  ],
                ),

                SizedBox(
                  height: Dimensions.height15,
                ),

                //* expenses card
                CardView(
                  margin: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                      children: [
                        //todo title and sum
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            // todo title
                            SmallText(
                              text: "Expenses",
                              color: Colors.black,
                              size: Dimensions.font20,
                            ),

                            SizedBox(
                              width: Dimensions.width40,
                            ),

                            // todo sum
                            SmallText(
                              text: "\$ 0",
                              color: Colors.black,
                              size: Dimensions.font26,
                              fontWeight: FontWeight.bold,
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height20,
                        ),

                        // todo Hour cost
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            // todo title
                            SmallText(
                              text: "Hour Cost",
                              color: Colors.black,
                              size: Dimensions.font20,
                            ),

                            SizedBox(
                              width: Dimensions.width30,
                            ),

                            //* hour cost text firld
                            MyTextField(
                              width: Get.width * 0.40,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]'),
                                ),
                              ],
                              hintText: '\$0',
                              labelText: "Add Hour Cost",
                              keyboardType: TextInputType.number,
                              controller: addExpenseController
                                  .hourExpenseTextController,
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height15,
                        ),

                        //todo travel expesne
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SmallText(
                              text: "Travel Cost",
                              color: Colors.black,
                              size: Dimensions.font20,
                            ),

                            SizedBox(
                              width: Dimensions.width30,
                            ),

                            //* travel cost field
                            MyTextField(
                              width: Get.width * 0.40,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]'),
                                ),
                              ],
                              hintText: '\$0',
                              labelText: "Add Travel Cost",
                              keyboardType: TextInputType.number,
                              controller: addExpenseController
                                  .travelExpenseTextController,
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        Button(
                          width: Dimensions.screenWidth * 0.4,
                          height: Dimensions.screenHeight * 0.05,
                          text: "Add Expenses",
                          on_pressed: () {},
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //* check box
                            Obx(
                              () => Checkbox(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                checkColor: AppColors.white,
                                activeColor: AppColors.black,
                                value: addExpenseController
                                    .isExpenseNotRequired.value,
                                onChanged: (value) =>
                                    addExpenseController.updateExpense(value!),
                              ),
                            ),
                            SmallText(
                                text:
                                    "I do not have any expense for this signing."),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height18,
                ),

                //* row -> Custom Expsenses & Add Expenses
                CardView(
                  margin: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        //todo  Custom Expenses title
                        Flexible(
                          child: SmallText(
                            text: "Suggested Expense for Mobile Notorisation",
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),

                        SizedBox(
                          width: Dimensions.width15,
                        ),

                        // todo Add Expenses Button
                        Button(
                          width: Dimensions.screenWidth * 0.3,
                          height: Dimensions.screenHeight * 0.05,
                          text: "Custom Expense",
                          on_pressed: () =>
                              CustomExpense.showCustomExpenseDialog(),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height30,
                ),

                Obx(
                  () => ListTileWithCheckBox(
                    tileColor: Colors.white,
                    title: "Dont send invoice to customer",
                    checkbox: Checkbox(
                      value: addExpenseController.isExpenseNotRequired.value,
                      onChanged: (value) =>
                          addExpenseController.updateExpense(value!),
                    ),
                  ),
                ),
                Obx(
                  () => ListTileWithCheckBox(
                    tileColor: Colors.white,
                    title: "Dont send invoice to customer",
                    checkbox: Checkbox(
                      value: addExpenseController.isExpenseNotRequired.value,
                      onChanged: (value) =>
                          addExpenseController.updateExpense(value!),
                    ),
                  ),
                ),

                Center(
                    child: BigText(
                  text: 'Added Expenses',
                  fontWeight: FontWeight.bold,
                )),
                Center(
                    child: BigText(
                  text: 'Added Expenses',
                  fontWeight: FontWeight.bold,
                )),
                SizedBox(
                  height: Dimensions.height30,
                ),
                GetBuilder<AddExpenseController>(
                  builder: (_) {
                    return ListView.builder(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 10),
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount:
                          addExpenseController.appointment['expenses'].length,
                      itemBuilder: ((context, index) {
                        return const ListTileActions(
                          tileColor: Colors.white,
                          dateTime: "2023-12-12",
                          acts: "10",
                          price: "100",
                        );
                      }),
                    );
                  },
                ),
                SizedBox(
                  height: Dimensions.height30,
                ),
                Center(
                    child: BigText(
                  text: 'Recommended Expenses',
                  fontWeight: FontWeight.bold,
                )),
                Center(
                    child: BigText(
                  text: 'Select any expense you wanted to add',
                  size: Dimensions.font15,
                )),
                SizedBox(
                  height: Dimensions.height30,
                ),
                GetBuilder<AddExpenseController>(
                  builder: (_) {
                    return Column(
                      children: [
                        ListView.builder(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount:
                              addExpenseController.recommendedExpenses.length,
                          itemBuilder: ((context, index) {
                            Expenses expenseValue = Expenses(
                              expenseName: addExpenseController
                                  .recommendedExpenses[index]['expenseName'],
                              cost: addExpenseController
                                  .recommendedExpenses[index]['cost'],
                              description: addExpenseController
                                  .recommendedExpenses[index]['description'],
                              vendorName: addExpenseController
                                  .recommendedExpenses[index]['vendorName'],
                              notes: addExpenseController
                                  .recommendedExpenses[index]['notes'],
                              category: addExpenseController
                                  .recommendedExpenses[index]['category'],
                            );
                            bool value = false;
                            for (Expenses expense
                                in addExpenseController.expenses) {
                              if (expense.expenseName ==
                                      expenseValue.expenseName &&
                                  expense.cost == expenseValue.cost &&
                                  expense.description ==
                                      expenseValue.description &&
                                  expense.vendorName ==
                                      expenseValue.vendorName &&
                                  expense.notes == expenseValue.notes &&
                                  expense.category == expenseValue.category) {
                                value = true;
                                break;
                              }
                            }
                            print(value);
                            return
                                // Obx(() =>
                                GestureDetector(
                              onTap: () {
                                print(addExpenseController
                                    .recommendedExpenses[index]);
                                addExpenseController.toAddExpenses(
                                    addExpenseController
                                        .recommendedExpenses[index]);
                              },
                              child: ListTileActions(
                                tileColor: value ? Colors.grey : Colors.white,
                                dateTime:
                                    '${addExpenseController.recommendedExpenses[index]['expenseName']}',
                                acts: "10",
                                price:
                                    "${addExpenseController.recommendedExpenses[index]['cost']}",
                              ),
                            )
                                // )
                                ;
                          }),
                        ),
                        addExpenseController.expenses.isNotEmpty
                            ? Column(
                                children: [
                                  SizedBox(
                                    height: Dimensions.height30,
                                  ),
                                  Button(
                                    width: Dimensions.screenWidth * 0.4,
                                    height: Dimensions.screenHeight * 0.05,
                                    text: "Update Expenses",
                                    on_pressed: () {
                                      addExpenseController.addExpense();
                                    },
                                  ),
                                ],
                              )
                            : SizedBox()
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
