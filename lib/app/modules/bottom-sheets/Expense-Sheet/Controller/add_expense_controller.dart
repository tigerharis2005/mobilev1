import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/repo/expense_repo.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/api_data_model/expense_model.dart';
import '../../../../data/models/repo_response/repo_response.dart';

class AddExpenseController extends GetxController {
  //* hour expense
  TextEditingController hourExpenseTextController = TextEditingController();

  //* travel expense
  TextEditingController travelExpenseTextController = TextEditingController();

  //* search text controller
  TextEditingController searchTextController = TextEditingController();

  RxBool isExpenseNotRequired=false.obs;

  List<Expenses> expenses=[];


  void toAddExpenses(data) {
  Expenses expenseValue = Expenses(
    expenseName: data['expenseName'],
    cost: data['cost'],
    description: data['description'],
    vendorName: data['vendorName'],
    notes: data['notes'],
    category: data['category'],
  );

  bool alreadyExists = false;
  expenses.removeWhere((expense) {
    if (expense.expenseName == expenseValue.expenseName &&
        expense.cost == expenseValue.cost &&
        expense.description == expenseValue.description &&
        expense.vendorName == expenseValue.vendorName &&
        expense.notes == expenseValue.notes &&
        expense.category == expenseValue.category) {
      alreadyExists = true;
      return true; // Remove the matching expense
    }
    return false; // Keep the expense
  });

  if (!alreadyExists) {
    expenses.add(expenseValue);
    update();
  } else {
    update();
  }
}



  updateExpense(bool value){
    isExpenseNotRequired.value=value;
  }

  var appointment;
  List recommendedExpenses=[];
  init() {
    appointment = GetStorage().read('selectedApt');
    print(appointment);
    if(appointment['expenses'].length==0){
      isExpenseNotRequired=true.obs;
    }
    // print(GetStorage().read('suggestedExpenses'));
    recommendedExpenses=GetStorage().read('suggestedExpenses');
    update();
  }



  // var addExpenseData = [];
  addExpense() async {
    LoadingUtils.showLoader();
    ExpenseRepository repo = ExpenseRepository();
    AddExpense data = AddExpense(
        notaryId: GetStorage().read('userId'),
        aptId: appointment['_id'],
        expenses: expenses
        );
    RepoResponse response = await repo.addExpense(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      print('added');
      // addExpenseData = response.data['appointments'];
      update();
    } else if (response.error != null) {
    }
  }
}
