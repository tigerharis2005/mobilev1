import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Custom-Expense/custom_expense_controller.dart';
import 'package:mobilev1/widgets/my_text_field.dart';

import '../../../../utils/colors.dart';
import '../../../../utils/dimensions.dart';
import '../../../../widgets/button.dart';
import '../../../../widgets/drop_down.dart';
import '../../../../widgets/small_text.dart';

class CustomExpense {
  static Future<void> showCustomExpenseDialog() {
    final CustomExpenseController customExpenseController =
        Get.put(CustomExpenseController());
    return Get.defaultDialog(
      title: "Add Custom Expense",
      titleStyle: const TextStyle(
        color: AppColors.black,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
      content: SizedBox(
        height: Get.height * 0.40,
        width: Get.width * 0.75,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //* expense name
              Row(
                children: [
                  SmallText(
                    text: "Expense Name",
                    size: Dimensions.font18,
                  ),
                  const Spacer(),
                  MyTextField(
                    controller:
                        customExpenseController.expenseNameTextController,
                    hintText: "Name",
                    width: Dimensions.screenWidth * 0.3,
                    inputFormatters: const [],
                    height: Get.height * 0.05,
                  ),
                ],
              ),

              SizedBox(height: Dimensions.height15),

              //* amount text field

              Row(
                children: [
                  SmallText(
                    text: "Expense Amount",
                    size: Dimensions.font18,
                  ),
                  const Spacer(),
                  MyTextField(
                    controller:
                        customExpenseController.expenseAmountTextController,
                    hintText: "Amount",
                    width: Dimensions.screenWidth * 0.3,
                    inputFormatters: const [],
                    height: Get.height * 0.05,
                  ),
                ],
              ),

              SizedBox(height: Dimensions.height15),

              //* category
              Row(
                children: [
                  SmallText(
                    text: "Category",
                    size: Dimensions.font18,
                  ),
                  const Spacer(),
                  DropDown(
                    width: Get.width * 0.3,
                    height: Get.height * 0.05,
                    items: customExpenseController.dropMenuItems,
                    name: "Category",
                    value: 1,
                    onChanged: (value) {},
                  ),
                ],
              ),

              SizedBox(height: Dimensions.height15),

              //* upload reciept
              Row(
                children: [
                  SmallText(
                    text: "Upload Receipt",
                    size: Dimensions.font18,
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.upload_file,
                      color: AppColors.black,
                    ),
                  ),
                ],
              ),

              SizedBox(height: Dimensions.height15),

              //* checkbox future save
              Row(
                children: [
                  Obx(
                    () => Checkbox(
                      checkColor: AppColors.white,
                      activeColor: AppColors.black,
                      value: customExpenseController.saveCheckBox.value,
                      onChanged: (value) {
                        customExpenseController
                            .updateCheckBoxForFutureSignings(value!);
                      },
                    ),
                  ),
                  Flexible(
                      child: SmallText(
                          text:
                              "Save this expense to show in future signings")),
                ],
              ),

              SizedBox(height: Dimensions.height15),

              //* save button
              Button(
                width: Dimensions.screenWidth * 0.4,
                height: Get.height * 0.05,
                text: "Create Custom",
                on_pressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
