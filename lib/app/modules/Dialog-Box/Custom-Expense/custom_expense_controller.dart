import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomExpenseController extends GetxController {
  //* expense amount Text controller
  TextEditingController expenseAmountTextController = TextEditingController();

  TextEditingController expenseNameTextController = TextEditingController();

  //* expense category
  RxList<DropdownMenuItem<Object>> dropMenuItems = <DropdownMenuItem<Object>>[
    const DropdownMenuItem(
      value: 1,
      child: Text("Select"),
    )
  ].obs;

  //* checkbox
  RxBool saveCheckBox = false.obs;

  //* update Checkbox
  void updateCheckBoxForFutureSignings(bool value) {
    saveCheckBox.value = value;
    update();
  }
}
