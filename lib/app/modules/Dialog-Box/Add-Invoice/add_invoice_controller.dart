import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddInvoiceController extends GetxController {
  //* add invoice item name text controller
  final TextEditingController addInvoiceItemNameTextController =
      TextEditingController();

  //* add invoice search text controller
  final TextEditingController addInvoiceItemSearchTextController =
      TextEditingController();

  //* add invoice item cost text controller
  final TextEditingController addInvoiceItemCostTextController =
      TextEditingController();

  @override
  void dispose() {
    addInvoiceItemNameTextController.dispose();
    addInvoiceItemSearchTextController.dispose();
    addInvoiceItemCostTextController.dispose();
    super.dispose();
  }
}
