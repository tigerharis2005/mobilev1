import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Add-Invoice/add_invoice_controller.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/button.dart';

import '../../../../utils/colors.dart';
import '../../../../widgets/my_text_field.dart';

class AddInvoice {
  //* show dialog box
  static Future<void> showAddInvoiceDialog() {
    //*
    final AddInvoiceController addInvoiceController =
        Get.put<AddInvoiceController>(AddInvoiceController());
    //*
    return Get.defaultDialog(
      title: "Add Invoice Item",
      titleStyle: const TextStyle(
        color: AppColors.black,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
      content: SizedBox(
        height: 200,
        width: 400,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: [
              //* item name
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: MyTextField(
                  width: Get.width * 0.8,
                  height: Get.height * 0.06,
                  inputFormatters: const [],
                  hintText: "Add Item Name",
                  labelText: "Service / Product",
                  controller:
                      addInvoiceController.addInvoiceItemNameTextController,
                ),
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* search text field
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: MyTextField(
                      width: Get.width * 0.30,
                      height: Get.height * 0.06,
                      inputFormatters: const [],
                      hintText: "Add Item Cost",
                      labelText: "Cost",
                      controller: addInvoiceController
                          .addInvoiceItemSearchTextController,
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: MyTextField(
                      width: Get.width * 0.30,
                      height: Get.height * 0.06,
                      inputFormatters: const [],
                      hintText: "Add Item Quantity",
                      labelText: "Quantity",
                      controller:
                          addInvoiceController.addInvoiceItemCostTextController,
                    ),
                  ),
                ],
              ),

              SizedBox(
                height: Dimensions.height20,
              ),

              //* add button
              Button(
                width: Get.width * 0.5,
                height: Get.height * 0.06,
                text: "Add",
                on_pressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
