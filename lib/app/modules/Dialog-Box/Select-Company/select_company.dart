import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Select-Company/select_company_controller.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/my_text_field.dart';

import '../../../../utils/colors.dart';
import '../../../../widgets/small_text.dart';

// this code is to trigger select company dialog box

class SelectCompany {
  static Future<void> showCompanyDialog() {
    //* find controller
    final SelectCompanyController selectCompanyController =
        Get.put(SelectCompanyController());

    //* open dialog box
    return Get.defaultDialog(
      title: "Select Company",
      titleStyle: const TextStyle(
        color: AppColors.black,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
      content: SizedBox(
        height: 350,
        width: 400,
        child: Column(
          children: [
            //* sub title
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Flexible(
                child: SmallText(
                    color: AppColors.black,
                    text:
                        "Contacts which have a company linked show up  here. If you dont find a contact. Create a new one."),
              ),
            ),

            SizedBox(
              height: Dimensions.height10,
            ),

            //* search text field
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: MyTextField(
                width: Get.width * 0.8,
                inputFormatters: const [],
                hintText: "Search",
                controller: selectCompanyController.searchCompanyTextController,
              ),
            ),

            //* list view
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: 10,
                itemBuilder: (context, index) {
                  return ListTile(
                      title: const Text("Ashley Company"),
                      subtitle: const Text("Pro Signings Title Company"),
                      onTap: () {
                        selectCompanyController.selectedCompany.value =
                            "Ashley Company";
                        Get.back();
                      },
                      trailing: IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.mode_edit_outlined,
                          color: Colors.black,
                        ),
                      ));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
