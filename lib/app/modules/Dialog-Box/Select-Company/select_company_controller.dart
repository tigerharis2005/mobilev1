import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Models/invoice_item.dart';

class SelectCompanyController extends GetxController {
  //* search company in dialog box controller
  final TextEditingController searchCompanyTextController =
      TextEditingController();

  Rx<String> selectedCompany = "Ashley Company".obs;

  Rx<String> selectedUser = "Nandha Kumar".obs;

  List<InvoiceItem> invoiceItemList = <InvoiceItem>[
    InvoiceItem(itemName: "Legal Advice", price: 500, quantity: 2, total: 1000),
    InvoiceItem(
        itemName: "Expert Consulting", price: 400, quantity: 10, total: 400),
  ];
}
