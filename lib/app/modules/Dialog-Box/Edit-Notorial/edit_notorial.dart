import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/dialog-Box/Edit-Notorial/edit_notorial_controller.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/my_text_field.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../utils/colors.dart';

class EditNotorial {
  static Future<void> openEditNotorialActDialogBox(
      {required int notorialActs, required int price}) {
    final EditNotorialController editNotorialController =
        Get.put(EditNotorialController());
    return Get.defaultDialog(
      title: "Edit Act",
      titleStyle: const TextStyle(
        color: AppColors.black,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
      content: SizedBox(
        width: Get.width * 0.75,
        height: Get.height * 0.20,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: SmallText(
                  text: "Notorial Acts",
                  color: AppColors.black,
                  fontWeight: FontWeight.w600,
                  size: Dimensions.font18,
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Container(
                width: Dimensions.screenWidth * 0.8,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: AppColors.black,
                    width: 1,
                  ),
                ),
                child: SmallText(
                  color: AppColors.black,
                  fontWeight: FontWeight.w600,
                  size: Dimensions.font18,
                  text: notorialActs.toString(),
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: SmallText(
                  text: "Cost Per Act",
                  color: AppColors.black,
                  fontWeight: FontWeight.w600,
                  size: Dimensions.font18,
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Container(
                width: Dimensions.screenWidth * 0.8,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: AppColors.black,
                    width: 1,
                  ),
                ),
                child: SmallText(
                  color: AppColors.black,
                  fontWeight: FontWeight.w600,
                  size: Dimensions.font18,
                  text: price.toString(),
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),

              //* date
              Wrap(
                spacing: 10,
                children: [
                  Obx(
                    () => MyTextField(
                      width: Get.width * 0.60,
                      height: Get.height * 0.06,
                      keyboardType: TextInputType.text,
                      readOnly: true,
                      hintText: "",
                      inputFormatters: const [],
                      controller: editNotorialController.editTimeTextController,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.black,
                        width: 0.3,
                      ),
                    ),
                    child: IconButton(
                      iconSize: 20,
                      onPressed: () => editNotorialController.pickDate(),
                      icon:
                          const Icon(Icons.calendar_today, color: Colors.black),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
