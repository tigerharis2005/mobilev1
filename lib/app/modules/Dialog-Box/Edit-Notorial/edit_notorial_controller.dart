import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class EditNotorialController extends GetxController {
  //* edit date dialog text controller
  final TextEditingController editTimeTextController = TextEditingController();

  Rx<DateTime> selectedDate =
      DateTime.parse(DateFormat("yyyy-MM-dd").format(DateTime.now())).obs;

  void pickDate() async {
    final DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year, 1), // current year
      lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
    );
    if (pickedDate != null) {
      selectedDate.value =
          DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
    }
  }
}
