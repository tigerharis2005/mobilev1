import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/get_appointment_by_notary_id_model.dart';
import 'package:mobilev1/app/data/models/api_data_model/invoices_model.dart';
import 'package:mobilev1/app/data/models/repo_response/repo_response.dart';
import 'package:mobilev1/app/data/repo/get_appointment_by_notary_repo.dart';
import 'package:mobilev1/app/data/repo/invoices_repo.dart';
import 'package:mobilev1/helper/loading.dart';

import '../../../../data/models/api_data_model/login.dart';
import '../../../../data/repo/login_repo.dart';

class HomePageController extends GetxController
    with GetTickerProviderStateMixin {
  LoginRepository loginRepo = LoginRepository();
  final List<String> items = ['Item 1', 'Item 2', 'Item 3'];
  late TabController tabController;
  RxInt bottomTabIndex = 0.obs;
  var allPaidInvoices = [];
  var allDueInvoices = [];
  var allOverdueInvoices = [];
  var allInvoices = <dynamic>[].obs;

  List appointments = [];
  List service = [];
  List companyList = [];

  String selectedAptType = 'Today';
  int selectedAptIndex = 0;

  var selectedDifficultyLevelIndex = 0.obs;
  var totalCount = 0.obs;
  var paidCount = 0.obs;
  var dueCount = 0.obs;
  var overdueCount = 0.obs;

  selectApt(String selectType, int index) {
    selectedAptType = selectType;
    selectedAptIndex = index;
    update();
  }

  String name = '';

  var selectedIndex = 0.obs;

  void onContainerPressed(int index) {
    selectedIndex.value = index;
  }

  void onButtonPressed(int difficultyLevelIndex) {
    selectedDifficultyLevelIndex.value = difficultyLevelIndex;
  }

  String paid = '';
  String due = '';
  String overdue = '';

  @override
  void onReady() async {
    await login();
    name = GetStorage().read('name') ?? "User";
    appointments = await GetStorage().read('upcomingApts');
    service = await GetStorage().read('availableServices')['services'];
    companyList = await GetStorage().read('companiesList');

    update();
    super.onReady();
  }

  @override
  void onInit() {
    super.onInit();

    tabController = TabController(length: 2, vsync: this);

    update();
  }

  login() async {
    LoginModel login = LoginModel(
      email: GetStorage().read('email'),
      uid: GetStorage().read('uid'),
      source: 'mobile',
    );
    LoadingUtils.showLoader();
    RepoResponse response = await loginRepo.login(login);
    LoadingUtils.hideLoader();
    if (response.error != null) {
      debugPrint(response.error!.message);
    } else {
      debugPrint('success');
    }
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }

  void changeBottomTabIndex(int index) {
    if (index == 1) {
      upcomingApointment();
      completedApointment();
    } else if (index == 2) {
      getallInvoices();
    } else if (index == 0) {
      if (appointments.isNotEmpty) {
        GetStorage().write('selectedApt', appointments[0]);
        selectedAptIndex = 0;
        selectedAptType = 'Today';
        update();
      }
    }
    bottomTabIndex.value = index;
    update();
  }

  var upcomingApointmentData = [];
  upcomingApointment() async {
    LoadingUtils.showLoader();
    AppointmentRepository repo = AppointmentRepository();
    GetAppointmentsByNotaryId data = GetAppointmentsByNotaryId(
      notaryId: GetStorage().read('userId'),
      status: 0,
    );
    RepoResponse response = await repo.getAppointmentByNotaryId(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      debugPrint('success');
      upcomingApointmentData = response.data['appointments'];
      update();
    } else if (response.error != null) {
      debugPrint(response.error!.message);
    }
  }

  var completedApointmentData = [];
  completedApointment() async {
    LoadingUtils.showLoader();
    AppointmentRepository repo = AppointmentRepository();
    GetAppointmentsByNotaryId data = GetAppointmentsByNotaryId(
      notaryId: GetStorage().read('userId'),
      status: 1,
    );
    RepoResponse response = await repo.getAppointmentByNotaryId(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      debugPrint('success');
      completedApointmentData = response.data['appointments'];
      update();
    } else if (response.error != null) {
      debugPrint(response.error!.message);
    }
  }

  var allApt = [];
  allApointment() async {
    LoadingUtils.showLoader();
    AppointmentRepository repo = AppointmentRepository();
    GetAppointmentsByNotaryId data = GetAppointmentsByNotaryId(
      notaryId: GetStorage().read('userId'),
    );
    RepoResponse response = await repo.getAppointmentByNotaryId(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      allApt = response.data['appointments'];
      update();
    } else if (response.error != null) {
      debugPrint(response.error!.message);
    }
  }

  getDueInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data = GetInvoicesForNotary(
      notaryId: GetStorage().read('userId'),
      status: 0,
    );
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      debugPrint('success');
      allDueInvoices = response.data;
      updateCounts();
    } else if (response.error != null) {
      debugPrint(response.error!.message);
    }
  }

  // getPaidInvoices() async {
  //   LoadingUtils.showLoader();
  //   InvoicesRepository repo = InvoicesRepository();
  //   GetInvoicesForNotary data = GetInvoicesForNotary(
  //     notaryId: GetStorage().read('userId'),
  //     status: 1,
  //   );
  //   RepoResponse response = await repo.getInvoicesForNotary(data);
  //   LoadingUtils.hideLoader();
  //   if (response.data != null) {
  //     debugPrint('success');
  //     allPaidInvoices = response.data;
  //     updateCounts();
  //   } else if (response.error != null) {
  //     debugPrint(response.error!.message);
  //   }
  // }

  getOverdueInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data = GetInvoicesForNotary(
      notaryId: GetStorage().read('userId'),
      status: 2,
    );
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      debugPrint('success');
      allOverdueInvoices = response.data;
      updateCounts();
    } else if (response.error != null) {
      debugPrint(response.error!.message);
    }
  }

  getallInvoices() {
    getDueInvoices();
    //getPaidInvoices();
    getOverdueInvoices();
  }

  void updateCounts() {
    totalCount.value = allPaidInvoices.length +
        allDueInvoices.length +
        allOverdueInvoices.length;
    paidCount.value = allPaidInvoices.length;
    dueCount.value = allDueInvoices.length;
    overdueCount.value = allOverdueInvoices.length;
  }
}
