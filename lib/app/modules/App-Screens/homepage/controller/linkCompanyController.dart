import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mobilev1/Models/invoice_item.dart';

class LinkCompanyController extends GetxController {
  //* Switch Controller
  RxBool isSwitchOn = false.obs;

  //* Invoice Text Field Controller
  TextEditingController invoiceNumberController = TextEditingController();

  //* recipient emails controller
  TextEditingController recipientEmailController = TextEditingController();

  //* from date controller
  TextEditingController fromDateTextController = TextEditingController();

  //* additional comments controller
  TextEditingController additionalCommentTextController =
      TextEditingController();

  //* From Date
  Rx<DateTime> fromDateSelected =
      DateTime.parse(DateFormat("yyyydd-MM-dd").format(DateTime.now())).obs;

  //* To Date
  Rx<DateTime> toDateSelected =
      DateTime.parse(DateFormat("yyyydd-MM-dd").format(DateTime.now())).obs;

  //* update swtich
  void updateSwitch(bool value) {
    isSwitchOn.value = !isSwitchOn.value;
  }

  //* update date
  void pickDate({required bool fromDate, required bool toDate}) async {
    if (fromDate) {
      final DateTime? pickedDate = await showDatePicker(
        context: Get.context!,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year, 1), // current year
        lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
      );
      if (pickedDate != null && pickedDate != fromDateSelected.value) {
        fromDateSelected.value =
            DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
      }
    } else {
      final DateTime? pickedDate = await showDatePicker(
        context: Get.context!,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year, 1), // current year
        lastDate: DateTime(DateTime.now().year + 2), // current year + 2 years
      );
      if (pickedDate != null && pickedDate != DateTime.now()) {
        toDateSelected.value =
            DateTime.parse(DateFormat("yyyy-MM-dd").format(pickedDate));
      }
    }
  }

  Rx<String> selectedCompany = "Ashley Company".obs;

  Rx<String> selectedUser = "Nandha Kumar".obs;

  List<InvoiceItem> invoiceItemList = <InvoiceItem>[
    InvoiceItem(itemName: "Legal Advice", price: 500, quantity: 2, total: 1000),
    InvoiceItem(
        itemName: "Expert Consulting", price: 400, quantity: 10, total: 400),
  ];

  void searchCompany() {}

  void save() {}

  void cancel() {}

  @override
  void dispose() {
    invoiceNumberController.dispose();
    super.dispose();
  }
}
