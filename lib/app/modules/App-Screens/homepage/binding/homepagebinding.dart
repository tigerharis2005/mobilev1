import 'package:get/get.dart';

import '../controller/homepagecontroller.dart';

class HomePageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomePageController());
  }
}
