import "package:flutter/material.dart";
import "package:font_awesome_flutter/font_awesome_flutter.dart";
import "package:get/get.dart";
import "package:mobilev1/app/modules/App-Screens/homepage/controller/linkCompanyController.dart";
import 'package:mobilev1/app/modules/dialog-Box/Add-Invoice/add_invoice_item.dart';
import 'package:mobilev1/app/modules/dialog-Box/Select-Company/select_company.dart';
import "package:mobilev1/utils/dimensions.dart";
import "package:mobilev1/widgets/button.dart";
import "package:mobilev1/widgets/invoice_item_tile.dart";
import "package:mobilev1/widgets/my_text_field.dart";
import "package:mobilev1/widgets/small_text.dart";

import "../../../../../utils/colors.dart";
import "../../../../../widgets/card.dart";
import "../../../bottom-sheets/Bill-To/views/bill_to_view.dart";

class LinkCompanyScreen extends StatelessWidget {
  const LinkCompanyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LinkCompanyController linkCompanyController =
        Get.put(LinkCompanyController());

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
            child: Column(
              children: [
                //* 1st row
                CardView(
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: Dimensions.width10,
                        ),
                        SmallText(
                          text: "Invoice #",
                          color: AppColors.black,
                          size: Dimensions.font20,
                        ),
                        SizedBox(
                          width: Dimensions.width20,
                        ),
                        MyTextField(
                          height: Get.height * 0.05,
                          width: Get.width * 0.45,
                          keyboardType: TextInputType.text,
                          labelText: "ET-123123",
                          hintText: "Invoice Number",
                          inputFormatters: const [],
                          controller:
                              linkCompanyController.invoiceNumberController,
                        ),
                        SizedBox(
                          width: Dimensions.width10,
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.info,
                            color: AppColors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height10,
                ),

                //* warning card
                CardView(
                  cardColor: Colors.red.shade50,
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Column(
                      children: [
                        //* first row
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              width: Dimensions.width10,
                            ),
                            const FaIcon(
                              FontAwesomeIcons.circleExclamation,
                              color: Colors.red,
                            ),
                            SizedBox(
                              width: Dimensions.width20,
                            ),
                            Flexible(
                              child: SmallText(
                                text:
                                    "Escrow Number is missing - All Real estate Signings require an Escrow number. You can edit if you want custom invoice number.",
                                color: AppColors.black,
                                size: Dimensions.font15,
                              ),
                            ),
                          ],
                        ),

                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* warning card - switch
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: SmallText(
                                text:
                                    "Set this as Escrow number for this signing",
                                color: AppColors.black,
                                size: Dimensions.font15,
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.width20,
                            ),
                            Obx(
                              () => Switch(
                                inactiveThumbColor:
                                    Colors.black.withOpacity(0.6),
                                activeColor: AppColors.black,
                                activeTrackColor: Colors.black12,
                                value: linkCompanyController.isSwitchOn.value,
                                onChanged: (value) =>
                                    linkCompanyController.updateSwitch(value),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                //* details card
                CardView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: Dimensions.height10,
                      ),

                      //* card title
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: SmallText(
                            text: "Details",
                            color: AppColors.black,
                            size: Dimensions.font20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height15,
                      ),

                      //* emails
                      MyTextField(
                        width: Get.width * 0.80,
                        inputFormatters: const [],
                        keyboardType: TextInputType.text,
                        hintText: "Recipient Emails",
                        labelText: "",
                        controller:
                            linkCompanyController.recipientEmailController,
                      ),

                      SizedBox(
                        height: Dimensions.height20,
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            //* Issue Date
                            Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: SmallText(
                                    text: "Issue Date",
                                    color: AppColors.black,
                                    size: Dimensions.font15,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Row(
                                  children: [
                                    Obx(
                                      () => MyTextField(
                                        readOnly: true,
                                        width: Get.width * 0.29,
                                        keyboardType: TextInputType.text,
                                        hintText:
                                            "${linkCompanyController.toDateSelected.value.year.toString()} - ${linkCompanyController.toDateSelected.value.month.toString()} - ${linkCompanyController.toDateSelected.value.day.toString()}",
                                        inputFormatters: const [],
                                        controller: linkCompanyController
                                            .fromDateTextController,
                                      ),
                                    ),
                                    IconButton(
                                      iconSize: 20,
                                      onPressed: () =>
                                          linkCompanyController.pickDate(
                                              fromDate: false, toDate: true),
                                      icon: const Icon(Icons.calendar_today,
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                              ],
                            ),

                            //* Due Date
                            Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: SmallText(
                                    text: "Due Date",
                                    color: AppColors.black,
                                    size: Dimensions.font15,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Row(
                                  children: [
                                    Obx(
                                      () => MyTextField(
                                        readOnly: true,
                                        width: Get.width * 0.29,
                                        keyboardType: TextInputType.text,
                                        hintText:
                                            "${linkCompanyController.toDateSelected.value.year.toString()} - ${linkCompanyController.toDateSelected.value.month.toString()} - ${linkCompanyController.toDateSelected.value.day.toString()}",
                                        inputFormatters: const [],
                                        controller: linkCompanyController
                                            .fromDateTextController,
                                      ),
                                    ),
                                    IconButton(
                                      iconSize: 20,
                                      onPressed: () =>
                                          linkCompanyController.pickDate(
                                              fromDate: false, toDate: true),
                                      icon: const Icon(Icons.calendar_today,
                                          color: Colors.black),
                                    ),
                                    SizedBox(width: Dimensions.width10),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height20,
                      ),

                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: SmallText(
                            text: "Bill to",
                            color: AppColors.black,
                            size: Dimensions.font15,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height10,
                      ),

                      //* Bill to
                      Obx(
                        () => GestureDetector(
                          onTap: () => BillTo.openBillToBottomSheet(),
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            width: Get.width * 0.80,
                            height: Get.height * 0.05,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: Colors.black.withOpacity(0.2),
                                width: 1,
                              ),
                            ),
                            child: Row(
                              children: [
                                //* company name
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(linkCompanyController
                                      .selectedCompany.value),
                                ),

                                const Spacer(),

                                //* dropdown icon
                                IconButton(
                                  onPressed: () =>
                                      BillTo.openBillToBottomSheet(),
                                  icon: Icon(
                                    Icons.arrow_drop_down_circle_outlined,
                                    color: Colors.black.withOpacity(0.7),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height10,
                      ),

                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: SmallText(
                            text: "Bill from",
                            color: AppColors.black,
                            size: Dimensions.font15,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height10,
                      ),

                      //* Bill from
                      Obx(
                        () => GestureDetector(
                          onTap: () => SelectCompany.showCompanyDialog(),
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            width: Get.width * 0.80,
                            height: Get.height * 0.05,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: Colors.black.withOpacity(0.2),
                                width: 1,
                              ),
                            ),
                            child: Row(
                              children: [
                                //* company name
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                      linkCompanyController.selectedUser.value),
                                ),

                                const Spacer(),

                                //* dropdown icon
                                IconButton(
                                  onPressed: () =>
                                      SelectCompany.showCompanyDialog(),
                                  icon: Icon(
                                    Icons.arrow_drop_down_circle_outlined,
                                    color: Colors.black.withOpacity(0.7),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: Dimensions.height20,
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                //* Invoice items
                CardView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 10),
                    child: Column(
                      children: [
                        //* invoice items title
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: SmallText(
                              text: "Invoice Items",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),
                        //* invoice items title
                        Wrap(
                          spacing: 40,
                          children: [
                            SmallText(
                              text: "Items",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.w600,
                            ),
                            SmallText(
                              text: "Price",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.w600,
                            ),
                            SmallText(
                              text: "Qty",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.w600,
                            ),
                            SmallText(
                              text: "Total Price",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),

                        //* invoice items list
                        ListView.builder(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            itemCount:
                                linkCompanyController.invoiceItemList.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              final res =
                                  linkCompanyController.invoiceItemList[index];
                              return InvoiceItemTile(
                                itemName: res.itemName,
                                price: res.price,
                                quantity: res.quantity,
                                total: res.total,
                              );
                            }),

                        //* add new item row
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            children: [
                              //* add item title
                              TextButton.icon(
                                icon: const Icon(Icons.add,
                                    color: AppColors.black),
                                onPressed: () =>
                                    AddInvoice.showAddInvoiceDialog(),
                                label: SmallText(
                                  text: "Add Item",
                                  size: Dimensions.font15,
                                  color: AppColors.black,
                                ),
                              ),

                              const Spacer(),

                              //* total amount
                              RichText(
                                text: TextSpan(
                                  text: "Total Amount: ",
                                  style: TextStyle(
                                    fontFamily: "Whitney",
                                    color: AppColors.black,
                                    fontSize: Dimensions.font15,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: "\$1000",
                                      style: TextStyle(
                                        fontFamily: "Whitney",
                                        color: AppColors.black,
                                        fontSize: Dimensions.font18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height15,
                        ),

                        //* additional comments title
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: SmallText(
                              text: "Additional Notes",
                              color: AppColors.black,
                              size: Dimensions.font20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: Dimensions.height10,
                        ),

                        //* additional comments text field
                        MyTextField(
                          width: Get.width * 0.80,
                          height: Get.height * 0.10,
                          maxLines: 2,
                          hintText: "Additional notes for the client",
                          inputFormatters: const [],
                          controller: linkCompanyController
                              .additionalCommentTextController,
                        ),

                        SizedBox(
                          height: Dimensions.height15,
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),

                //* buttons
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: Get.height * 0.05,
                        width: Get.width * 0.24,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.black,
                          ),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: TextButton(
                          onPressed: () {},
                          child: SmallText(
                            text: "Preview",
                            color: AppColors.black,
                            fontWeight: FontWeight.w500,
                            size: Dimensions.font15,
                          ),
                        ),
                      ),

                      const Spacer(),

                      //* save button
                      Button(
                        width: Get.width * 0.24,
                        height: Get.height * 0.05,
                        text: "Save as Draft",
                        on_pressed: () => linkCompanyController.save(),
                      ),

                      SizedBox(
                        width: Dimensions.width10,
                      ),

                      //* cancel button
                      Button(
                        width: Get.width * 0.20,
                        height: Get.height * 0.05,
                        text: "Send",
                        on_pressed: () => linkCompanyController.cancel(),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: Dimensions.height20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
