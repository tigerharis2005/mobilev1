import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/modules/Auth-Screens/loginpage/view/loginpageview.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../controller/homepagecontroller.dart';
import 'profileOptionRowWidget.dart';

class ProfileWidget extends GetView<HomePageController> {

  
  const ProfileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: Dimensions.height30 * 1.7,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Container(
            //     padding: EdgeInsets.only(
            //         top: Dimensions.height15 / 3,
            //         bottom: Dimensions.height15 / 3,
            //         left: Dimensions.height15 / 1.4,
            //         right: Dimensions.height15 / 5),
            //     decoration: BoxDecoration(
            //         borderRadius: BorderRadius.all(
            //             Radius.circular(Dimensions.radius15 / 2)),
            //         border: Border.all(
            //             width: 1, color: Colors.lightBlue.withOpacity(0.5))),
            //     child: Center(
            //         child: Icon(
            //       Icons.arrow_back_ios,
            //       size: Dimensions.iconSize24,
            //       color: const Color(0xFF0D47A1),
            //     ))),
            SizedBox(
              width: Dimensions.screenWidth * 0.37,
            ),
            BigText(
              text: 'Profile',
              size: Dimensions.font26 * 1.2,
              fontWeight: FontWeight.w500,
            ),
          ],
        ),
        SizedBox(
          height: Dimensions.height40 * 1.1,
        ),
        Row(
          children: [
            SizedBox(
              width: Dimensions.screenWidth * 0.32,
            ),
            CircleAvatar(
              radius: Dimensions.radius30 * 2.2,
              backgroundImage: const NetworkImage(
                  'https://cdn-icons-png.flaticon.com/512/2919/2919906.png'),
            ),
          ],
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        BigText(text: 'Ashley Octavia'),
        SizedBox(
          height: Dimensions.height10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.manage_accounts),
            SizedBox(
              width: Dimensions.width10,
            ),
            SmallText(
              text: 'Manager',
              size: Dimensions.font15 * 1.2,
            ),
          ],
        ),
        SizedBox(
          height: Dimensions.height40 * 1.2,
        ),
        const ProfileOptionsRow(
          profileOptionIcon: Icons.person,
          profileOptionName: 'Account Settings',
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        const ProfileOptionsRow(
          profileOptionIcon: Icons.contact_mail,
          profileOptionName: 'Contact List',
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        const ProfileOptionsRow(
          profileOptionIcon: Icons.work,
          profileOptionName: 'Company List',
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        const ProfileOptionsRow(
          profileOptionIcon: Icons.design_services,
          profileOptionName: 'Services List',
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        const ProfileOptionsRow(
          profileOptionIcon: Icons.apartment,
          profileOptionName: 'AptDetails List',
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        const Spacer(),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: Dimensions.width15),
            child: GestureDetector(
              onTap: () {
                Get.clearRouteTree();
                GetStorage().erase();
                LoginPage.launch();
              },
              child: Container(
                width: Dimensions.width40*3.5,
                  padding: EdgeInsets.only(
                      top: Dimensions.height20 / 3,
                      bottom: Dimensions.height20 / 3,
                      left: Dimensions.height20 / 1.4,
                      ),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(Radius.circular(Dimensions.radius15 / 2)),
                      border: Border.all(
                          width: 1, color: Colors.lightBlue.withOpacity(0.5))),
                  child: Center(
                      child: Row(
                    children: [
                      Icon(
                        Icons.logout,
                        size: Dimensions.iconSize24,
                        color: const Color(0xFF0D47A1),
                      ),
                      SizedBox(width: Dimensions.width10,),
                      BigText(text: 'Sign Out'),
                      SizedBox(width: Dimensions.width10,),
                    ],
                  ))),
            ),
          ),
        ),
            SizedBox(height: Dimensions.height10,)
      ],
    );
  }
}
