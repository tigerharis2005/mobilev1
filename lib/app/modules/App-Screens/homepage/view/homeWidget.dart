import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/controller/homepagecontroller.dart';
import 'package:mobilev1/widgets/buttons.dart';
import '../../../../../utils/colors.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../apt_details_tab/controller/apt_details_controller.dart';
import '../../apt_details_tab/view/apt_details_view.dart';

class HomeWidget extends GetView<HomePageController> {
  const HomeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height20,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome",
                        style:
                            Theme.of(context).textTheme.displayLarge!.copyWith(
                                  color: Colors.grey[700],
                                ),
                      ),
                      GetBuilder<HomePageController>(
                        builder: (_) {
                          return Text(
                            controller.name,
                            style: Theme.of(context).textTheme.displayLarge,
                          );
                        },
                      ),
                    ],
                  ),
                  const Spacer(),
                  const CircleAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage(
                        "https://images.pexels.com/photos/5384445/pexels-photo-5384445.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"),
                  ),
                  SizedBox(
                    width: Dimensions.width20,
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: Dimensions.height20,
                  ),
                  controller.appointments.isNotEmpty
                      ? MyButton(
                          text:
                              'Total Upcoming Appointments: ${controller.appointments.length}',
                          onPressed: () {},
                        )
                      : const SizedBox(),
                  SizedBox(height: Dimensions.height15),
                  Text(
                    'Appointment',
                    style: Theme.of(context).textTheme.displaySmall,
                  ),
                  SizedBox(height: Dimensions.height15),
                  SizedBox(
                    width: double.maxFinite,
                    height: Dimensions.screenHeight * 0.2,
                    child: GetBuilder<HomePageController>(
                      builder: (_) {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.appointments.length,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  GetStorage().write('selectedApt',
                                      controller.appointments[index]);
                                  Get.lazyPut(() => AptDetailsController());
                                  Get.to(() => AptDetailsPage(
                                      data: controller.appointments[index]));
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: Dimensions.width20,
                                      vertical: Dimensions.height10),
                                  margin: EdgeInsets.only(
                                      right: Dimensions.width10),
                                  height: Dimensions.height40 * 6,
                                  width: Dimensions.width40 * 8,
                                  decoration: BoxDecoration(
                                    color: AppColors.kDarkBackgroundColor,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            controller.appointments[index]
                                                ['signers'][0]['firstName'],
                                            style: Theme.of(context)
                                                .textTheme
                                                .displayMedium!
                                                .copyWith(
                                                  color: Colors.white,
                                                ),
                                          ),
                                          ActionChip(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                            ),
                                            label: Text(
                                              "USD 10",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodySmall!
                                                  .copyWith(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      BigText(
                                        text: controller.appointments[index]
                                            ['propertyAddress'],
                                        maxLines: 2,
                                        // fontWeight: FontWeight.bold,
                                        color: AppColors.white,
                                        size: Dimensions.font20 * .7,
                                      ),
                                      BigText(
                                        color: AppColors.white,
                                        text: controller.appointments[index]
                                            ['createdAt'],
                                        size: Dimensions.font20 * .7,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            });
                      },
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.height20,
                  ),
                  Text(
                    'Services we provide',
                    style: Theme.of(context).textTheme.displaySmall,
                  ),
                  SizedBox(
                    height: Dimensions.height20,
                  ),
                  MyButton(
                    text: "Total Services: ${controller.service.length}",
                    onPressed: () {},
                  ),
                  SizedBox(
                    height: Dimensions.height20,
                  ),
                  SizedBox(
                    width: double.maxFinite,
                    height: Dimensions.height40 * 4,
                    child: GetBuilder<HomePageController>(
                      builder: (_) {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.service.length,
                            itemBuilder: (context, index) {
                              return Container(
                                  margin: EdgeInsets.only(
                                      right: Dimensions.width15),
                                  height: Dimensions.height40 * 4,
                                  width: Dimensions.width40 * 7,
                                  child: Stack(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: Dimensions.width40 * 2),
                                        width: double.maxFinite,
                                        height: double.maxFinite,
                                        margin: EdgeInsets.fromLTRB(
                                            Dimensions.width20 * 1.5, 0, 0, 0),
                                        decoration: BoxDecoration(
                                            color: const Color.fromARGB(
                                                255, 230, 230, 230),
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        child: Center(
                                            child: BigText(
                                          text: controller.service[index]
                                                  ['serviceName'] ??
                                              'Unknown',
                                          color: AppColors.black,
                                          size: Dimensions.font20 * 1.1,
                                          maxLines: null,
                                        )),
                                      ),
                                      Align(
                                          alignment: Alignment.bottomLeft,
                                          child: BigText(
                                            text: '${index + 1}',
                                            size: Dimensions.font26 * 4,
                                            fontWeight: FontWeight.w900,
                                            color: const Color.fromARGB(
                                                131, 0, 0, 0),
                                          ))
                                    ],
                                  ));
                            });
                      },
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.height40,
                  ),
                  Text(
                    'Companies',
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  Text(
                    'Total Companies',
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  SizedBox(
                    width: double.maxFinite,
                    height: Dimensions.height40 * 4,
                    child: GetBuilder<HomePageController>(
                      builder: (_) {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.companyList.length,
                            itemBuilder: (context, index) {
                              return Container(
                                  padding: EdgeInsets.only(
                                      right: Dimensions.width20),
                                  margin: EdgeInsets.only(
                                      right: Dimensions.width15),
                                  height: Dimensions.height40 * 4,
                                  width: Dimensions.width40 * 7,
                                  decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 230, 230, 230),
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: Dimensions.width40 * 3,
                                        height: double.maxFinite,
                                        decoration: BoxDecoration(
                                            color: AppColors.blue,
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        child: Image.network(controller
                                            .companyList[index]['photoURL']),
                                      ),
                                      BigText(
                                        text:
                                            '${controller.companyList[index]['name']}',
                                        size: Dimensions.font20 * 1.1,
                                        color: AppColors.black,
                                      )
                                    ],
                                  ));
                            });
                      },
                    ),
                  ),
                  const SizedBox(height: 5),
                ],
              )
              // SizedBox(height: Dimensions.height10,),
              // BigText(text: 'Your upcoming appointments',color: AppColors.grey,size: Dimensions.font20*.8,),

              // Expanded(
              //   child: SingleChildScrollView(
              //     child: Column(
              //       children: [
              //         SizedBox(
              //           height: Dimensions.height20,
              //         ),
              //         Padding(
              //           padding: const EdgeInsets.all(8.0),
              //           child: Container(
              //             height: Dimensions.screenHeight * 0.63,
              //             decoration: BoxDecoration(
              //               color: AppColors.white,
              //               borderRadius: BorderRadius.circular(Dimensions.radius20),
              //               boxShadow: const [
              //                 BoxShadow(
              //                   color: Colors.black87,
              //                   blurRadius: 10.0,
              //                   spreadRadius: 0.0,
              //                   offset: Offset(
              //                     0.0, // horizontal offset
              //                     0.0, // vertical offset
              //                   ),
              //                 ),
              //               ],
              //             ),
              //             child: Padding(
              //               padding: EdgeInsets.all(Dimensions.height20),
              //               child: Column(
              //                 children: [
              //                   Row(
              //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                     children: [
              //                       BigText(
              //                         text: 'Upcoming Appointments',
              //                         size: Dimensions.font15 * 1.15,
              //                         fontWeight: FontWeight.bold,
              //                       ),
              //                       BigText(
              //                           text: 'View All',
              //                           color: Colors.purple.shade800,
              //                           fontWeight: FontWeight.bold,
              //                           size: Dimensions.font15 * 1.15),
              //                     ],
              //                   ),
              //                   SizedBox(
              //                     height: Dimensions.height10,
              //                   ),
              //                   const Divider(
              //                     color: Colors.grey,
              //                     thickness: 1.0,
              //                     height: 20.0,
              //                     indent: 10.0,
              //                     endIndent: 10.0,
              //                   ),
              //                   SizedBox(
              //                     height: Dimensions.height10,
              //                   ),
              //                   Align(
              //                       alignment: Alignment.centerLeft,
              //                       child: BigText(
              //                         text: 'Today',
              //                         size: Dimensions.font15 * 1.2,
              //                       )),
              //                   SizedBox(
              //                     height: Dimensions.height10,
              //                   ),
              //                   Container(
              //                       height: Dimensions.height30 * 6.6,
              //                       decoration: BoxDecoration(
              //                         color: Colors.grey[200],
              //                       ),
              //                       padding: EdgeInsets.symmetric(
              //                           vertical: Dimensions.height10 / 2),
              //                       child: GetBuilder<HomePageController>(
              //                         builder: (controller) {
              //                           return ListView.builder(
              //                             padding:
              //                                 EdgeInsets.all(Dimensions.height10 / 2),
              //                             itemCount: controller.appointments.length,
              //                             itemBuilder: (context, index) {
              //                               return GestureDetector(
              //                                 onTap: () {
              //                                   controller.selectApt('Today', index);
              //                                   GetStorage().write('selectedApt',
              //                                       controller.appointments[index]);
              //                                 },
              //                                 child: Card(
              //                                   color: controller.selectedAptType ==
              //                                           'Today'
              //                                       ? controller.selectedAptIndex ==
              //                                               index
              //                                           ? Color.fromARGB(
              //                                               255, 160, 212, 255)
              //                                           : Colors.white
              //                                       : Colors.white,
              //                                   elevation: 2.0,
              //                                   shape: RoundedRectangleBorder(
              //                                     borderRadius:
              //                                         BorderRadius.circular(8.0),
              //                                   ),
              //                                   child: Padding(
              //                                     padding: const EdgeInsets.all(8.0),
              //                                     child: Column(
              //                                       children: [
              //                                         SizedBox(
              //                                           height:
              //                                               Dimensions.height15 / 2,
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             BigText(
              //                                               text: controller
              //                                                           .appointments[
              //                                                       index]['signers']
              //                                                   [0]['firstName'],
              //                                               fontWeight:
              //                                                   FontWeight.bold,
              //                                             ),
              //                                             const Spacer(),
              //                                             Icon(
              //                                               Icons.phone,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                             ),
              //                                             SizedBox(
              //                                               width: Dimensions.width10,
              //                                             ),
              //                                             Icon(
              //                                               Icons.message,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                               color: Colors.green,
              //                                             ),
              //                                           ],
              //                                         ),
              //                                         SizedBox(
              //                                           height: Dimensions.height15,
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             Icon(
              //                                               Icons.note_add_outlined,
              //                                               color:
              //                                                   const Color.fromARGB(
              //                                                       255,
              //                                                       179,
              //                                                       142,
              //                                                       247),
              //                                               size:
              //                                                   Dimensions.iconSize28,
              //                                             ),
              //                                             SizedBox(
              //                                               width: Dimensions.width10,
              //                                             ),
              //                                             SizedBox(
              //                                               width:
              //                                                   Dimensions.width40 *
              //                                                       3,
              //                                               child: const Expanded(
              //                                                 child: Text(
              //                                                     'General Notary - Acknowledgement'),
              //                                               ),
              //                                             ),
              //                                             const Spacer(),
              //                                             BigText(
              //                                               text: controller
              //                                                       .appointments[
              //                                                   index]['createdAt'],
              //                                               fontWeight:
              //                                                   FontWeight.bold,
              //                                               size:
              //                                                   Dimensions.font15 / 2,
              //                                             ),
              //                                           ],
              //                                         ),
              //                                         Padding(
              //                                           padding: EdgeInsets.symmetric(
              //                                               vertical:
              //                                                   Dimensions.height10 /
              //                                                       2),
              //                                           child: const SizedBox(
              //                                             width:
              //                                                 double.maxFinite * 0.9,
              //                                             child: Divider(
              //                                               color: Colors.grey,
              //                                               thickness: 1,
              //                                             ),
              //                                           ),
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             Icon(
              //                                               Icons.laptop,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                             ),
              //                                             SizedBox(
              //                                               width:
              //                                                   Dimensions.width10 /
              //                                                       2,
              //                                             ),
              //                                             BigText(
              //                                               text: 'online signing',
              //                                               size: Dimensions.font15 *
              //                                                   1.2,
              //                                             ),
              //                                             const Spacer(),
              //                                             BigText(
              //                                               text: 'USD 10',
              //                                               size: Dimensions.font15,
              //                                             )
              //                                           ],
              //                                         ),
              //                                         SizedBox(
              //                                           height:
              //                                               Dimensions.height15 / 2,
              //                                         ),
              //                                       ],
              //                                     ),
              //                                   ),
              //                                 ),
              //                               );
              //                             },
              //                           );
              //                         },
              //                       )),
              //                   SizedBox(
              //                     height: Dimensions.height10,
              //                   ),
              //                   Align(
              //                       alignment: Alignment.centerLeft,
              //                       child: BigText(
              //                         text: 'Tomorrow',
              //                         size: Dimensions.font15 * 1.2,
              //                       )),
              //                   SizedBox(
              //                     height: Dimensions.height10,
              //                   ),
              //                   Container(
              //                       height: Dimensions.height30 * 6.6,
              //                       decoration: BoxDecoration(
              //                         color: Colors.grey[200],
              //                       ),
              //                       padding: EdgeInsets.symmetric(
              //                           vertical: Dimensions.height10 / 2),
              //                       child: GetBuilder<HomePageController>(
              //                         builder: (controller) {
              //                           return ListView.builder(
              //                             padding:
              //                                 EdgeInsets.all(Dimensions.height10 / 2),
              //                             itemCount: controller.appointments.length,
              //                             itemBuilder: (context, index) {
              //                               return GestureDetector(
              //                                 onTap: () {
              //                                   controller.selectApt(
              //                                       'Tomorrow', index);
              //                                   GetStorage().write('selectedApt',
              //                                       controller.appointments[index]);
              //                                 },
              //                                 child: Card(
              //                                   color: controller.selectedAptType ==
              //                                           'Tomorrow'
              //                                       ? controller.selectedAptIndex ==
              //                                               index
              //                                           ? Color.fromARGB(
              //                                               255, 160, 212, 255)
              //                                           : Colors.white
              //                                       : Colors.white,
              //                                   elevation: 2.0,
              //                                   shape: RoundedRectangleBorder(
              //                                     borderRadius:
              //                                         BorderRadius.circular(8.0),
              //                                   ),
              //                                   child: Padding(
              //                                     padding: const EdgeInsets.all(8.0),
              //                                     child: Column(
              //                                       children: [
              //                                         SizedBox(
              //                                           height:
              //                                               Dimensions.height15 / 2,
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             BigText(
              //                                               text: controller
              //                                                           .appointments[
              //                                                       index]['signers']
              //                                                   [0]['firstName'],
              //                                               fontWeight:
              //                                                   FontWeight.bold,
              //                                             ),
              //                                             const Spacer(),
              //                                             Icon(
              //                                               Icons.phone,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                             ),
              //                                             SizedBox(
              //                                               width: Dimensions.width10,
              //                                             ),
              //                                             Icon(
              //                                               Icons.message,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                               color: Colors.green,
              //                                             ),
              //                                           ],
              //                                         ),
              //                                         SizedBox(
              //                                           height: Dimensions.height15,
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             Icon(
              //                                               Icons.note_add_outlined,
              //                                               color:
              //                                                   const Color.fromARGB(
              //                                                       255,
              //                                                       179,
              //                                                       142,
              //                                                       247),
              //                                               size:
              //                                                   Dimensions.iconSize28,
              //                                             ),
              //                                             SizedBox(
              //                                               width: Dimensions.width10,
              //                                             ),
              //                                             SizedBox(
              //                                               width:
              //                                                   Dimensions.width40 *
              //                                                       3,
              //                                               child: const Expanded(
              //                                                 child: Text(
              //                                                     'General Notary - Acknowledgement'),
              //                                               ),
              //                                             ),
              //                                             const Spacer(),
              //                                             BigText(
              //                                               text: controller
              //                                                       .appointments[
              //                                                   index]['createdAt'],
              //                                               fontWeight:
              //                                                   FontWeight.bold,
              //                                               size:
              //                                                   Dimensions.font15 / 2,
              //                                             ),
              //                                           ],
              //                                         ),
              //                                         Padding(
              //                                           padding: EdgeInsets.symmetric(
              //                                               vertical:
              //                                                   Dimensions.height10 /
              //                                                       2),
              //                                           child: const SizedBox(
              //                                             width:
              //                                                 double.maxFinite * 0.9,
              //                                             child: Divider(
              //                                               color: Colors.grey,
              //                                               thickness: 1,
              //                                             ),
              //                                           ),
              //                                         ),
              //                                         Row(
              //                                           children: [
              //                                             Icon(
              //                                               Icons.laptop,
              //                                               size:
              //                                                   Dimensions.iconSize24,
              //                                             ),
              //                                             SizedBox(
              //                                               width:
              //                                                   Dimensions.width10 /
              //                                                       2,
              //                                             ),
              //                                             BigText(
              //                                               text: 'online signing',
              //                                               size: Dimensions.font15 *
              //                                                   1.2,
              //                                             ),
              //                                             const Spacer(),
              //                                             BigText(
              //                                               text: 'USD 10',
              //                                               size: Dimensions.font15,
              //                                             )
              //                                           ],
              //                                         ),
              //                                         SizedBox(
              //                                           height:
              //                                               Dimensions.height15 / 2,
              //                                         ),
              //                                       ],
              //                                     ),
              //                                   ),
              //                                 ),
              //                               );
              //                             },
              //                           );
              //                         },
              //                       )),
              //                 ],
              //               ),
              //             ),
              //           ),
              //         ),
              //         SizedBox(
              //           height: Dimensions.height20,
              //         ),

              //         //? to remove later
              //         Align(
              //             alignment: Alignment.centerLeft,
              //             child: BigText(text: 'Furthur Info - :')),
              //         SizedBox(
              //           height: Dimensions.height15,
              //         ),
              //         Wrap(
              //           direction: Axis.horizontal,
              //           children: [
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Add Mileage",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => AddMileage.addMileageBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Mark as Complete",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => MarkComplete.markCompleteBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Expense Sheet",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => AddExpenseSheet.addExpenseBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Notorial Acts",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => NotorialAct.openNotorialBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "No Show UI",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => NoShow.openNoShowBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Cancel Invoice",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () =>
              //                   CancelInvoice.openCancelInvoiceBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Change Service",
              //                 color: Colors.black,
              //               ),
              //               onPressed: () => CostService.openCostServiceBottomSheet(),
              //             ),
              //             SizedBox(
              //               width: Dimensions.width10,
              //             ),
              //             ActionChip(
              //               backgroundColor: Colors.deepPurple.shade100,
              //               label: SmallText(
              //                 text: "Update Payment",
              //                 color: Colors.black,
              //               ),
              //               onPressed: (){}
              //                   // UpdatePayment.openUpdatePaymentBottomSheet(),
              //             ),
              //           ],
              //         ),
              //         SizedBox(
              //           height: Dimensions.height20,
              //         ),
              //         //? to remove later

              //         Align(
              //             alignment: Alignment.centerLeft,
              //             child: BigText(text: 'Quick Actions')),
              //         SizedBox(
              //           height: Dimensions.height15,
              //         ),
              //         Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //           children: [
              //             SizedBox(
              //               height: 50,
              //               // width: 180,
              //               child: Card(
              //                 elevation: 4.0,
              //                 shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(8.0),
              //                 ),
              //                 child: Padding(
              //                   padding: const EdgeInsets.all(8.0),
              //                   child: Row(
              //                     mainAxisSize: MainAxisSize.min,
              //                     children: [
              //                       const Icon(
              //                         Icons.local_hospital,
              //                         size: 30,
              //                         color: Colors.green,
              //                       ),
              //                       const SizedBox(width: 8.0),
              //                       Text(
              //                         'Book new appointment',
              //                         style: TextStyle(
              //                           fontWeight: FontWeight.bold,
              //                           fontSize: Dimensions.font15 * 0.9,
              //                         ),
              //                       ),
              //                     ],
              //                   ),
              //                 ),
              //               ),
              //             ),
              //             SizedBox(
              //               height: 50,
              //               // width: 180,
              //               child: Card(
              //                 elevation: 4.0,
              //                 shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(8.0),
              //                 ),
              //                 child: Padding(
              //                   padding: const EdgeInsets.all(8.0),
              //                   child: Row(
              //                     mainAxisSize: MainAxisSize.min,
              //                     children: [
              //                       const Icon(Icons.bus_alert_outlined,
              //                           size: 30, color: Colors.green),
              //                       const SizedBox(width: 8.0),
              //                       Text(
              //                         'Call ambulance',
              //                         style: TextStyle(
              //                           fontWeight: FontWeight.bold,
              //                           fontSize: Dimensions.font15 * 0.9,
              //                         ),
              //                       ),
              //                     ],
              //                   ),
              //                 ),
              //               ),
              //             )
              //           ],
              //         ),
              //         SizedBox(
              //           height: Dimensions.height30,
              //         ),
              //         Align(
              //             alignment: Alignment.centerLeft,
              //             child: BigText(
              //               text: 'Review your Past Signings',
              //               fontWeight: FontWeight.bold,
              //             )),
              //         SizedBox(
              //           height: Dimensions.height20,
              //         ),
              //         InkWell(
              //           onTap: () {},
              //           child: Container(
              //             width: double.maxFinite * 0.9,
              //             height: 50,
              //             decoration: BoxDecoration(
              //               border: Border.all(color: Colors.green.shade900),
              //               borderRadius:
              //                   BorderRadius.circular(Dimensions.radius20 / 4),
              //               color: const Color(0xFFe8f3db),
              //             ),
              //             child: Row(
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: 'Invoice Entry Required',
              //                       color: const Color(0xFF345b24),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.bold,
              //                     )),
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: '7 Signings',
              //                       color: const Color(0xFF345b24),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.w500,
              //                     )),
              //               ],
              //             ),
              //           ),
              //         ),
              //         SizedBox(
              //           height: Dimensions.height15,
              //         ),
              //         InkWell(
              //           onTap: () {},
              //           child: Container(
              //             width: double.maxFinite * 0.9,
              //             height: 50,
              //             decoration: BoxDecoration(
              //               border: Border.all(color: Colors.green.shade900),
              //               borderRadius:
              //                   BorderRadius.circular(Dimensions.radius20 / 4),
              //               color: const Color(0xFFe8f3db),
              //             ),
              //             child: Row(
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: 'Notarial Acts Not Entered',
              //                       color: const Color(0xFF345b24),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.bold,
              //                     )),
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: '7 Signings',
              //                       color: const Color(0xFF345b24),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.w500,
              //                     )),
              //               ],
              //             ),
              //           ),
              //         ),
              //         SizedBox(
              //           height: Dimensions.height15,
              //         ),
              //         InkWell(
              //           onTap: () {},
              //           child: Container(
              //             width: double.maxFinite * 0.9,
              //             height: 50,
              //             decoration: BoxDecoration(
              //               border: Border.all(color: const Color(0xFFff0405)),
              //               borderRadius:
              //                   BorderRadius.circular(Dimensions.radius20 / 4),
              //               color: const Color(0xFFfad3e0),
              //             ),
              //             child: Row(
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: 'Expense Entry Required',
              //                       color: const Color(0xFFff0405),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.bold,
              //                     )),
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: '7 Signings',
              //                       color: const Color(0xFFff0405),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.w500,
              //                     )),
              //               ],
              //             ),
              //           ),
              //         ),
              //         SizedBox(
              //           height: Dimensions.height15,
              //         ),
              //         InkWell(
              //           onTap: () {},
              //           child: Container(
              //             width: double.maxFinite * 0.9,
              //             height: 50,
              //             decoration: BoxDecoration(
              //               border: Border.all(color: const Color(0xFF4b5cba)),
              //               borderRadius:
              //                   BorderRadius.circular(Dimensions.radius20 / 4),
              //               color: const Color(0xFFd8dcf0),
              //             ),
              //             child: Row(
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: 'Mileage Entry Required',
              //                       color: const Color(0xFF4b5cba),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.bold,
              //                     )),
              //                 Padding(
              //                     padding: EdgeInsets.symmetric(
              //                         horizontal: Dimensions.height10),
              //                     child: BigText(
              //                       text: '7 Signings',
              //                       color: const Color(0xFF4b5cba),
              //                       size: Dimensions.font15,
              //                       fontWeight: FontWeight.w500,
              //                     )),
              //               ],
              //             ),
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
