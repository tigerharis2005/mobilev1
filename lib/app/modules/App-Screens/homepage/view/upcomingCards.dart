import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/controller/apt_details_controller.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/apt_details_view.dart';
import 'package:mobilev1/widgets/buttons.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/icons.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';

class UpcomingCard extends StatelessWidget {
  UpcomingCard({super.key, required this.data});
  var data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        GetStorage().write('selectedApt', data);
        Get.lazyPut(() => AptDetailsController());
        Get.to(() => AptDetailsPage(data: data));
      },
      child: CardView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(height: Dimensions.height10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(width: Dimensions.width10),
                  Text(
                    data['signers'][0]['firstName'],
                    style: Theme.of(context).textTheme.displaySmall,
                  ),
                  const Spacer(),
                  FaIcon(
                    FontAwesomeIcons.phone,
                    size: Dimensions.iconSize24,
                  ),
                  SizedBox(
                    width: Dimensions.width20,
                  ),
                  FaIcon(
                    FontAwesomeIcons.message,
                    size: Dimensions.iconSize24,
                    color: Colors.green,
                  ),
                  SizedBox(
                    width: Dimensions.width20,
                  ),
                ],
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              Row(
                children: [
                  MyIcon(
                    icon: FontAwesomeIcons.bookmark,
                    iconColor: const Color.fromARGB(255, 179, 142, 247),
                    onPressed: () {},
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'General Notary - Acknowledgement',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 15,
                            ),
                      ),
                      Text(
                        data['createdAt'],
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 14,
                            ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 5),
              Divider(
                color: Colors.grey.shade300,
                thickness: 1,
                endIndent: 10,
                indent: 10,
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                  FaIcon(
                    FontAwesomeIcons.laptop,
                    size: Dimensions.iconSize24,
                  ),
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                  Text(
                    'Online Signing',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const Spacer(),
                  Text(
                    'USD 10',
                    style: Theme.of(context)
                        .textTheme
                        .displaySmall!
                        .copyWith(fontSize: 16),
                  ),
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                ],
              ),
              SizedBox(
                height: Dimensions.height15 / 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
