import 'package:flutter/material.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';

class InvoiceCard extends StatelessWidget {
  InvoiceCard({
    required this.data,
    Key? key,
  }) : super(key: key);

  final dynamic data;

  @override
  Widget build(BuildContext context) {
    final invoiceNo = data["invoiceNo"] ?? "";
    final totalCost = data["totalCost"] ?? 0;

    return Padding(
      padding: EdgeInsets.only(
        bottom: Dimensions.height20,
        left: Dimensions.height10,
        right: Dimensions.height10,
      ),
      child: Container(
        height: Dimensions.screenHeight * 0.2,
        width: double.maxFinite,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(Dimensions.radius20),
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.all(Dimensions.height10),
          child: Column(
            children: [
              Row(
                children: [
                  BigText(
                    text: 'Invoice#: $invoiceNo',
                    size: Dimensions.font15,
                  ),
                  const Spacer(),
                  BigText(
                    text: 'USD $totalCost',
                    size: Dimensions.font18,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    width: Dimensions.width20,
                  ),
                ],
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Ashley Bolden',
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: Dimensions.height10 / 2),
              Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Real Estate - Buyer Packages',
                  size: Dimensions.font15,
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    width: Dimensions.width10 * 19,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BigText(
                          text: 'Issue Date: ${data["issuedDate"] ?? ""}',
                          size: Dimensions.font15 / 1.5,
                        ),
                        SizedBox(height: Dimensions.height10 / 2),
                        BigText(
                          text: 'Linked to Pro Signings Tile Company',
                          size: Dimensions.font15,
                        ),
                      ],
                    ),
                  ),
                  const Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      BigText(
                        text: 'Due Date: ${data["dueDate"] ?? ""}',
                        size: Dimensions.font15 * 0.5,
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      Container(
                        height: 40,
                        width: 110,
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.black),
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: BigText(
                              text: 'Mark as Paid',
                              size: Dimensions.font15,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
