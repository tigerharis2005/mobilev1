import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/controller/homepagecontroller.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/invoiceCard.dart';
<<<<<<< HEAD

import '../../../../../helper/loading.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../data/models/api_data_model/invoices_model.dart';
import '../../../../data/models/repo_response/repo_response.dart';
import '../../../../data/repo/invoices_repo.dart';

class InvoicesWidget extends StatefulWidget {
  const InvoicesWidget({super.key});
=======
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/big_text.dart';

class InvoicesWidget extends GetView<HomePageController> {
  late int status;
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344

  @override
  State<InvoicesWidget> createState() => _InvoicesWidgetState();
}

class _InvoicesWidgetState extends State<InvoicesWidget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getallInvoices();

  }

  var combinedArr = {};

  getallInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data =
    GetInvoicesForNotary(notaryId: GetStorage().read('userId'));
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      combinedArr = response.data['invoices'];
      setState(() {

      });
    }
  }

  getDueInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data =
    GetInvoicesForNotary(notaryId: GetStorage().read('userId'), status: 0);
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      print('success');
      combinedArr.clear();
      combinedArr = response.data;
      setState(() {

      });
    } else if (response.error != null) {
      print(response.error!.message);
    }
  }

  getPaidInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data =
    GetInvoicesForNotary(notaryId: GetStorage().read('userId'), status: 1);
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      print('success');
      combinedArr.clear();
      combinedArr = response.data;
      setState(() {

      });
    } else if (response.error != null) {
      print(response.error!.message);
    }
  }

  getOverdueInvoices() async {
    LoadingUtils.showLoader();
    InvoicesRepository repo = InvoicesRepository();
    GetInvoicesForNotary data =
    GetInvoicesForNotary(notaryId: GetStorage().read('userId'), status: 1);
    RepoResponse response = await repo.getInvoicesForNotary(data);
    LoadingUtils.hideLoader();
    if (response.data != null) {
      print('success');
      combinedArr.clear();
      combinedArr = response.data;
      setState(() {

      });
    } else if (response.error != null) {
      print(response.error!.message);
    }
  }


  Widget _buildContainer(int index, String text) {
    return GestureDetector(
<<<<<<< HEAD
      //onTap: () => controller.onContainerPressed(index),
        child: Container(
          decoration: BoxDecoration(
            // color: controller.selectedIndex.value == index
            //     ? const Color(0xFF9ba4d3)
            //     : Colors.white,
=======
      onTap: () => controller.onContainerPressed(index),
      child: Obx(
        () => Container(
          decoration: BoxDecoration(
            color: controller.selectedIndex.value == index
                ? const Color(0xFF9ba4d3)
                : Colors.white,
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
            border: Border.all(
              color: const Color(0xFF4453ad),
              width: 2,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
<<<<<<< HEAD
                horizontal: Dimensions.height40 * 0.6,
                vertical: Dimensions.height10),
=======
              horizontal: Dimensions.height40 * 0.6,
              vertical: Dimensions.height10,
            ),
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
            child: Center(
              child: Text(
                text,
                style: TextStyle(
<<<<<<< HEAD
                  // color: controller.selectedIndex.value == index
                  //     ? Colors.white
                  //     : Colors.black,
=======
                  color: controller.selectedIndex.value == index
                      ? Colors.white
                      : Colors.black,
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
                  fontSize: Dimensions.font20 * 0.8,
                ),
              ),
            ),
          ),
<<<<<<< HEAD
        )
=======
        ),
      ),
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
    );
  }

  Widget _buildDifficultyLevelButton(int difficultyIndex, String text) {
<<<<<<< HEAD
    return
      Container(
        padding: EdgeInsets.symmetric(
            vertical: Dimensions.height10,
            horizontal: Dimensions.height10 * 1.2),
        decoration: BoxDecoration(
          // color:
          //     controller.selectedDifficutlyLevelIndex.value == difficultyIndex
          //         ? const Color(0xFFdbc5fb)
          //         : Colors.grey[300],
          borderRadius: BorderRadius.circular(32),
        ),
        child: Text(
          text,
          style: TextStyle(
            // color: controller.selectedDifficutlyLevelIndex.value ==
            //         difficultyIndex
            //     ? const Color(0xFF7b3ef1)
            //     : Colors.grey[600],
            fontWeight: FontWeight.bold,
            fontSize: 14,
=======
    return GestureDetector(
      onTap: () async {
        controller.onButtonPressed(difficultyIndex);
        Dio dio = Dio();

        final Map<String, dynamic> data = {
          "notaryId": "643074200605c500112e0902",
        };

        String?
            status; // Declare status as a nullable variable with initial value null

        if (difficultyIndex == 0) {
          // All Tab
          status = null; // No need to specify a status value
        } else if (difficultyIndex == 1) {
          // Paid Tab
          status = "1";
        } else if (difficultyIndex == 2) {
          // Due Tab
          status = "0";
        } else if (difficultyIndex == 3) {
          // Overdue Tab
          status = "3";
        }

        if (status != null) {
          data["status"] = status;
        }

        Get.dialog(
          const Center(
            child: CircularProgressIndicator(),
          ),
        );

        await Future.delayed(const Duration(seconds: 1)); // Delay for 1 second

        try {
          final response = await dio.post(
            'https://staging.thenotary.app/invoices/getInvoicesForaNotaryId',
            data: jsonEncode(data),
          );
          if (response.statusCode == 200) {
            // Handle the successful response here
            print(response.data);

            // Extract totalCount from the response data
            final totalCount = response.data['totalCount'];
            controller.totalCount.value = totalCount;
          } else {
            // Handle other status codes if needed
            print('Request failed with status code: ${response.statusCode}');
          }
        } catch (error) {
          // Handle the error here
          print('Error: $error');
        } finally {
          Get.back(); // Close the progress bar dialog
        }
      },
      child: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(
            vertical: Dimensions.height10,
            horizontal: Dimensions.height10 * 1.2,
          ),
          decoration: BoxDecoration(
            color:
                controller.selectedDifficultyLevelIndex.value == difficultyIndex
                    ? const Color(0xFFdbc5fb)
                    : Colors.grey[300],
            borderRadius: BorderRadius.circular(32),
          ),
          child: Text(
            text,
            style: TextStyle(
              color: controller.selectedDifficultyLevelIndex.value ==
                      difficultyIndex
                  ? const Color(0xFF7b3ef1)
                  : Colors.grey[600],
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
          ),
        ),
      );

  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: Dimensions.height30 * 1.7,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: BigText(
            text: 'Invoices',
            size: Dimensions.font26,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        SizedBox(
          height: Dimensions.height40 * 3.7,
          width: double.maxFinite * 0.9,
          child: Card(
            elevation: 4.0,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _buildContainer(0, '30 days'),
                      _buildContainer(1, '45 days'),
                      _buildContainer(2, '60 days'),
                    ],
                  ),
                  SizedBox(
                    height: Dimensions.height10,
                  ),
                  Obx(
                    () => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BigText(
                          text: 'Paid: ${controller.paidCount}',
                          size: Dimensions.font15,
                        ),
                        SizedBox(
                          width: Dimensions.height15,
                        ),
                        BigText(
                          text: 'Due: ${controller.dueCount}',
                          size: Dimensions.font15,
                        ),
                        SizedBox(
                          width: Dimensions.height15,
                        ),
                        BigText(
                          text: 'Overdue: ${controller.overdueCount}',
                          size: Dimensions.font15,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: Dimensions.height20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
<<<<<<< HEAD
            InkWell(onTap: () => getallInvoices(), child: _buildDifficultyLevelButton(0, "All")),
            InkWell(onTap: () => getPaidInvoices(), child: _buildDifficultyLevelButton(1, "Paid")),
            InkWell(onTap: () => getDueInvoices(), child: _buildDifficultyLevelButton(2, "Due")),
            InkWell(onTap: () => getOverdueInvoices(),child: _buildDifficultyLevelButton(3, "OverDue")),
=======
            _buildDifficultyLevelButton(0, "All"),
            _buildDifficultyLevelButton(1, "Paid"),
            _buildDifficultyLevelButton(2, "Due"),
            _buildDifficultyLevelButton(3, "Overdue"),
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
          ],
        ),
        SizedBox(
          height: Dimensions.height20 * 1.2,
        ),

        combinedArr.length==0 ? Text(("No Data")) :
        Expanded(
          child: GetBuilder<HomePageController>(
            builder: (_) {
<<<<<<< HEAD
              return ListView.builder(
                itemCount: combinedArr['invoices'].length,
                itemBuilder: ((context, index) {
                  //return InvoiceCard(data: allPaidInvoices['invoices'][index],);
                  final data = combinedArr['invoices'][index];
                  return Padding(
                    padding: EdgeInsets.only(
                        bottom: Dimensions.height20,
                        left: Dimensions.height10,
                        right: Dimensions.height10),
                    child: Container(
                        height: Dimensions.screenHeight * 0.2,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 1.0,

                          ),
                          borderRadius: BorderRadius.circular(Dimensions.radius20),
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(Dimensions.height10),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  BigText(text: 'Invoice#: ${data["invoiceNo"]}',size: Dimensions.font15,),
                                  const Spacer(),
                                  BigText(
                                    text: 'USD ${data["totalCost"]}',
                                    size: Dimensions.font18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  SizedBox(
                                    width: Dimensions.width20,
                                  ),
                                  // Container(
                                  //   padding: EdgeInsets.symmetric(
                                  //       vertical: Dimensions.height10,
                                  //       horizontal: Dimensions.height10 * 1.2),
                                  //   decoration: BoxDecoration(
                                  //     color: const Color(0xFFdbc5fb),
                                  //     borderRadius: BorderRadius.circular(32),
                                  //   ),
                                  //   child: const Text(
                                  //     'Medium',
                                  //     style: TextStyle(
                                  //       color: Color(0xFF7b3ef1),
                                  //       fontWeight: FontWeight.bold,
                                  //       fontSize: 14,
                                  //     ),
                                  //   ),
                                  // ),
                                ],
                              ),
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: BigText(
                                    text: 'Ashley Bolden',
                                    fontWeight: FontWeight.bold,
                                  )),
                              SizedBox(height: Dimensions.height10/2,),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: BigText(
                                  text: 'Real Estate - Buyer Packages',
                                  size: Dimensions.font15,
                                ),
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: Dimensions.width10 * 19,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        BigText(
                                          text: 'Issue Date: ${data["issuedDate"]}',
                                          size: Dimensions.font15/1.5,
                                        ),
                                        SizedBox(height: Dimensions.height10/2,),
                                        BigText(
                                          text:
                                          'Linked to Pro Signings Tile Company',
                                          size: Dimensions.font15,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Spacer(),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      BigText(
                                        text: 'Due Date: ${data["dueDate"]}',
                                        size: Dimensions.font15*0.5,
                                      ),
                                      SizedBox(
                                        height: Dimensions.height10,
                                      ),
                                      Container(
                                        height: 40,
                                        width: 110,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2, color: Colors.black),
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius15),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Center(
                                              child: BigText(
                                                text: 'Mark as Paid',
                                                size: Dimensions.font15,
                                              )),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        ) // Your content goes here
                    ),
                  );
                }),
              );
=======
              List<dynamic> invoices = [];

              if (controller.selectedIndex.value == 0) {
                // All Tab
                invoices = controller.allInvoices ?? [];
              } else if (controller.selectedIndex.value == 1) {
                // Paid Tab
                invoices = controller.allInvoices != null
                    ? controller.allInvoices
                        .where((invoice) => invoice['status'] == "1")
                        .toList()
                    : [];
              } else if (controller.selectedIndex.value == 2) {
                // Due Tab
                invoices = controller.allInvoices != null
                    ? controller.allInvoices
                        .where((invoice) => invoice['status'] == "0")
                        .toList()
                    : [];
              } else if (controller.selectedIndex.value == 3) {
                // Overdue Tab
                invoices = controller.allInvoices != null
                    ? controller.allInvoices
                        .where((invoice) => invoice['status'] == "3")
                        .toList()
                    : [];
              }

              print('Selected Index: ${controller.selectedIndex.value}');
              print('Invoices: $invoices');

              return invoices.isNotEmpty
                  ? ListView.builder(
                      itemCount: invoices.length,
                      itemBuilder: (context, index) {
                        return InvoiceCard(data: invoices[index]);
                      },
                    )
                  : const Center(
                      child: Text(
                        'No invoices found',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    );
>>>>>>> 4ecda11dd8f0fc510b3d23ffcf7be6155bd1c344
            },
          ),
        ),
      ],
    );
  }
}




