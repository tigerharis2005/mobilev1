import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:mobilev1/app/modules/App-Screens/debug/view/debug_view.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/appointmentWidget.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/homeWidget.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/invoicesWidget.dart';
import 'package:mobilev1/app/modules/App-Screens/homepage/view/profileWidget.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/utils/dimensions.dart';
import '../controller/homepagecontroller.dart';

class HomePage extends GetView<HomePageController> {
  static const route = '/homepage';
  static launch() => Get.toNamed(route);
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(left: Dimensions.width15),
        child: GetBuilder<HomePageController>(
          builder: (controller) => IndexedStack(
            index: controller.bottomTabIndex.value,
            children: [
              HomeWidget(),
              AppointmentWidget(),
              InvoicesWidget(),
              ProfileWidget(),
              DebugView(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: GetBuilder<HomePageController>(
        builder: (controller) => Container(
          color: AppColors.kDarkBackgroundColor,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: GNav(
            selectedIndex: controller.bottomTabIndex.value,
            onTabChange: (int index) => controller.changeBottomTabIndex(index),
            tabBorderRadius: 30,
            iconSize: 18,
            backgroundColor: AppColors.kDarkBackgroundColor,
            color: AppColors.white, // unselected icon color
            activeColor: Colors.white, // selected icon and text color
            haptic: true,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            tabActiveBorder: Border.all(
                color: AppColors.grey, width: 0.9), // tab button border
            curve: Curves.easeOutExpo, // tab animation curves
            gap: 8,
            tabs: const [
              GButton(
                icon: FontAwesomeIcons.house,
                text: 'Home',
              ),
              GButton(
                icon: FontAwesomeIcons.solidCalendarCheck,
                text: 'Appointment',
              ),
              GButton(
                icon: FontAwesomeIcons.receipt,
                text: 'Invoice',
              ),
              GButton(
                icon: FontAwesomeIcons.userTie,
                text: 'Profile',
              ),
              GButton(
                icon: FontAwesomeIcons.spider,
                text: 'Debug',
              ),
            ],
          ),
        ),
      ),
      // bottomNavigationBar: Theme(
      //   data: ThemeData(
      //     splashColor: Colors.transparent,
      //     highlightColor: Colors.transparent,
      //   ),
      //   child: GetBuilder<HomePageController>(
      //     builder: (controller) {
      //       return BottomNavigationBar(
      //         elevation: 0,
      //         backgroundColor: Colors.white,
      //         onTap: (int index) {
      //           controller.changeBottomTabIndex(index);
      //         },
      //         type: BottomNavigationBarType.fixed,
      //         currentIndex: controller.bottomTabIndex,
      //         selectedItemColor: AppColors.blue,
      //         unselectedItemColor: Colors.grey,
      //         showSelectedLabels: false,
      //         showUnselectedLabels: false,
      //         items: [
      //           BottomNavigationBarItem(
      //             label: "Home",
      //             icon: Padding(
      //               padding: EdgeInsets.only(left: Dimensions.width10 / 2),
      //               child: const FaIcon(
      //                 FontAwesomeIcons.house,
      //                 size: 22,
      //               ),
      //             ),
      //           ),
      //           const BottomNavigationBarItem(
      //             label: "Signings",
      //             icon: FaIcon(
      //               FontAwesomeIcons.calendarCheck,
      //               size: 22,
      //               // color: Colors.grey,
      //             ),
      //           ),
      //           const BottomNavigationBarItem(
      //             label: "Invoices",
      //             icon: FaIcon(
      //               FontAwesomeIcons.receipt,
      //               size: 22,
      //             ),
      //           ),
      //           const BottomNavigationBarItem(
      //             label: "Profile",
      //             icon: FaIcon(
      //               FontAwesomeIcons.userTie,
      //               size: 22,
      //             ),
      //           ),
      //           BottomNavigationBarItem(
      //             label: "Debug",
      //             icon: FaIcon(
      //               FontAwesomeIcons.spider,
      //               color: Colors.red.shade300,
      //               size: 27,
      //             ),
      //           ),
      //         ],
      //       );
      //     },
      //   ),
      // ),
    );
  }
}
