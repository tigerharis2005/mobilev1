import 'package:flutter/material.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../accountpage/view/accountpageview.dart';
import '../../companypage/view/companypageview.dart';
import '../../contactpage/view/contactpageview.dart';
import '../../servicespage/view/servicespageview.dart';

class ProfileOptionsRow extends StatelessWidget {
  final String profileOptionName;
  final IconData profileOptionIcon;
  const ProfileOptionsRow({
    super.key,
    required this.profileOptionName,
    required this.profileOptionIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: Dimensions.width15,
        ),
        Icon(
          profileOptionIcon,
          size: Dimensions.iconSize24 * 1.2,
        ),
        SizedBox(
          width: Dimensions.width20,
        ),
        BigText(text: profileOptionName),
        const Spacer(),
        InkWell(
            onTap: (() {
              if (profileOptionName == "Account Settings") {
                AccountPage.launch();
              } else if (profileOptionName == "Contact List") {
                ContactPage.launch();
              } else if (profileOptionName == "Company List") {
                CompanyPage.launch();
              } else if (profileOptionName == "Services List") {
                ServicesPage.launch();
              }
            }),
            child: const Icon(Icons.arrow_forward_ios)),
        SizedBox(
          width: Dimensions.width15,
        )
      ],
    );
  }
}
