// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/big_text.dart';
import '../../../../../utils/dimensions.dart';
import '../controller/homepagecontroller.dart';
import 'completedCards.dart';
import 'upcomingCards.dart';

class AppointmentWidget extends GetView<HomePageController> {
  const AppointmentWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(right: Dimensions.height15),
        child: Column(
          children: [
            SizedBox(height: Dimensions.height10),
            Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Appointments',
                  style: Theme.of(context).textTheme.displayLarge,
                )),
            SizedBox(height: Dimensions.height10),
            TabBar(
              unselectedLabelColor: Colors.black,
              labelColor: AppColors.kButtonBackgroundColor,
              controller: controller.tabController,
              indicatorColor: AppColors.kButtonBackgroundColor,
              labelPadding: null,
              labelStyle: Theme.of(context).textTheme.displayMedium!.copyWith(
                  fontSize: Dimensions.font18, fontWeight: FontWeight.bold),
              unselectedLabelStyle: Theme.of(context)
                  .textTheme
                  .displayMedium!
                  .copyWith(
                      fontSize: Dimensions.font18, fontWeight: FontWeight.w600),
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: const [
                Tab(child: Text('Upcoming')),
                Tab(child: Text('Completed')),
              ],
            ),
            Expanded(
              child: TabBarView(
                controller: controller.tabController,
                children: [
                  GetBuilder<HomePageController>(
                    builder: (_) {
                      return ListView.separated(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          itemCount: controller.upcomingApointmentData.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                          itemBuilder: (BuildContext context, int index) {
                            return UpcomingCard(
                              data: controller.upcomingApointmentData[index],
                            );
                          });
                    },
                  ),
                  GetBuilder<HomePageController>(
                    builder: (_) {
                      return ListView.separated(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          itemCount: controller.completedApointmentData.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(
                                height: Dimensions.height10,
                              ),
                          itemBuilder: (BuildContext context, int index) {
                            return CompletedCard(
                              data: controller.completedApointmentData[index],
                            );
                          });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
