import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../apt_details_tab/controller/apt_details_controller.dart';
import '../../apt_details_tab/view/apt_details_view.dart';

class CompletedCard extends StatelessWidget {
  CompletedCard({super.key, required this.data});
  var data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.lazyPut(() => AptDetailsController());
        Get.to(() => AptDetailsPage(data: data));
        // GetStorage().write('selectedApt', data);
        // Get.dialog(FractionallySizedBox(
        //   heightFactor: 0.3,
        //   widthFactor: 0.7,
        //   child: Material(
        //     borderRadius: BorderRadius.circular(Dimensions.radius20),
        //     child: Center(
        //       child: Wrap(
        //         direction: Axis.horizontal,
        //         children: [
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Add Mileage",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => AddMileage.addMileageBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Mark as Complete",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => MarkComplete.markCompleteBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Expense Sheet",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => AddExpenseSheet.addExpenseBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Notorial Acts",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => NotorialAct.openNotorialBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "No Show UI",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => NoShow.openNoShowBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Cancel Invoice",
        //               color: Colors.black,
        //             ),
        //             onPressed: () =>
        //                 CancelInvoice.openCancelInvoiceBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Change Service",
        //               color: Colors.black,
        //             ),
        //             onPressed: () => CostService.openCostServiceBottomSheet(),
        //           ),
        //           SizedBox(
        //             width: Dimensions.width10,
        //           ),
        //           ActionChip(
        //             backgroundColor: Colors.deepPurple.shade100,
        //             label: SmallText(
        //               text: "Update Payment",
        //               color: Colors.black,
        //             ),
        //             onPressed: () =>
        //                 UpdatePayment.openUpdatePaymentBottomSheet(),
        //           ),
        //         ],
        //       ),
        //     ),
        //   ),
        // ));
      },
      child: CardView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height15 / 2,
              ),
              Row(
                children: [
                  BigText(
                    text: data['signers'][0]['firstName'],
                    fontWeight: FontWeight.bold,
                  ),
                  const Spacer(),
                  Icon(
                    Icons.phone,
                    size: Dimensions.iconSize24,
                  ),
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                  Icon(
                    Icons.message,
                    size: Dimensions.iconSize24,
                    color: Colors.green,
                  ),
                ],
              ),
              SizedBox(
                height: Dimensions.height15,
              ),
              Row(
                children: [
                  Icon(
                    Icons.note_add_outlined,
                    color: const Color.fromARGB(255, 179, 142, 247),
                    size: Dimensions.iconSize28,
                  ),
                  SizedBox(
                    width: Dimensions.width10,
                  ),
                  SizedBox(
                    width: Dimensions.width40 * 3,
                    child: const Expanded(
                      child: Text('General Notary - Acknowledgement'),
                    ),
                  ),
                  const Spacer(),
                  BigText(
                    text: data['createdAt'],
                    fontWeight: FontWeight.bold,
                    size: Dimensions.font15 / 2,
                  ),
                ],
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: Dimensions.height10 / 2),
                child: const SizedBox(
                  width: double.maxFinite * 0.9,
                  child: Divider(
                    color: Colors.grey,
                    thickness: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Icon(
                    Icons.laptop,
                    size: Dimensions.iconSize24,
                  ),
                  SizedBox(
                    width: Dimensions.width10 / 2,
                  ),
                  BigText(
                    text: 'online signing',
                    size: Dimensions.font15 * 1.2,
                  ),
                  const Spacer(),
                  BigText(
                    text: 'USD 10',
                    size: Dimensions.font15,
                  )
                ],
              ),
              SizedBox(
                height: Dimensions.height15 / 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
