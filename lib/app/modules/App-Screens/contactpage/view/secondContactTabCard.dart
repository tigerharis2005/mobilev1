import 'package:flutter/material.dart';

import '../../../../../utils/colors.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';

class SecondContactTabCard extends StatelessWidget {
  const SecondContactTabCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: Dimensions.screenHeight * 0.25,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(Dimensions.radius20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black87,
              blurRadius: 10.0,
              spreadRadius: 0.0,
              offset: Offset(
                0.0, // horizontal offset
                0.0, // vertical offset
              ),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(Dimensions.height20),
          child: Column(
            children: [
              Row(
                children: [
                  BigText(
                    text: 'Contact - second Tab Card (list view)',
                    fontWeight: FontWeight.w500,
                    size: Dimensions.font15,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
