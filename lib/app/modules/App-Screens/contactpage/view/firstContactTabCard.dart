import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/button.dart';

import '../../../../../utils/colors.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../controller/contactpagecontroller.dart';

class FirstContactTabCard extends GetView<ContactPageController> {
  final int index;
  const FirstContactTabCard(this.index, {super.key});

  @override
  Widget build(BuildContext context) {
    return controller.getContactsdata[index]['type']=='Customer'? Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.maxFinite,
        height: Dimensions.screenHeight * 0.25,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(Dimensions.radius20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black87,
              blurRadius: 10.0,
              spreadRadius: 0.0,
              offset: Offset(
                0.0, // horizontal offset
                0.0, // vertical offset
              ),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(Dimensions.height20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  const Icon(Icons.work_outline_outlined,color: Colors.blueAccent,),
                  SizedBox(width: Dimensions.width15,),
                  BigText(
                    text: 'Customer',
                    fontWeight: FontWeight.w500,
                    size: Dimensions.font15,
                    color: Colors.blueAccent,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BigText(text: controller.getContactsdata[index]['firstName'],color: Colors.black,fontWeight: FontWeight.bold,size: Dimensions.font20/1.1,),
                  Row(
                    children: [
                      const Icon(Icons.email_outlined,color: Colors.blueAccent,),
                      SizedBox(width: Dimensions.width20,),
                      const Icon(Icons.phone_outlined,color: Colors.blueAccent,),
                    ],
                  ),
                ], 
              ),
              BigText(text: 'Assoiciated with',color: Colors.black,size: Dimensions.font15,),
              if(controller.getContactsdata[index]['companyId']!=null) BigText(text: controller.getContactsdata[index]['companyName']?? 'Not mentioned',color: Colors.black,size: Dimensions.font15,),
              if(controller.getContactsdata[index]['companyId']==null) Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BigText(text: 'No Company Linked',color: Colors.black,size: Dimensions.font15,),
                  Button(on_pressed: (){}, text: 'Link Company',width: Dimensions.width40*3,height: Dimensions.height20*2,)
                ],
              ),
              if(controller.getContactsdata[index]['companyId']!=null) BigText(text: 'Address: ${controller.getContactsdata[index]["city"]}',color: Colors.black,size: Dimensions.font15,),
              BigText(text: 'Date: ${controller.getContactsdata[index]["createdOn"]}',color: Colors.black,size: Dimensions.font15,),
            ],
          ),
        ),
      ),
    ):signer();
  }


  Widget signer(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.maxFinite,
        height: Dimensions.screenHeight * 0.2,
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(Dimensions.radius20),
          boxShadow: const [
            BoxShadow(
              color: Colors.black87,
              blurRadius: 10.0,
              spreadRadius: 0.0,
              offset: Offset(
                0.0, // horizontal offset
                0.0, // vertical offset
              ),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(Dimensions.height20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  const Icon(Icons.person_outlined,color: Colors.blueAccent,),
                  SizedBox(width: Dimensions.width15,),
                  BigText(
                    text: 'Signer',
                    fontWeight: FontWeight.w500,
                    size: Dimensions.font15,
                    color: Colors.blueAccent,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BigText(text: controller.getContactsdata[index]['firstName'],color: Colors.black,fontWeight: FontWeight.bold,size: Dimensions.font20/1.1,),
                  Row(
                    children: [
                      const Icon(Icons.email_outlined,color: Colors.blueAccent,),
                      SizedBox(width: Dimensions.width20,),
                      const Icon(Icons.phone_outlined,color: Colors.blueAccent,),
                    ],
                  ),
                ],
              ),
              BigText(text: 'Created At: ${controller.getContactsdata[index]["createdOn"]}',color: Colors.black,size: Dimensions.font15,),
            ],
          ),
        ),
      ),
    );
  }
}
