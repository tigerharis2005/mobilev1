import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/contacts_model.dart';
import 'package:mobilev1/app/data/models/api_data_model/invoices_model.dart';
import 'package:mobilev1/app/data/models/repo_response/repo_response.dart';
import 'package:mobilev1/app/data/repo/contacts_repo.dart';
import 'package:mobilev1/app/data/repo/invoices_repo.dart';
import 'package:mobilev1/helper/loading.dart';

class ContactPageController extends GetxController
with GetTickerProviderStateMixin{
  var getContactsdata=[];

  final List<String> items = ['Item 1', 'Item 2', 'Item 3'];
  late TabController tabController;
  @override
  void onReady() {
    getContacts();
  }

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(length: 3, vsync: this);
    
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }


  
  getContacts()async{
    LoadingUtils.showLoader();
    GetContacts data=GetContacts(notaryId: GetStorage().read('userId'));
    ContactsRepository repo =ContactsRepository();
    RepoResponse response=await repo.getContacts(data);
    LoadingUtils.hideLoader();
    if(response.data!=null){
      getContactsdata=response.data['leads'];
      update();
    }
    
  }
  var getInvoicesdataOfContact={};


  getInvoicesofContact()async{
    LoadingUtils.showLoader();
    InvoicesForContact data=InvoicesForContact(leadId: GetStorage().read('userId'));
    InvoicesRepository repo =InvoicesRepository();
    RepoResponse response=await repo.invoicesForContact(data);
    if(response.data!=null){
      getInvoicesdataOfContact=response.data;
    }
    LoadingUtils.hideLoader();
  }
  updateContact()async{
    LoadingUtils.showLoader();
    Map<String, Object> data={
    "leadId": "638723042d34fa11e1b45c25",
    "notaryId":"63871e8731e3dd0f77cdaa1e",
    "editObj": {
          "firstName": "",  
          "lastName": "asdf",
		   "imageURL": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7GoLl5TulaocWLQ8pi__zObTN8Sj5PmFvec-6NEPb&s",
            "email":"Jonny455@gmail.com",
            "phoneNumber":9986758348,
            "type": "customer",
            "labels":["Title Agency", "Houston"],
            "bio": "asdf",
            "city": "asdf",
            "state": "asdf",
            "zipCode": 123123
          
    }
    
};
    ContactsRepository repo =ContactsRepository();
    RepoResponse response=await repo.updateContacts(data);
    // if(response.data!=null){
    //   getInvoicesdataOfContact=response.data;
    // }
    LoadingUtils.hideLoader();
  }



  addContact(String? email,String? imageUrl,String? bio,String? firstName,String? lastName,String? type,String? companyId,int? phoneNumber,List<String>? labels,String? city,int? zipCode,String? state,String? source)async{
    LoadingUtils.showLoader();
    AddContacts data=AddContacts(notaryId: GetStorage().read('userId'),email: email,imageURL: imageUrl,bio: bio,firstName: firstName,labels: labels,lastName: lastName,companyId: companyId,phoneNumber: phoneNumber,city: city,zipCode: zipCode,state: state,source: source);
    ContactsRepository repo =ContactsRepository();
    RepoResponse response=await repo.addContacts(data);
    // if(response.data!=null){
    //   getInvoicesdataOfContact=response.data;
    // }
    LoadingUtils.hideLoader();
  }
}