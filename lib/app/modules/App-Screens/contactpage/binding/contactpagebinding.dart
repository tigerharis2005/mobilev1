import 'package:get/get.dart';

import '../controller/contactpagecontroller.dart';


class ContactPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ContactPageController());
  }
}
