import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AptDetailsController extends GetxController
    with GetTickerProviderStateMixin {
  late TabController tabController;

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(length: 3, vsync: this);
  }

  var apt;
  void aptDetail(var data) {
    apt = data;
    update();
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }

  String markAsCompletedText =
      'You can edit final amount in next step, Thank you email will be sent to signers & Witnessess';
  String startMileageText =
      'Please make sure to turn on GPS during the Notarisation. You can edit it later as well';
  String addExpensesText =
      'You had added expense of USD 120 | You marked expenses as no needed';
  String addNotarialActsText =
      'You had added expense of USD 120 | You marked expenses as no needed';
  String updatePaymentStatusText = 'Invoice of USD 120 is due for this signing';
  String completedAndPaymentReceivedText =
      'Signing is complete & payment has been received';
  String uncollectableText = 'You marked the signing as uncollectable';
  String cancelledWithInvoiceText =
      'You had raised a cancellation invoice of USD 50 on this signing';
  String cancelledText = 'You marked the signing as cancelled';
  String mileageTrackingText = 'Mileage Tracking in progress';
  String miDriverText = 'You are saving USD 14 on this dollar';
}
