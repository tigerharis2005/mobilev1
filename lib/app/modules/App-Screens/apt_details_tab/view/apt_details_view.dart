// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/activity_log/views/activity_log.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/info_view.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/signers_view.dart';
import 'package:mobilev1/utils/colors.dart';
import 'package:mobilev1/widgets/big_text.dart';

import '../../../../../utils/dimensions.dart';
import '../controller/apt_details_controller.dart';

class AptDetailsPage extends GetView<AptDetailsController> {
  final data;
  // static const route = '/aptDetails-page';
  // static launch() => Get.toNamed(route);
  const AptDetailsPage({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    controller.aptDetail(data);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.kScaffoldBackgroundColor,
          leading: IconButton(
            icon: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.black,
              size: Dimensions.iconSize20,
            ),
            onPressed: () => Get.back(),
          ),
          elevation: 0,
          centerTitle: false,
          title: Text(
            'AptDetails List',
            style: Theme.of(context).textTheme.displayLarge,
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
          child: Column(
            children: [
              TabBar(
                unselectedLabelColor: Colors.black,
                labelColor: const Color(0xFF0D47A1),
                controller: controller.tabController,
                indicatorColor: const Color(0xFF0D47A1),
                labelPadding: null,
                labelStyle: Theme.of(context).textTheme.displayMedium!.copyWith(
                    fontSize: Dimensions.font18, fontWeight: FontWeight.bold),
                unselectedLabelStyle: Theme.of(context)
                    .textTheme
                    .displayMedium!
                    .copyWith(
                        fontSize: Dimensions.font18,
                        fontWeight: FontWeight.w600),
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: const [
                  Tab(child: Text('Info')),
                  Tab(child: Text('Signers')),
                  Tab(child: Text('Activity Log')),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: controller.tabController,
                  children: [
                    InfoTab(),
                    SignersTab(),
                    ActivityLog(),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
