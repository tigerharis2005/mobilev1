import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/big_text.dart';

import '../../../../../../utils/dimensions.dart';
import '../../controller/apt_details_controller.dart';

class MileageTrackingCard extends StatelessWidget {
  const MileageTrackingCard({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(
      () => AptDetailsController(),
    );
    AptDetailsController controller = Get.find<AptDetailsController>();
    return SizedBox(
      // width: double.maxFinite,
      // height: Dimensions.height40*2,
      child: Column(
        children: [
          SizedBox(
            height: Dimensions.height10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: Dimensions.width30,
                height: Dimensions.height30,
                decoration: const BoxDecoration(
                  color: Colors.yellow, // dark green background color
                  shape: BoxShape.circle, // circular shape
                ),
                child: const Icon(
                  Icons.taxi_alert,
                  color: Colors.black, // light green arrow color
                ),
              ),
              SizedBox(
                width: Dimensions.width10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BigText(
                    text: 'Mileage Tracking',
                    size: Dimensions.font20 * 0.96,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: Dimensions.height10,
                  ),
                  SizedBox(
                    width: Dimensions.width10 * 22,
                    child: Expanded(
                      child: Text(controller.mileageTrackingText),
                    ),
                  ),
                ],
              ),
              const Spacer(),
              Icon(
                Icons.square,
                color: Colors.orange,
                size: Dimensions.iconSize28 * 1.5,
              )
            ],
          ),
          SizedBox(
            height: Dimensions.height10,
          ),
          SizedBox(
            width: double.maxFinite * 0.7,
            child: Divider(
              indent: 5,
              endIndent: 5,
              color: Colors.grey.shade200,
              thickness: 1,
            ),
          )
        ],
      ),
    );
  }
}
