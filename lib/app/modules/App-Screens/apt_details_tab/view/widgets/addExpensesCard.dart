import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../../utils/dimensions.dart';
import '../../../../bottom-sheets/Expense-Sheet/View/add_expense.dart';
import '../../controller/apt_details_controller.dart';

class AddExpensesCard extends StatelessWidget {
  const AddExpensesCard({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(
      () => AptDetailsController(),
    );
    AptDetailsController controller = Get.find<AptDetailsController>();
    return GestureDetector(
      onTap: () {
        AddExpenseSheet.addExpenseBottomSheet(context);
      },
      child: SizedBox(
        // width: double.maxFinite,
        // height: Dimensions.height40*2,
        child: Column(
          children: [
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: Dimensions.width40,
                  height: Dimensions.height40,
                  decoration: BoxDecoration(
                    color: Colors.green[900], // dark green background color
                    shape: BoxShape.circle, // circular shape
                  ),
                  child: Center(
                    child: FaIcon(
                      FontAwesomeIcons.sackDollar,
                      size: Dimensions.iconSize20,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmallText(
                      text: 'Add Expenses',
                      size: Dimensions.font20 * 0.94,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 5),
                    SizedBox(
                      width: Dimensions.width10 * 22,
                      child: Flexible(
                        child: Text(
                          controller.addExpensesText,
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                FaIcon(
                  FontAwesomeIcons.plus,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height10,
            ),
            SizedBox(
              width: double.maxFinite * 0.7,
              child: Divider(
                indent: 5,
                endIndent: 5,
                color: Colors.grey.shade200,
                thickness: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
