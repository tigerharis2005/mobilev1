import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/mileageTracker/views/mileage_tracker.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../../utils/dimensions.dart';
import '../../controller/apt_details_controller.dart';

class StartMileageCard extends StatelessWidget {
  const StartMileageCard({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(
      () => AptDetailsController(),
    );
    AptDetailsController controller = Get.find<AptDetailsController>();
    return GestureDetector(
      onTap: () => Get.to(() => const MileageTracker()),
      child: SizedBox(
        // width: double.maxFinite,
        // height: Dimensions.height40*2,
        child: Column(
          children: [
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: Dimensions.width40,
                  height: Dimensions.height40,
                  decoration: const BoxDecoration(
                    color: Colors.yellow, // dark green background color
                    shape: BoxShape.circle, // circular shape
                  ),
                  child: Center(
                    child: FaIcon(
                      FontAwesomeIcons.taxi,
                      size: Dimensions.iconSize20,
                    ),
                  ),
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmallText(
                      text: 'Start Mileage',
                      size: Dimensions.font20 * 0.96,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 5),
                    SizedBox(
                      width: Dimensions.width10 * 22,
                      child: Flexible(
                        child: Text(
                          controller.startMileageText,
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                FaIcon(
                  FontAwesomeIcons.play,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height10,
            ),
            SizedBox(
              width: double.maxFinite * 0.7,
              child: Divider(
                indent: 5,
                endIndent: 5,
                color: Colors.grey.shade200,
                thickness: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
