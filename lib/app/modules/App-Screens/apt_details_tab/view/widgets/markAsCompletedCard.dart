import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../../utils/dimensions.dart';
import '../../../../bottom-sheets/Mark-As-Complete/View/mark_complete_bottomSheet.dart';
import '../../controller/apt_details_controller.dart';

class MarkAsCompletedCard extends GetView<AptDetailsController> {
  const MarkAsCompletedCard({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        MarkComplete.markCompleteBottomSheet(context);
      },
      child: SizedBox(
        // width: double.maxFinite,
        // height: Dimensions.height40*2,
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: Dimensions.width40,
                  height: Dimensions.height40,
                  decoration: BoxDecoration(
                    color: Colors.green[900], // dark green background color
                    shape: BoxShape.circle, // circular shape
                  ),
                  child: Center(
                    child: FaIcon(
                      FontAwesomeIcons.marker,
                      color: Colors.white,
                      size: Dimensions.iconSize20,
                    ),
                  ),
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SmallText(
                      text: 'Mark As Completed',
                      size: Dimensions.font20 * 0.96,
                      fontWeight: FontWeight.bold,
                    ),
                    const SizedBox(height: 5),
                    SizedBox(
                      width: Dimensions.width10 * 26,
                      child: Flexible(
                        child: Text(
                          controller.markAsCompletedText,
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: Dimensions.height10,
            ),
            SizedBox(
              width: double.maxFinite * 0.7,
              child: Divider(
                indent: 5,
                endIndent: 5,
                color: Colors.grey.shade200,
                thickness: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
