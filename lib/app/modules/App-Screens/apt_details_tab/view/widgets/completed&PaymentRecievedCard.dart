import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/big_text.dart';

import '../../../../../../utils/dimensions.dart';
import '../../controller/apt_details_controller.dart';

class CompletedAndPaymentReceivedCard extends StatelessWidget {
  const CompletedAndPaymentReceivedCard({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(
      () => AptDetailsController(),
    );
    AptDetailsController controller = Get.find<AptDetailsController>();
    return SizedBox(
      // width: double.maxFinite,
      // height: Dimensions.height40*2,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: Dimensions.width30,
                height: Dimensions.height30,
                decoration: BoxDecoration(
                  color: Colors.green[900], // dark green background color
                  shape: BoxShape.circle, // circular shape
                ),
                child: Icon(
                  Icons.check,
                  color: Colors.green[100], // light green arrow color
                ),
              ),
              SizedBox(
                width: Dimensions.width10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BigText(
                    text: 'Completed & Payment Received',
                    size: Dimensions.font20 * 0.96,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: Dimensions.height10,
                  ),
                  SizedBox(
                    width: Dimensions.width10*26,
                    child: Expanded(
                      child: Text(controller.completedAndPaymentReceivedText),
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: Dimensions.height10,
          ),
          const SizedBox(
            width: double.maxFinite * 0.9,
            child: Divider(
              color: Colors.grey,
              thickness: 1,
            ),
          )
        ],
      ),
    );
  }
}
