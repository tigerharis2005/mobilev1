// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/controller/apt_details_controller.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';

class SignersTab extends GetView<AptDetailsController> {
  const SignersTab({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: Dimensions.height10,
        ),
        signersCard(),
        SizedBox(
          height: Dimensions.height10,
        ),
        witnessesCard(),
        SizedBox(
          height: Dimensions.height10,
        ),
      ],
    );
  }

  Widget signersCard() {
    return CardView(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Row(
              children: [
                BigText(
                  text: 'Signers',
                  size: Dimensions.font15 * 1.4,
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    Get.dialog(
                      Dialog(
                        child: Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              BigText(text: 'Add Signer'),
                              SizedBox(
                                height: Dimensions.height15,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Name',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Email',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Phone',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              ElevatedButton(
                                onPressed: () {
                                  // save the form data
                                  Get.back();
                                },
                                child: Text('Save'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  child: BigText(
                    text: 'Add More',
                    size: Dimensions.font15,
                    color: Colors.green,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height10,
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'There are 1 signer for this appointment',
                  size: Dimensions.font15 * 1.1,
                  color: const Color.fromARGB(255, 138, 138, 138),
                )),
            SizedBox(
              height: Dimensions.height20,
            ),
            Row(
              children: <Widget>[
                CircleAvatar(
                  radius: Dimensions.radius20 * 1.2,
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/2919/2919906.png'),
                ),
                SizedBox(width: Dimensions.width15),
                BigText(text: controller.apt['signers'][0]['firstName']),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    Get.dialog(
                      Dialog(
                        child: Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              BigText(text: 'Edit Signer'),
                              SizedBox(
                                height: Dimensions.height15,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Name',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Email',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Phone',
                                ),
                              ),
                              SizedBox(height: Dimensions.height20),
                              ElevatedButton(
                                onPressed: () {
                                  // save the form data
                                  Get.back();
                                },
                                child: Text('Save'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  child: FaIcon(
                    FontAwesomeIcons.pen,
                    size: Dimensions.iconSize20,
                  ),
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                FaIcon(
                  FontAwesomeIcons.xmark,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height30,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Dimensions.width10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FaIcon(
                    FontAwesomeIcons.envelope,
                    size: Dimensions.iconSize24,
                  ),
                  FaIcon(
                    FontAwesomeIcons.message,
                    size: Dimensions.iconSize24,
                  ),
                  FaIcon(
                    FontAwesomeIcons.phone,
                    size: Dimensions.iconSize24,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget witnessesCard() {
    return CardView(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Row(
              children: [
                BigText(
                  text: 'Witnesses',
                  size: Dimensions.font15 * 1.4,
                ),
                const Spacer(),
                BigText(
                  text: 'Add More',
                  size: Dimensions.font15,
                  color: Colors.green,
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height20,
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'There are no witnesses',
                  size: Dimensions.font15 * 1.2,
                ))
          ],
        ),
      ),
    );
  }
}
