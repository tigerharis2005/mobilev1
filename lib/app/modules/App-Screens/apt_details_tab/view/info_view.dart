// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/controller/apt_details_controller.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/addExpensesCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/addNotarialActsCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/cancelledCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/cancelledWithInvoiceCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/completed&PaymentRecievedCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/markAsCompletedCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/startMileageCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/uncollectableCard.dart';
import 'package:mobilev1/app/modules/App-Screens/apt_details_tab/view/widgets/updatePaymentStatusCard.dart';
import 'package:mobilev1/widgets/card.dart';

import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';

class InfoTab extends GetView<AptDetailsController> {
  const InfoTab({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          SizedBox(
            height: Dimensions.height10,
          ),
          signerInfoCard(context),
          SizedBox(
            height: Dimensions.height10,
          ),
          dateTimeServiceInfoCard(),
          SizedBox(
            height: Dimensions.height20,
          ),
          infoMainCard(),
          SizedBox(
            height: Dimensions.height20,
          ),
          controller.apt['isOnlineSigning'] == false
              ? signingLocationCard()
              : onlineSigningCard(),
          SizedBox(
            height: Dimensions.height20,
          ),
          if (controller.apt['serviceDetails']['serviceId'] == 'LSA_ONLINE' ||
              controller.apt['serviceDetails']['serviceId'] == 'LSA_OFFLINE')
            titleCompanyDetails(),
          SizedBox(
            height: Dimensions.height20,
          ),
        ],
      ),
    );
  }

  Widget signerInfoCard(BuildContext context) {
    return CardView(
      margin: 2,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Signer Info',
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                        color: Colors.grey.shade800,
                        fontSize: Dimensions.font18,
                      ),
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              children: <Widget>[
                CircleAvatar(
                  radius: Dimensions.radius20 * 1.4,
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/2919/2919906.png'),
                ),
                SizedBox(width: 16.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    BigText(
                      text: controller.apt['signers'][0]['firstName'],
                      fontWeight: FontWeight.bold,
                      size: Dimensions.font20,
                    ),
                    Text(
                      controller.apt['signers'][0]['phoneNumber'],
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            fontSize: Dimensions.font18,
                          ),
                    ),
                  ],
                ),
                const Spacer(),
                FaIcon(
                  FontAwesomeIcons.envelope,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                FaIcon(
                  FontAwesomeIcons.phone,
                  size: Dimensions.iconSize20,
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget dateTimeServiceInfoCard() {
    return CardView(
      margin: 2,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            SizedBox(
              height: Dimensions.height10,
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: controller.apt['createdAt'],
                  size: Dimensions.font15 * 1.1,
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Apt. Type: In Person',
                  size: Dimensions.font15 * 1.1,
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Service: Real Estate Notarisation',
                  size: Dimensions.font15 * 1.1,
                )),
          ],
        ),
      ),
    );
  }

  Widget infoMainCard() {
    return CardView(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            if (controller.apt['status'] == 0) MarkAsCompletedCard(),
            if (controller.apt['status'] == 1) UpdatePaymentStatusCard(),
            if (controller.apt['status'] == 2)
              CompletedAndPaymentReceivedCard(),
            if (controller.apt['status'] == 3) UncollectableCard(),
            if (controller.apt['status'] == 4) CancelledWithInvoiceCard(),
            if (controller.apt['status'] == 5) CancelledCard(),
            if (controller.apt['isOnlineSigning'] == false) StartMileageCard(),
            AddExpensesCard(),
            AddNotarialActsCard(),
          ],
        ),
      ),
    );
  }

  Widget onlineSigningCard() {
    return CardView(
      margin: 2,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Online Signings',
                  size: Dimensions.font15 * 1.1,
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.laptop,
                  color: Colors.black,
                  size: Dimensions.iconSize28 * 1.2,
                ),
                SizedBox(width: 16.0),
                SizedBox(
                  width: 180,
                  child: BigText(
                    text: 'This is an online meeting.',
                    size: Dimensions.font15 * 1.05,
                    color: Colors.grey.shade600,
                  ),
                ),
                const Spacer(),
                Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      color: Color(0xFFe4edfe),
                      borderRadius: BorderRadius.circular(Dimensions.radius15)),
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: BigText(
                      text: 'Join',
                      color: Color(0xFF706bee),
                      size: Dimensions.font20,
                    ),
                  )),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget signingLocationCard() {
    return CardView(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Signing Location',
                  size: Dimensions.font15 * 1.1,
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.buildingUser,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(width: 15.0),
                SizedBox(
                  width: 180,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      BigText(
                        text: controller.apt['propertyAddress'],
                        fontWeight: FontWeight.bold,
                        size: Dimensions.font18,
                      ),
                      SizedBox(height: Dimensions.height10 * 0.4),
                      BigText(
                        text: '3.45 miles from your home location',
                        size: Dimensions.font15 * 1.05,
                        color: Colors.grey.shade600,
                      )
                    ],
                  ),
                ),
                const Spacer(),
                IconButton.filled(
                  padding: EdgeInsets.all(5),
                  icon: Center(
                    child: IconButton(
                      onPressed: () {},
                      icon: FaIcon(
                        FontAwesomeIcons.locationDot,
                        size: Dimensions.iconSize24,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget titleCompanyDetails() {
    return CardView(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: BigText(
                  text: 'Title Company Details',
                  size: Dimensions.font15 * 1.1,
                )),
            SizedBox(
              height: Dimensions.height10,
            ),
            Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.user,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(width: 16.0),
                SizedBox(
                  width: 180,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      BigText(
                        text: 'Claire Anderson',
                        fontWeight: FontWeight.bold,
                        size: Dimensions.font18,
                      ),
                      SizedBox(height: Dimensions.height10 * 0.4),
                      BigText(
                        text: 'From TitleAgency name',
                        size: Dimensions.font15 * 1.05,
                        color: Colors.grey.shade600,
                      )
                    ],
                  ),
                ),
                const Spacer(),
                FaIcon(
                  FontAwesomeIcons.envelope,
                  size: Dimensions.iconSize24,
                ),
                SizedBox(
                  width: Dimensions.width20,
                ),
                FaIcon(
                  FontAwesomeIcons.phone,
                  size: Dimensions.iconSize20,
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
