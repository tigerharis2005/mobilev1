import 'package:get/get.dart';

import '../controller/apt_details_controller.dart';


class AptDetailsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AptDetailsController());
  }
}
