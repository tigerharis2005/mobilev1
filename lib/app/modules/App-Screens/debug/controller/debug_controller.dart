import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:get/get.dart';

class DebugController extends GetxController {
  void initNotifications() {
    AwesomeNotifications().isNotificationAllowed().then((value) {
      //* if the user has not allowed notifications, we request permission
      if (!value) {
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });
  }

  @override
  void onInit() {
    super.onInit();
    initNotifications();
  }
}
