import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:mobilev1/app/modules/App-Screens/activity_log/views/activity_log.dart';
import 'package:mobilev1/app/modules/App-Screens/mileageTracker/views/mileage_tracker.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Bill-To/views/bill_to_view.dart';
import 'package:mobilev1/app/modules/bottom-sheets/Bill-from/views/bill_from_view.dart';
import 'package:mobilev1/services/notifications/local/local_notifications.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../widgets/big_text.dart';
import '../../../bottom-sheets/Add-Mileage/View/add_mileage.dart';
import '../../../bottom-sheets/Cancel-Invoice/Views/cancel_invoice.dart';
import '../../../bottom-sheets/Cost-Service/Views/cost_service_view.dart';
import '../../../bottom-sheets/Expense-Sheet/View/add_expense.dart';
import '../../../bottom-sheets/Mark-As-Complete/View/mark_complete_bottomSheet.dart';
import '../../../bottom-sheets/No-Show/Views/no_show.dart';
import '../../../bottom-sheets/Notorial-Acts/Views/notorial_act.dart';
import '../../../bottom-sheets/Update-Payment/Views/update_payment.dart';
import '../controller/debug_controller.dart';

class DebugView extends GetView<DebugController> {
  const DebugView({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.center,
              child: SmallText(
                text: "Tab To be removed later",
                color: Colors.grey,
                size: 22,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: Dimensions.height40),
            Align(
              alignment: Alignment.centerLeft,
              child: SmallText(
                text: "Local Notifications",
                color: Colors.black,
                size: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: Dimensions.height10),
            Wrap(
              spacing: 10,
              children: [
                ActionChip(
                  backgroundColor: Colors.black,
                  onPressed: () => LocalNotifications.basicNotification(),
                  label: SmallText(
                    text: "Test Local Notification",
                    color: Colors.white,
                  ),
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  onPressed: () => LocalNotifications.basicNotification(),
                  label: SmallText(
                    text: "Firebase Notification",
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            SizedBox(height: Dimensions.height10),
            Align(
              alignment: Alignment.centerLeft,
              child: SmallText(
                text: "Bottom Sheets",
                color: Colors.black,
                size: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: Dimensions.height10),
            //? to remove later

            Wrap(
              direction: Axis.horizontal,
              children: [
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Add Mileage",
                    color: Colors.white,
                  ),
                  onPressed: () => AddMileage.addMileageBottomSheet(context),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Mark as Complete",
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      MarkComplete.markCompleteBottomSheet(context),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Expense Sheet",
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      AddExpenseSheet.addExpenseBottomSheet(context),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Notorial Acts",
                    color: Colors.white,
                  ),
                  onPressed: () => NotorialAct.openNotorialBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "No Show UI",
                    color: Colors.white,
                  ),
                  onPressed: () => NoShow.openNoShowBottomSheet(context),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Cancel Invoice",
                    color: Colors.white,
                  ),
                  onPressed: () => CancelInvoice.openCancelInvoiceBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Change Service",
                    color: Colors.white,
                  ),
                  onPressed: () => CostService.openCostServiceBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Update Payment",
                    color: Colors.white,
                  ),
                  onPressed: () => UpdatePayment.openUpdatePaymentBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Bill from",
                    color: Colors.white,
                  ),
                  onPressed: () => BillFrom.openBillFromBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Bill to",
                    color: Colors.white,
                  ),
                  onPressed: () => BillTo.openBillToBottomSheet(),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Mileage Tracker",
                    color: Colors.white,
                  ),
                  onPressed: () => Get.to(() => const MileageTracker()),
                ),
                SizedBox(
                  width: Dimensions.width10,
                ),
                ActionChip(
                  backgroundColor: Colors.black,
                  label: SmallText(
                    text: "Activity Log",
                    color: Colors.white,
                  ),
                  onPressed: () => Get.to(() => const ActivityLog()),
                ),
              ],
            ),
            SizedBox(
              height: Dimensions.height20,
            ),
            //? to remove later
          ],
        ),
      ),
    );
  }
}
