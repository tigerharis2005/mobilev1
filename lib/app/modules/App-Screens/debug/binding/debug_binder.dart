import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/debug/controller/debug_controller.dart';

class DebugBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DebugController());
  }
}
