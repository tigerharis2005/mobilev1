import 'package:get/get.dart';

import '../controller/accountpagecontroller.dart';

class AccountPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AccountPageController());
  }
}
