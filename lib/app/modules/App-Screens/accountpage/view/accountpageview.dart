import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/big_text.dart';

import '../../../../../utils/dimensions.dart';
import '../controller/accountpagecontroller.dart';

class AccountPage extends GetView<AccountPageController> {
  static const route = '/account-page';
  static launch() => Get.toNamed(route);
  const AccountPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.symmetric(horizontal: Dimensions.width30),
      child: Column(
        children: [
          SizedBox(
            height: Dimensions.height30 * 1.9,
          ),
          Row(
            children: [
              InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Icon(Icons.arrow_back_ios,
                      size: Dimensions.iconSize24 * 1.2,
                      color: const Color(0xFF0D47A1))),
              const Spacer(),
            ],
          ),
          SizedBox(
            height: Dimensions.height40 * 2,
          ),
          Center(child: BigText(text: 'AccountPage')),
        ],
      ),
    ));
  }
}
