import 'package:get/get.dart';

class ActivityLogController extends GetxController {
  //* current step
  RxInt currentStep = 0.obs;

  //* next step
  void continueStep() {
    if (currentStep.value < 3) {
      currentStep.value++;
    }
  }

  //* previous step
  void cancelStep() {
    if (currentStep.value > 0) {
      currentStep.value--;
    }
  }
}
