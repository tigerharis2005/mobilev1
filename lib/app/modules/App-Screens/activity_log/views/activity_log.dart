import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../widgets/button.dart';
import '../controller/activity_log_controller.dart';

class ActivityLog extends StatelessWidget {
  const ActivityLog({super.key});

  Widget stepBuilder(context, details) {
    return Row(
      children: [
        Button(
          width: Get.width * 0.3,
          height: Get.height * 0.05,
          on_pressed: details.onStepContinue,
          text: ("Next"),
        ),
        const SizedBox(width: 10),
        Button(
          width: Get.width * 0.3,
          height: Get.height * 0.05,
          on_pressed: details.onStepCancel,
          text: ("Back"),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final ActivityLogController activityLogController =
        Get.put(ActivityLogController());
    return Scaffold(
      body: Center(
          child: Obx(
        () => Theme(
          data: ThemeData(
            colorScheme: Theme.of(context).colorScheme.copyWith(
                  onSurface: Colors.black,
                ),
          ),
          child: Stepper(
            elevation: 1,
            controlsBuilder: stepBuilder,
            currentStep: activityLogController.currentStep.value,
            onStepContinue: activityLogController.continueStep,
            onStepCancel: activityLogController.cancelStep,
            steps: [
              Step(
                label: const CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Icon(Icons.person, color: Colors.black),
                ),
                title: SmallText(text: 'Step 1 Message', size: 18),
                content: const Text(''),
                isActive: false,
                subtitle: const Text("2 Days ago"),
              ),
              Step(
                title: SmallText(text: 'Step 2 Message', size: 18),
                content: const Text(''),
                isActive: false,
                subtitle: const Text("2 Days ago"),
              ),
              Step(
                title: SmallText(text: 'Step 3 Message', size: 18),
                content: const Text(''),
                isActive: false,
                subtitle: const Text("2 Days ago"),
              ),
              Step(
                title: SmallText(text: 'Step 4 Message', size: 18),
                content: const Text(''),
                isActive: false,
                subtitle: const Text("2 Days ago"),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
