import 'package:get/get.dart';

import '../controller/servicespagecontroller.dart';


class ServicesPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ServicesPageController());
  }
}
