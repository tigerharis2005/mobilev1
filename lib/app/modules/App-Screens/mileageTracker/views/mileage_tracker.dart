import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/utils/dimensions.dart';
import 'package:mobilev1/widgets/big_text.dart';
import 'package:mobilev1/widgets/buttons.dart';
import 'package:mobilev1/widgets/card.dart';
import 'package:mobilev1/widgets/small_text.dart';

import '../../../../../widgets/button.dart';
import '../controller/mileage_tracker_controller.dart';

class MileageTracker extends StatelessWidget {
  const MileageTracker({super.key});

  @override
  Widget build(BuildContext context) {
    final MileageTrackerController mileageTrackerController =
        Get.put(MileageTrackerController());
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mileage Tracker"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CardView(
                  margin: 2,
                  cardColor: Colors.red.shade50,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Text(
                            "You have not entered any mileage for this order",
                            style: Theme.of(context)
                                .textTheme
                                .displaySmall!
                                .copyWith(
                                  fontSize: Dimensions.font18,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        ),
                        //SizedBox(height: Dimensions.height10),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: Dimensions.height10),
                Center(
                    child: SmallText(text: "Either above card or below card")),
                SizedBox(height: Dimensions.height10),
                //* 1st row row
                CardView(
                  margin: 2,
                  cardColor: Colors.green.shade50,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              const TextSpan(
                                text: "You have entered ",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                              TextSpan(
                                text: mileageTrackerController.mileage.value
                                    .toString(),
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ),
                              const TextSpan(
                                text: " Miles for this order saving \$",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                              TextSpan(
                                text: mileageTrackerController.mileageCost.value
                                    .toString(),
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ),
                              const TextSpan(
                                text: " of Tax",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: Dimensions.height10),
                        SmallText(
                            size: 14,
                            text:
                                "You can change by computing the mileage again"),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: Dimensions.height20),

                //* Title
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    "Mileage Tracker",
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                ),

                SizedBox(height: Dimensions.height10),

                //* subtitle
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: SmallText(
                      size: 16,
                      text:
                          "Enter the start destination and\nend destination to autocomplete."),
                ),

                SizedBox(height: Dimensions.height30),

                //* start
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //* icon
                    const FaIcon(FontAwesomeIcons.locationDot,
                        color: Colors.red),
                    SizedBox(width: Dimensions.width10),
                    //* text
                    Text(
                      "Start Destination",
                      style: Theme.of(context).textTheme.displaySmall,
                    ),
                  ],
                ),

                SizedBox(height: Dimensions.height10),

                Center(
                  child: MyButton(
                    onPressed: () {},
                    text: "Your Current Location",
                  ),
                ),

                SizedBox(height: Dimensions.height20),

                //* end destination
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //* icon
                    const FaIcon(FontAwesomeIcons.locationPinLock,
                        color: Colors.red),
                    SizedBox(width: Dimensions.width10),
                    //* text
                    Text(
                      "End Destination",
                      style: Theme.of(context).textTheme.displaySmall,
                    ),
                  ],
                ),
                SizedBox(height: Dimensions.height10),

                Center(
                  child: MyButton(
                    onPressed: () {},
                    text: "End Destination",
                  ),
                ),

                SizedBox(height: Dimensions.height20),

                //* round trip check box
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Checkbox(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          checkColor: Colors.white,
                          activeColor: Colors.black,
                          value: mileageTrackerController.roundTripBool.value,
                          onChanged: (value) => mileageTrackerController
                              .updateRoundTripBool(value!)),
                    ),
                    SmallText(
                      text: "Make this a round trip",
                      size: 16,
                    ),
                  ],
                ),

                //* calculate button
                Center(
                  child: MyButton(
                    text: "Calculate",
                    onPressed: () {},
                  ),
                ),

                SizedBox(height: Dimensions.height10),

                //* note
                CardView(
                  margin: 2,
                  cardColor: Colors.deepPurple.shade50,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FaIcon(
                          Icons.lightbulb,
                          size: Dimensions.iconSize24,
                          color: Colors.amber,
                        ),
                        SizedBox(width: Dimensions.width10),
                        Flexible(
                          child: Text(
                            'Advanced Real Time Mileage Tracking coming soon!',
                            style: Theme.of(context)
                                .textTheme
                                .displaySmall!
                                .copyWith(
                                  fontSize: Dimensions.font18,
                                ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )),
        ),
      ),
    );
  }
}
