import 'package:get/get.dart';

class MileageTrackerController extends GetxController {
  //* fetch distance
  RxDouble mileage = 7.2.obs;

  //* fetch distance cost
  RxInt mileageCost = 14.obs;

  //* round trip check bool
  RxBool roundTripBool = false.obs;

  //* update round trip bool
  void updateRoundTripBool(bool value) {
    roundTripBool.value = value;
    update();
  }
}
