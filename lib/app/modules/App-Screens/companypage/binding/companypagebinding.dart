import 'package:get/get.dart';

import '../controller/companypagecontroller.dart';

class CompanyPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CompanyPageController());
  }
}
