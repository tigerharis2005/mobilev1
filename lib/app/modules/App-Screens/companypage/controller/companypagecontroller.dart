import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/company_model.dart';
import 'package:mobilev1/app/data/models/api_data_model/invoices_model.dart';
import 'package:mobilev1/app/data/repo/company_repo.dart';

import '../../../../../helper/loading.dart';
import '../../../../data/models/repo_response/repo_response.dart';
import '../../../../data/repo/invoices_repo.dart';

class CompanyPageController extends GetxController
with GetTickerProviderStateMixin{
  var getCompaniesdata={};

  final List<String> items = ['Item 1', 'Item 2', 'Item 3'];
  late TabController tabController;

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(length: 4, vsync: this);
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }

  
  getContacts()async{
    LoadingUtils.showLoader();
    GetCompanies data=GetCompanies(notaryId: GetStorage().read('userId'));
    CompanyRepository repo =CompanyRepository();
    RepoResponse response=await repo.getCompanies(data);
    if(response.data!=null){
      getCompaniesdata=response.data;
    }
    LoadingUtils.hideLoader();
  }

  var getInvoicesdataOfCompany={};


  getInvoicesofCompany(String companyId)async{
    LoadingUtils.showLoader();
    InvoicesForCompany data=InvoicesForCompany(companyId:companyId);
    InvoicesRepository repo =InvoicesRepository();
    RepoResponse response=await repo.invoicesForCompany(data);
    if(response.data!=null){
      getInvoicesdataOfCompany=response.data;
    }
    LoadingUtils.hideLoader();
  }
  editCompany(String companyId)async{
    LoadingUtils.showLoader();
    Map<String, dynamic> data={ 
    "CompanyId": "asdfasdf",
    "notaryId": "asdfasdf",
    "editObj":{
      	"name": "asdf",
				
				"phoneNumber": 9986758340 ,
				"fax": 5,
                "bio": "",
				"email":'everest@gmail.com',
				"orderEmailAddress": ['everest@gmail.com'],
				"taxNumber": "SOME Tax Number",
				"city": "BAngalore",
				"state": "state",
				"zipcode": 123123,
		
    }
    
    	
};
    CompanyRepository repo =CompanyRepository();
    RepoResponse response=await repo.editACompany(data);
    // if(response.data!=null){
    //   getInvoicesdataOfCompany=response.data;
    // }
    LoadingUtils.hideLoader();
  }
  addCompany(String? name,int? phoneNumber,int? fax,String? bio,String? email,String? taxNumber,String? city,String? state,int? zipcode)async{
    LoadingUtils.showLoader();
    AddACompany data=AddACompany(name: name,phoneNumber: phoneNumber,fax: fax,bio: bio,email: email,taxNumber: taxNumber,city: city,state: state,zipcode: zipcode,userId: GetStorage().read('userId'));
    CompanyRepository repo =CompanyRepository();
    RepoResponse response=await repo.addACompany(data);
    // if(response.data!=null){
    //   getInvoicesdataOfCompany=response.data;
    // }
    LoadingUtils.hideLoader();
  }


  
  

}