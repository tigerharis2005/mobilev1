// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobilev1/app/modules/App-Screens/companypage/view/firstCompanyTabCard.dart';
import 'package:mobilev1/app/modules/App-Screens/companypage/view/secondCompanyTabCard.dart';
import 'package:mobilev1/app/modules/App-Screens/companypage/view/thirdCompanyTabCard.dart';
import 'package:mobilev1/widgets/big_text.dart';

import '../../../../../utils/dimensions.dart';
import '../controller/companypagecontroller.dart';

class CompanyPage extends GetView<CompanyPageController> {
  static const route = '/company-page';
  static launch() => Get.toNamed(route);
  const CompanyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.symmetric(horizontal: Dimensions.width30),
      child: Column(
        children: [
          SizedBox(
            height: Dimensions.height30 * 2,
          ),
          Row(
            children: [
              InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Icon(Icons.arrow_back_ios,
                      size: Dimensions.iconSize24 * 1.2,
                      color: const Color(0xFF0D47A1))),
              SizedBox(
                width: Dimensions.width15,
              ),
              BigText(
                text: 'Company List',
                size: Dimensions.font26,
                fontWeight: FontWeight.w500,
              ),
              const Spacer(),
            ],
          ),
          SizedBox(
            height: Dimensions.height20,
          ),
          TabBar(
            unselectedLabelColor: Colors.black,
            labelColor: const Color(0xFF0D47A1),
            controller: controller.tabController,
            indicatorColor: const Color(0xFF0D47A1),
            labelPadding: null,
            labelStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: Dimensions.font15 * 1.1,
                fontWeight: FontWeight.w600),
            unselectedLabelStyle: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(
                    fontSize: Dimensions.font15 * 1.1,
                    fontWeight: FontWeight.w600),
            indicatorSize: TabBarIndicatorSize.tab,
            tabs: const [
              Tab(child: Text('1st Tab')),
              Tab(child: Text('2nd Tab')),
              Tab(child: Text('3rd Tab')),
              Tab(child: Text('4rth Tab')),
            ],
          ),
          Expanded(
            child: TabBarView(
              controller: controller.tabController,
              children: [
                ListView.separated(
                    padding: const EdgeInsets.all(16),
                    itemCount: controller.items.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                    itemBuilder: (BuildContext context, int index) {
                      return FirstCompanyTabCard();
                    }),
                ListView.separated(
                    padding: const EdgeInsets.all(16),
                    itemCount: controller.items.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                    itemBuilder: (BuildContext context, int index) {
                      return SecondCompanyTabCard();
                    }),
                ListView.separated(
                    padding: const EdgeInsets.all(16),
                    itemCount: controller.items.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                          height: Dimensions.height10,
                        ),
                    itemBuilder: (BuildContext context, int index) {
                      return ThirdCompanyTabCard();
                    }),
                Center(child: BigText(text: "4rth Tab without List View"))
              ],
            ),
          )
        ],
      ),
    ));
  }
}
