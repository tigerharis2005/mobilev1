import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:mobilev1/widgets/buttons.dart';
import 'package:mobilev1/widgets/my_text_field.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../widgets/big_text.dart';
import '../../../../../widgets/button.dart';
import '../../../../../widgets/text_field.dart';
import '../controller/loginpagecontroller.dart';

class LoginPage extends GetView<LoginPageController> {
  static const route = '/loginpage';
  static launch() => Get.toNamed(route);

  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    // Get.put(LoginPageController());
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(Dimensions.height30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'Login',
                style: Theme.of(context).textTheme.displayLarge!.copyWith(
                      fontSize: 28,
                    ),
              ),
              SizedBox(
                height: Dimensions.height10 * 7,
              ),
              Text(
                'Email',
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 18,
                    ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),

              MyTextField(
                hintText: "Email",
                prefixIcon: FaIcon(
                  Icons.mail,
                  color: Colors.grey.shade700,
                  size: 22,
                ),
                width: Dimensions.screenWidth * 0.9,
                inputFormatters: const [],
                controller: controller.emailController,
                keyboardType: TextInputType.emailAddress,
              ),

              // Text_Field(
              //   radius: Dimensions.radius20,
              //   text_field_width: double.maxFinite,
              //   text_field_height: Dimensions.height20 * 3,
              //   text_field: TextField(
              //     autofocus: true,
              //     onChanged: (value) {
              //       controller.email = value;
              //     },
              //     decoration: InputDecoration(
              //         prefixIcon: Icon(
              //           Icons.mail,
              //           color: Colors.grey.shade300,
              //         ),
              //         border: InputBorder.none,
              //         hintText: 'Uname@mail.com'),
              //   ),
              // ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Text(
                'Password',
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: 18,
                    ),
              ),

              //??

              SizedBox(
                height: Dimensions.height10,
              ),
              MyTextField(
                obscureText: true,
                hintText: "Password",
                prefixIcon: FaIcon(
                  FontAwesomeIcons.lock,
                  color: Colors.grey.shade700,
                  size: 22,
                ),
                width: Dimensions.screenWidth * 0.9,
                inputFormatters: const [],
                controller: controller.passwordController,
                keyboardType: TextInputType.visiblePassword,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),

              //??
              // Text_Field(
              //   radius: Dimensions.radius20,
              //   text_field_width: double.maxFinite,
              //   text_field_height: Dimensions.height20 * 3,
              //   text_field: TextField(
              //     autofocus: true,
              //     obscureText: true,
              //     onChanged: (value) {
              //       controller.password = value;
              //     },
              //     decoration: InputDecoration(
              //         prefixIcon: FaIcon(
              //           FontAwesomeIcons.lock,
              //           color: Colors.grey.shade600,
              //         ),
              //         border: InputBorder.none,
              //         hintText: 'Password'),
              //   ),
              // ),
              SizedBox(
                height: Dimensions.height40,
              ),
              Column(
                children: [
                  MyButton(
                    text: "LOGIN",
                    textStyle:
                        Theme.of(context).textTheme.displayMedium!.copyWith(
                              color: Colors.white,
                            ),
                    onPressed: () async {
                      controller.onLogin();
                    },
                  ),
                  SizedBox(
                    height: Dimensions.height20,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
