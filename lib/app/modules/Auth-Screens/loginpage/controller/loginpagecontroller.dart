import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/repo/login_repo.dart';
import 'package:mobilev1/helper/loading.dart';
import '../../../App-Screens/homepage/view/homepageview.dart';

class LoginPageController extends GetxController {
  LoginRepository loginRepo = LoginRepository();
  final auth = FirebaseAuth.instance;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  String verification = '';
  bool loading = false;

  onLogin() async {
    LoadingUtils.showLoader();
    try {
      await auth
          .signInWithEmailAndPassword(
              email: emailController.text, password: passwordController.text)
          .then((value) async {
        var uid = value.user!.uid;
        // String? token = await FirebaseMessaging.instance.getToken();
        LoadingUtils.hideLoader();
        HomePage.launch();

        GetStorage().write('uid', uid);
        GetStorage().write('email', emailController.text);
      });
      LoadingUtils.hideLoader();
      // SPController().setIsLoggedin(true);
    } on FirebaseAuthException catch (err) {
      LoadingUtils.hideLoader();
      if (err.code == 'user-not-found') {
        debugPrint('No user found for that email.');
      } else if (err.code == 'wrong-password') {
        debugPrint('Wrong password provided for that user.');
      }
    } catch (err) {
      LoadingUtils.hideLoader();
      debugPrint(err.toString());
    }
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
