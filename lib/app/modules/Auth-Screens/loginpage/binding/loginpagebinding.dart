import 'package:get/get.dart';

import '../controller/loginpagecontroller.dart';

class LoginPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginPageController());
  }
}
