import 'dart:io';
import 'package:dio/dio.dart';
import '../models/repo_response/error_response.dart';
import 'error_text.dart';

class APIException implements Exception{
  final String message;

  APIException({required this.message}); 
}

class ExceptionHandler {
  ExceptionHandler._privateConstructor();

  static APIException handleError(Exception error) {
    if (error is DioError) {
      switch (error.type) {
        case DioErrorType.sendTimeout:
          return APIException(message: ErrorMessages.noInternet);
        case DioErrorType.connectTimeout:
          return APIException(message: ErrorMessages.connectionTimeout);
        case DioErrorType.response:
          try {
            final errorResponse = ErrorResponse.fromJson(error.response!.data);
            return APIException(message: errorResponse.message);
          } catch (e) {
            return APIException(message: ErrorMessages.networkGeneral);
          }
        default:
          if (error.error is SocketException) {
            return APIException(
              message:
             // error.error.osError?.message ?? 
              ErrorMessages.networkGeneral,
            );
          }
          return APIException(message: ErrorMessages.networkGeneral);
      }
    } else {
      return APIException(message: ErrorMessages.networkGeneral);
    }
  }
}


