import '../../error_handler/error_text.dart';

class ErrorResponse {
  late final String message;
  late final int code;

  ErrorResponse.fromJson(Map<String, dynamic>? json) {
    message =
        json?['message'] ?? json?['error'] ?? ErrorMessages.networkGeneral;
    code = json?['statusCode'] ?? 0;
  }
}