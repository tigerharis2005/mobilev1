// ignore_for_file: unnecessary_getters_setters

class GetAppointmentsByNotaryId {
  String? notaryId;
  int? status;

  GetAppointmentsByNotaryId({this.notaryId, this.status});

  GetAppointmentsByNotaryId.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['status'] = status;
    return data;
  }
}

class CancelAnAppointment {
  String? _aptId;
  String? _notaryId;
  bool? _doesNeedCancellationInvoice;
  int? _amount;

  CancelAnAppointment(
      {String? aptId,
      String? notaryId,
      bool? doesNeedCancellationInvoice,
      int? amount}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
    if (doesNeedCancellationInvoice != null) {
      _doesNeedCancellationInvoice = doesNeedCancellationInvoice;
    }
    if (amount != null) {
      _amount = amount;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;
  bool? get doesNeedCancellationInvoice => _doesNeedCancellationInvoice;
  set doesNeedCancellationInvoice(bool? doesNeedCancellationInvoice) =>
      _doesNeedCancellationInvoice = doesNeedCancellationInvoice;
  int? get amount => _amount;
  set amount(int? amount) => _amount = amount;

  CancelAnAppointment.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _notaryId = json['notaryId'];
    _doesNeedCancellationInvoice = json['doesNeedCancellationInvoice'];
    _amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['notaryId'] = _notaryId;
    data['doesNeedCancellationInvoice'] = _doesNeedCancellationInvoice;
    data['amount'] = _amount;
    return data;
  }
}

class Reschedule {
  String? _notaryId;
  String? _aptId;
  int? _aptTimeStamp;
  String? _updateSource;
  String? _browserTimeZone;

  Reschedule(
      {String? notaryId,
      String? aptId,
      int? aptTimeStamp,
      String? updateSource,
      String? browserTimeZone}) {
    if (notaryId != null) {
      _notaryId = notaryId;
    }
    if (aptId != null) {
      _aptId = aptId;
    }
    if (aptTimeStamp != null) {
      _aptTimeStamp = aptTimeStamp;
    }
    if (updateSource != null) {
      _updateSource = updateSource;
    }
    if (browserTimeZone != null) {
      _browserTimeZone = browserTimeZone;
    }
  }

  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;
  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  int? get aptTimeStamp => _aptTimeStamp;
  set aptTimeStamp(int? aptTimeStamp) => _aptTimeStamp = aptTimeStamp;
  String? get updateSource => _updateSource;
  set updateSource(String? updateSource) => _updateSource = updateSource;
  String? get browserTimeZone => _browserTimeZone;
  set browserTimeZone(String? browserTimeZone) =>
      _browserTimeZone = browserTimeZone;

  Reschedule.fromJson(Map<String, dynamic> json) {
    _notaryId = json['notaryId'];
    _aptId = json['aptId'];
    _aptTimeStamp = json['aptTimeStamp'];
    _updateSource = json['updateSource'];
    _browserTimeZone = json['browserTimeZone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = _notaryId;
    data['aptId'] = _aptId;
    data['aptTimeStamp'] = _aptTimeStamp;
    data['updateSource'] = _updateSource;
    data['browserTimeZone'] = _browserTimeZone;
    return data;
  }
}

class ChangeMeetingLink {
  String? _aptId;
  String? _notaryId;
  String? _onlineMeetingLink;
  String? _updateSource;
  bool? _sendMailToSigners;

  ChangeMeetingLink(
      {String? aptId,
      String? notaryId,
      String? onlineMeetingLink,
      String? updateSource,
      bool? sendMailToSigners}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
    if (onlineMeetingLink != null) {
      _onlineMeetingLink = onlineMeetingLink;
    }
    if (updateSource != null) {
      _updateSource = updateSource;
    }
    if (sendMailToSigners != null) {
      _sendMailToSigners = sendMailToSigners;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;
  String? get onlineMeetingLink => _onlineMeetingLink;
  set onlineMeetingLink(String? onlineMeetingLink) =>
      _onlineMeetingLink = onlineMeetingLink;
  String? get updateSource => _updateSource;
  set updateSource(String? updateSource) => _updateSource = updateSource;
  bool? get sendMailToSigners => _sendMailToSigners;
  set sendMailToSigners(bool? sendMailToSigners) =>
      _sendMailToSigners = sendMailToSigners;

  ChangeMeetingLink.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _notaryId = json['notaryId'];
    _onlineMeetingLink = json['onlineMeetingLink'];
    _updateSource = json['updateSource'];
    _sendMailToSigners = json['sendMailToSigners'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['notaryId'] = _notaryId;
    data['onlineMeetingLink'] = _onlineMeetingLink;
    data['updateSource'] = _updateSource;
    data['sendMailToSigners'] = _sendMailToSigners;
    return data;
  }
}


class ChangeServiceOfApt {
  String? _aptId;
  String? _notaryId;
  Service? _service;

  ChangeServiceOfApt({String? aptId, String? notaryId, Service? service}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
    if (service != null) {
      _service = service;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;
  Service? get service => _service;
  set service(Service? service) => _service = service;

  ChangeServiceOfApt.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _notaryId = json['notaryId'];
    _service =
        json['service'] != null ? Service.fromJson(json['service']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['notaryId'] = _notaryId;
    if (_service != null) {
      data['service'] = _service!.toJson();
    }
    return data;
  }
}

class Service {
  String? _sId;
  String? _serviceName;
  String? _description;
  String? _cost;
  String? _serviceId;

  Service(
      {String? sId,
      String? serviceName,
      String? description,
      String? cost,
      String? serviceId}) {
    if (sId != null) {
      _sId = sId;
    }
    if (serviceName != null) {
      _serviceName = serviceName;
    }
    if (description != null) {
      _description = description;
    }
    if (cost != null) {
      _cost = cost;
    }
    if (serviceId != null) {
      _serviceId = serviceId;
    }
  }

  String? get sId => _sId;
  set sId(String? sId) => _sId = sId;
  String? get serviceName => _serviceName;
  set serviceName(String? serviceName) => _serviceName = serviceName;
  String? get description => _description;
  set description(String? description) => _description = description;
  String? get cost => _cost;
  set cost(String? cost) => _cost = cost;
  String? get serviceId => _serviceId;
  set serviceId(String? serviceId) => _serviceId = serviceId;

  Service.fromJson(Map<String, dynamic> json) {
    _sId = json['_id'];
    _serviceName = json['serviceName'];
    _description = json['description'];
    _cost = json['cost'];
    _serviceId = json['serviceId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = _sId;
    data['serviceName'] = _serviceName;
    data['description'] = _description;
    data['cost'] = _cost;
    data['serviceId'] = _serviceId;
    return data;
  }
}
class ChangeServiceCost {
  String? _aptId;
  String? _notaryId;
  String? _serviceIdDB;
  String? _newCost;
  bool? _isDefaultForThisService;

  ChangeServiceCost(
      {String? aptId,
      String? notaryId,
      String? serviceIdDB,
      String? newCost,
      bool? isDefaultForThisService}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
    if (serviceIdDB != null) {
      _serviceIdDB = serviceIdDB;
    }
    if (newCost != null) {
      _newCost = newCost;
    }
    if (isDefaultForThisService != null) {
      _isDefaultForThisService = isDefaultForThisService;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;
  String? get serviceIdDB => _serviceIdDB;
  set serviceIdDB(String? serviceIdDB) => _serviceIdDB = serviceIdDB;
  String? get newCost => _newCost;
  set newCost(String? newCost) => _newCost = newCost;
  bool? get isDefaultForThisService => _isDefaultForThisService;
  set isDefaultForThisService(bool? isDefaultForThisService) =>
      _isDefaultForThisService = isDefaultForThisService;

  ChangeServiceCost.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _notaryId = json['notaryId'];
    _serviceIdDB = json['serviceId_DB'];
    _newCost = json['newCost'];
    _isDefaultForThisService = json['isDefaultForThisService'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['notaryId'] = _notaryId;
    data['serviceId_DB'] = _serviceIdDB;
    data['newCost'] = _newCost;
    data['isDefaultForThisService'] = _isDefaultForThisService;
    return data;
  }
}

class AddSigner {
  String? _aptId;
  String? _firstName;
  String? _lastName;
  int? _phoneNumber;
  String? _emailAddress;
  String? _notaryId;

  AddSigner(
      {String? aptId,
      String? firstName,
      String? lastName,
      int? phoneNumber,
      String? emailAddress,
      String? notaryId}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (firstName != null) {
      _firstName = firstName;
    }
    if (lastName != null) {
      _lastName = lastName;
    }
    if (phoneNumber != null) {
      _phoneNumber = phoneNumber;
    }
    if (emailAddress != null) {
      _emailAddress = emailAddress;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get firstName => _firstName;
  set firstName(String? firstName) => _firstName = firstName;
  String? get lastName => _lastName;
  set lastName(String? lastName) => _lastName = lastName;
  int? get phoneNumber => _phoneNumber;
  set phoneNumber(int? phoneNumber) => _phoneNumber = phoneNumber;
  String? get emailAddress => _emailAddress;
  set emailAddress(String? emailAddress) => _emailAddress = emailAddress;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;

  AddSigner.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _firstName = json['firstName'];
    _lastName = json['lastName'];
    _phoneNumber = json['phoneNumber'];
    _emailAddress = json['emailAddress'];
    _notaryId = json['notaryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['firstName'] = _firstName;
    data['lastName'] = _lastName;
    data['phoneNumber'] = _phoneNumber;
    data['emailAddress'] = _emailAddress;
    data['notaryId'] = _notaryId;
    return data;
  }
}


class RemoveSigner {
  String? _aptId;
  String? _leadId;
  String? _notaryId;

  RemoveSigner({String? aptId, String? leadId, String? notaryId}) {
    if (aptId != null) {
      _aptId = aptId;
    }
    if (leadId != null) {
      _leadId = leadId;
    }
    if (notaryId != null) {
      _notaryId = notaryId;
    }
  }

  String? get aptId => _aptId;
  set aptId(String? aptId) => _aptId = aptId;
  String? get leadId => _leadId;
  set leadId(String? leadId) => _leadId = leadId;
  String? get notaryId => _notaryId;
  set notaryId(String? notaryId) => _notaryId = notaryId;

  RemoveSigner.fromJson(Map<String, dynamic> json) {
    _aptId = json['aptId'];
    _leadId = json['leadId'];
    _notaryId = json['notaryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = _aptId;
    data['leadId'] = _leadId;
    data['notaryId'] = _notaryId;
    return data;
  }
}

