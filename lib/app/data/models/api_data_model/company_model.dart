class GetCompanies {
  String? notaryId;

  GetCompanies({this.notaryId});

  GetCompanies.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    return data;
  }
}


class AddACompany {
  String? name;
  int? phoneNumber;
  int? fax;
  String? bio;
  String? email;
  String? taxNumber;
  String? city;
  String? state;
  int? zipcode;
  String? userId;

  AddACompany(
      {this.name,
      this.phoneNumber,
      this.fax,
      this.bio,
      this.email,
      this.taxNumber,
      this.city,
      this.state,
      this.zipcode,
      this.userId});

  AddACompany.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phoneNumber = json['phoneNumber'];
    fax = json['fax'];
    bio = json['bio'];
    email = json['email'];
    taxNumber = json['taxNumber'];
    city = json['city'];
    state = json['state'];
    zipcode = json['zipcode'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['fax'] = fax;
    data['bio'] = bio;
    data['email'] = email;
    data['taxNumber'] = taxNumber;
    data['city'] = city;
    data['state'] = state;
    data['zipcode'] = zipcode;
    data['userId'] = userId;
    return data;
  }
}
