class UpdateMileage {
  String? notaryId;
  String? aptId;
  int? milesDriven;
  int? perMileCost;
  String? dateAdded;
  String? mileageEntrySource;

  UpdateMileage(
      {this.notaryId,
      this.aptId,
      this.milesDriven,
      this.perMileCost,
      this.dateAdded,
      this.mileageEntrySource});

  UpdateMileage.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    aptId = json['aptId'];
    milesDriven = json['milesDriven'];
    perMileCost = json['perMileCost'];
    dateAdded = json['dateAdded'];
    mileageEntrySource = json['mileageEntrySource'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['aptId'] = aptId;
    data['milesDriven'] = milesDriven;
    data['perMileCost'] = perMileCost;
    data['dateAdded'] = dateAdded;
    data['mileageEntrySource'] = mileageEntrySource;
    return data;
  }
}


class MarkMileageNotRequired {
  String? aptId;
  String? notaryId;
  String? mileageEntrySource;
  bool? isMileageEntryRequired;

  MarkMileageNotRequired(
      {this.aptId,
      this.notaryId,
      this.mileageEntrySource,
      this.isMileageEntryRequired});

  MarkMileageNotRequired.fromJson(Map<String, dynamic> json) {
    aptId = json['aptId'];
    notaryId = json['notaryId'];
    mileageEntrySource = json['mileageEntrySource'];
    isMileageEntryRequired = json['isMileageEntryRequired'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = aptId;
    data['notaryId'] = notaryId;
    data['mileageEntrySource'] = mileageEntrySource;
    data['isMileageEntryRequired'] = isMileageEntryRequired;
    return data;
  }
}
