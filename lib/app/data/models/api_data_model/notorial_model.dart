class MarkActsNotRequired {
  String? aptId;
  String? notaryId;
  String? updateSource;
  bool? isNotarialActsNotRequired;

  MarkActsNotRequired(
      {this.aptId,
      this.notaryId,
      this.updateSource,
      this.isNotarialActsNotRequired});

  MarkActsNotRequired.fromJson(Map<String, dynamic> json) {
    aptId = json['aptId'];
    notaryId = json['notaryId'];
    updateSource = json['updateSource'];
    isNotarialActsNotRequired = json['isNotarialActsNotRequired'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = aptId;
    data['notaryId'] = notaryId;
    data['updateSource'] = updateSource;
    data['isNotarialActsNotRequired'] = isNotarialActsNotRequired;
    return data;
  }
}
