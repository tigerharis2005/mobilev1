// ignore_for_file: void_checks

class GetInvoicesForNotary {
  String? notaryId;
  int? status;

  GetInvoicesForNotary({this.notaryId, this.status});

  GetInvoicesForNotary.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['status'] = status;
    return data;
  }
}
class CancelInvoiceFlag {
  String? notaryId;
  String? invoiceId;
  String? aptId;
  bool? cancelAppointmentFlag;

  CancelInvoiceFlag(
      {this.notaryId, this.invoiceId, this.aptId, this.cancelAppointmentFlag});

  CancelInvoiceFlag.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    invoiceId = json['invoiceId'];
    aptId = json['aptId'];
    cancelAppointmentFlag = json['cancelAppointmentFlag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['invoiceId'] = invoiceId;
    data['aptId'] = aptId;
    data['cancelAppointmentFlag'] = cancelAppointmentFlag;
    return data;
  }
}


class MarkAsUncollectable {
  String? notaryId;
  String? invoiceId;
  String? aptId;

  MarkAsUncollectable({this.notaryId, this.invoiceId, this.aptId});

  MarkAsUncollectable.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    invoiceId = json['invoiceId'];
    aptId = json['aptId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['invoiceId'] = invoiceId;
    data['aptId'] = aptId;
    return data;
  }
}

class InvoicesForContact {
  String? leadId;
  int? status;

  InvoicesForContact({this.leadId, this.status});

  InvoicesForContact.fromJson(Map<String, dynamic> json) {
    leadId = json['leadId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['leadId'] = leadId;
    data['status'] = status;
    return data;
  }
}

class InvoicesForCompany {
  String? companyId;
  int? status;

  InvoicesForCompany({this.companyId, this.status});

  InvoicesForCompany.fromJson(Map<String, dynamic> json) {
    companyId = json['companyId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['companyId'] = companyId;
    data['status'] = status;
    return data;
  }
}


class SearchInvoice {
  String? notaryId;
  String? searchTerm;
  String? page;
  String? value;

  SearchInvoice({this.notaryId, this.searchTerm, this.page, this.value});

  SearchInvoice.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    searchTerm = json['searchTerm'];
    page = json['page'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['searchTerm'] = searchTerm;
    data['page'] = page;
    data['value'] = value;
    return data;
  }
}


class CreateInvoice {
  String? aptId;
  String? invoiceNo;
  String? notaryId;
  String? issueDate;
  String? dueDate;
  List<void>? receipientEmails;
  String? leadId;
  String? serviceId;
  String? companyId;
  int? totalCost;
  String? loanNumber;
  String? invoiceNotes;
  List<LineItems>? lineItems;
  String? updateSource;

  CreateInvoice(
      {this.aptId,
      this.invoiceNo,
      this.notaryId,
      this.issueDate,
      this.dueDate,
      this.receipientEmails,
      this.leadId,
      this.serviceId,
      this.companyId,
      this.totalCost,
      this.loanNumber,
      this.invoiceNotes,
      this.lineItems,
      this.updateSource});

  CreateInvoice.fromJson(Map<String, dynamic> json) {
    aptId = json['aptId'];
    invoiceNo = json['invoiceNo'];
    notaryId = json['notaryId'];
    issueDate = json['issueDate'];
    dueDate = json['dueDate'];
    if (json['receipientEmails'] != null) {
      receipientEmails = <Null>[];
      json['receipientEmails'].forEach((v) {
        receipientEmails!.add(v);
      });
    }
    leadId = json['leadId'];
    serviceId = json['serviceId'];
    companyId = json['companyId'];
    totalCost = json['totalCost'];
    loanNumber = json['loanNumber'];
    invoiceNotes = json['invoiceNotes'];
    if (json['lineItems'] != null) {
      lineItems = <LineItems>[];
      json['lineItems'].forEach((v) {
        lineItems!.add(LineItems.fromJson(v));
      });
    }
    updateSource = json['updateSource'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = aptId;
    data['invoiceNo'] = invoiceNo;
    data['notaryId'] = notaryId;
    data['issueDate'] = issueDate;
    data['dueDate'] = dueDate;
    if (receipientEmails != null) {
      data['receipientEmails'] =
          receipientEmails!.map((v) => v).toList();
    }
    data['leadId'] = leadId;
    data['serviceId'] = serviceId;
    data['companyId'] = companyId;
    data['totalCost'] = totalCost;
    data['loanNumber'] = loanNumber;
    data['invoiceNotes'] = invoiceNotes;
    if (lineItems != null) {
      data['lineItems'] = lineItems!.map((v) => v.toJson()).toList();
    }
    data['updateSource'] = updateSource;
    return data;
  }
}

class LineItems {
  String? itemName;
  String? category;
  int? cost;

  LineItems({this.itemName, this.category, this.cost});

  LineItems.fromJson(Map<String, dynamic> json) {
    itemName = json['itemName'];
    category = json['category'];
    cost = json['cost'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['itemName'] = itemName;
    data['category'] = category;
    data['cost'] = cost;
    return data;
  }
}


class EditInvoice {
  String? aptId;
  String? invoiceNo;
  String? notaryId;
  String? issueDate;
  String? dueDate;
  List<void>? receipientEmails;
  String? leadId;
  String? serviceId;
  String? companyId;
  int? totalCost;
  String? loanNumber;
  String? invoiceNotes;
  List<LineItems>? lineItems;
  String? updateSource;

  EditInvoice(
      {this.aptId,
      this.invoiceNo,
      this.notaryId,
      this.issueDate,
      this.dueDate,
      this.receipientEmails,
      this.leadId,
      this.serviceId,
      this.companyId,
      this.totalCost,
      this.loanNumber,
      this.invoiceNotes,
      this.lineItems,
      this.updateSource});

  EditInvoice.fromJson(Map<String, dynamic> json) {
    aptId = json['aptId'];
    invoiceNo = json['invoiceNo'];
    notaryId = json['notaryId'];
    issueDate = json['issueDate'];
    dueDate = json['dueDate'];
    if (json['receipientEmails'] != null) {
      receipientEmails = <Null>[];
      json['receipientEmails'].forEach((v) {
        receipientEmails!.add(v);
      });
    }
    leadId = json['leadId'];
    serviceId = json['serviceId'];
    companyId = json['companyId'];
    totalCost = json['totalCost'];
    loanNumber = json['loanNumber'];
    invoiceNotes = json['invoiceNotes'];
    if (json['lineItems'] != null) {
      lineItems = <LineItems>[];
      json['lineItems'].forEach((v) {
        lineItems!.add(LineItems.fromJson(v));
      });
    }
    updateSource = json['updateSource'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = aptId;
    data['invoiceNo'] = invoiceNo;
    data['notaryId'] = notaryId;
    data['issueDate'] = issueDate;
    data['dueDate'] = dueDate;
    if (receipientEmails != null) {
      data['receipientEmails'] =
          receipientEmails!.map((v) => v).toList();
    }
    data['leadId'] = leadId;
    data['serviceId'] = serviceId;
    data['companyId'] = companyId;
    data['totalCost'] = totalCost;
    data['loanNumber'] = loanNumber;
    data['invoiceNotes'] = invoiceNotes;
    if (lineItems != null) {
      data['lineItems'] = lineItems!.map((v) => v.toJson()).toList();
    }
    data['updateSource'] = updateSource;
    return data;
  }
}


class UpdatePayment {
  String? invoiceId;
  String? notaryId;
  String? amountPaid;
  String? paidVia;
  String? paymentUpdateSource;
  String? datePaidOn;

  UpdatePayment(
      {this.invoiceId,
      this.notaryId,
      this.amountPaid,
      this.paidVia,
      this.paymentUpdateSource,
      this.datePaidOn});

  UpdatePayment.fromJson(Map<String, dynamic> json) {
    invoiceId = json['invoiceId'];
    notaryId = json['notaryId'];
    amountPaid = json['amountPaid'];
    paidVia = json['paidVia'];
    paymentUpdateSource = json['paymentUpdateSource'];
    datePaidOn = json['datePaidOn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['invoiceId'] = invoiceId;
    data['notaryId'] = notaryId;
    data['amountPaid'] = amountPaid;
    data['paidVia'] = paidVia;
    data['paymentUpdateSource'] = paymentUpdateSource;
    data['datePaidOn'] = datePaidOn;
    return data;
  }

}



