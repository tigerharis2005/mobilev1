class LoginModel{
  final String? email;
  final String? uid;
  final String? firstName;
  final String? source;
  final String? deviceOs;
  final String? token;
  LoginModel({required this.email,required this.uid, this.firstName, this.source, this.deviceOs, this.token});
}