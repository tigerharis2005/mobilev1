class GetContacts {
  String? notaryId;

  GetContacts({this.notaryId});

  GetContacts.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    return data;
  }
}


class AddContacts {
  String? notaryId;
  String? email;
  String? imageURL;
  String? bio;
  String? firstName;
  String? lastName;
  String? type;
  String? companyId;
  int? phoneNumber;
  List<String>? labels;
  String? city;
  int? zipCode;
  String? state;
  String? source;

  AddContacts(
      {this.notaryId,
      this.email,
      this.imageURL,
      this.bio,
      this.firstName,
      this.lastName,
      this.type,
      this.companyId,
      this.phoneNumber,
      this.labels,
      this.city,
      this.zipCode,
      this.state,
      this.source});

  AddContacts.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    email = json['email'];
    imageURL = json['imageURL'];
    bio = json['bio'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    type = json['type'];
    companyId = json['companyId'];
    phoneNumber = json['phoneNumber'];
    labels = json['labels'].cast<String>();
    city = json['city'];
    zipCode = json['zipCode'];
    state = json['state'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['email'] = email;
    data['imageURL'] = imageURL;
    data['bio'] = bio;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['type'] = type;
    data['companyId'] = companyId;
    data['phoneNumber'] = phoneNumber;
    data['labels'] = labels;
    data['city'] = city;
    data['zipCode'] = zipCode;
    data['state'] = state;
    data['source'] = source;
    return data;
  }
}



class UpdateContacts {
  String? leadId;
  String? notaryId;
  EditObj? editObj;

  UpdateContacts({this.leadId, this.notaryId, this.editObj});

  UpdateContacts.fromJson(Map<String, dynamic> json) {
    leadId = json['leadId'];
    notaryId = json['notaryId'];
    editObj =
        json['editObj'] != null ? EditObj.fromJson(json['editObj']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['leadId'] = leadId;
    data['notaryId'] = notaryId;
    if (editObj != null) {
      data['editObj'] = editObj!.toJson();
    }
    return data;
  }
}

class EditObj {
  String? firstName;
  String? lastName;
  String? imageURL;
  String? email;
  int? phoneNumber;
  String? type;
  List<String>? labels;
  String? bio;
  String? city;
  String? state;
  int? zipCode;

  EditObj(
      {this.firstName,
      this.lastName,
      this.imageURL,
      this.email,
      this.phoneNumber,
      this.type,
      this.labels,
      this.bio,
      this.city,
      this.state,
      this.zipCode});

  EditObj.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    imageURL = json['imageURL'];
    email = json['email'];
    phoneNumber = json['phoneNumber'];
    type = json['type'];
    labels = json['labels'].cast<String>();
    bio = json['bio'];
    city = json['city'];
    state = json['state'];
    zipCode = json['zipCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['imageURL'] = imageURL;
    data['email'] = email;
    data['phoneNumber'] = phoneNumber;
    data['type'] = type;
    data['labels'] = labels;
    data['bio'] = bio;
    data['city'] = city;
    data['state'] = state;
    data['zipCode'] = zipCode;
    return data;
  }
}


class DeleteContacts {
  String? leadId;
  String? notaryId;

  DeleteContacts({this.leadId, this.notaryId});

  DeleteContacts.fromJson(Map<String, dynamic> json) {
    leadId = json['leadId'];
    notaryId = json['notaryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['leadId'] = leadId;
    data['notaryId'] = notaryId;
    return data;
  }
}


class GetApptByContact {
  String? leadId;

  GetApptByContact({this.leadId});

  GetApptByContact.fromJson(Map<String, dynamic> json) {
    leadId = json['leadId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['leadId'] = leadId;
    return data;
  }
}
