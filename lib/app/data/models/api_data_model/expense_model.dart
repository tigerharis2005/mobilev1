class MarkExpenseNotRequired {
  String? aptId;
  String? notaryId;
  String? updateSource;
  bool? isExpenseNotRequired;

  MarkExpenseNotRequired(
      {this.aptId,
      this.notaryId,
      this.updateSource,
      this.isExpenseNotRequired});

  MarkExpenseNotRequired.fromJson(Map<String, dynamic> json) {
    aptId = json['aptId'];
    notaryId = json['notaryId'];
    updateSource = json['updateSource'];
    isExpenseNotRequired = json['isExpenseNotRequired'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aptId'] = aptId;
    data['notaryId'] = notaryId;
    data['updateSource'] = updateSource;
    data['isExpenseNotRequired'] = isExpenseNotRequired;
    return data;
  }
}

class AddExpense {
  String? notaryId;
  String? aptId;
  List<Expenses>? expenses;

  AddExpense({this.notaryId, this.aptId, this.expenses});

  AddExpense.fromJson(Map<String, dynamic> json) {
    notaryId = json['notaryId'];
    aptId = json['aptId'];
    if (json['expenses'] != null) {
      expenses = <Expenses>[];
      json['expenses'].forEach((v) {
        expenses!.add(Expenses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['notaryId'] = notaryId;
    data['aptId'] = aptId;
    if (expenses != null) {
      data['expenses'] = expenses!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Expenses {
  String? expenseName;
  int? cost;
  String? category;
  String? description;
  String? dateAdded;
  String? receiptURL;
  String? vendorName;
  String? notes;
  bool? isIncomeCategory;

  Expenses(
      {this.expenseName,
      this.cost,
      this.category,
      this.description,
      this.dateAdded,
      this.receiptURL,
      this.vendorName,
      this.notes,
      this.isIncomeCategory});

  Expenses.fromJson(Map<String, dynamic> json) {
    expenseName = json['expenseName'];
    cost = json['cost'];
    category = json['category'];
    description = json['description'];
    dateAdded = json['dateAdded'];
    receiptURL = json['receiptURL'];
    vendorName = json['vendorName'];
    notes = json['notes'];
    isIncomeCategory = json['isIncomeCategory'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['expenseName'] = expenseName;
    data['cost'] = cost;
    data['category'] = category;
    data['description'] = description;
    data['dateAdded'] = dateAdded;
    data['receiptURL'] = receiptURL;
    data['vendorName'] = vendorName;
    data['notes'] = notes;
    data['isIncomeCategory'] = isIncomeCategory;
    return data;
  }
}
