class API{
  static String login='customer/login';
  static String getAppointmentByNotaryId='aptFillers/getAppointmentsByNotaryId';
  static String getContacts='lead/getLeads';
  static String addContacts='lead/createLead';
  static String updateContacts='lead/editLead';
  static String deleteContact='lead/deleteLead';
  static String getCompanies='company/getCompanys';
  static String addACompany='company/createCompany';
  static String editCompany='company/editCompany';
  static String getInvoicesForNotary='invoices/getInvoicesForaNotaryId';
  static String cancelInvoice='invoices/cancelInvoice';
  static String markAsUncollectable='invoices/markAsUncollectable';
  static String invoicesForContact='invoices/getInvoicesForaLead';
  static String invoicesForCompany='invoices/getInvoicesForaCompanyId';
  static String searchInvoice='invoices/searchInvoices';
  static String createInvoice='invoices/createInvoiceForApt';
  static String getApptByContacts='aptFillers/getAppointmentsByLeadId';
  static String editInvoice='invoices/editInvoice';
  static String updatePayment='invoices/updatePayment';
  static String updateMileage='aptFillers/addMileages';
  static String markMileageNotRequired='aptFillers/markMileageNotrequired';
  static String markExpenseNotRequired='aptFillers/markMileageNotrequired';
  static String addExpense='aptFillers/addExpenses';
  static String markActsNotRequired='aptFillers/markNotarialActsNotRequired';
  static String cancelAnAppointment='aptFillers/markAsCanclled';
  static String reschedule='aptFillers/reschduleAppointment';
  static String changeMeetingLink='aptFillers/changeMeetingLink';
  static String changeServiceOfApt='aptFillers/changeServiceOfApt';
  static String changeServiceCost='aptFillers/changeServiceCost';
  static String addSigner='aptFillers/addSignersToApt';
  static String removeSigner='aptFillers/removeSignerFromApt';
} 