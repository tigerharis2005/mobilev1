import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/api_data_model/mileage_model.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class MileageRepository{
  Future<RepoResponse> updateMileage(UpdateMileage data) async {
    final response =
        await NetworkRequester.shared.post(path: API.updateMileage,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> markMileageNotRequired(MarkMileageNotRequired data) async {
    final response =
        await NetworkRequester.shared.post(path: API.markMileageNotRequired,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
}