import 'package:mobilev1/app/data/models/api_data_model/get_appointment_by_notary_id_model.dart';

import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class AppointmentRepository{
  Future<RepoResponse> getAppointmentByNotaryId(GetAppointmentsByNotaryId data) async {
    final response =
        await NetworkRequester.shared.post(path: API.getAppointmentByNotaryId,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> cancelAnAppointment(CancelAnAppointment data) async {
    final response =
        await NetworkRequester.shared.post(path: API.cancelAnAppointment,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> reschedule(Reschedule data) async {
    final response =
        await NetworkRequester.shared.post(path: API.reschedule,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> changeMeetingLink(ChangeMeetingLink data) async {
    final response =
        await NetworkRequester.shared.post(path: API.changeMeetingLink,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> changeServiceOfApt(ChangeServiceOfApt data) async {
    final response =
        await NetworkRequester.shared.post(path: API.changeServiceOfApt,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> changeServiceCost(ChangeServiceCost data) async {
    final response =
        await NetworkRequester.shared.post(path: API.changeServiceCost,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> addSigner(AddSigner data) async {
    final response =
        await NetworkRequester.shared.post(path: API.addSigner,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> removeSigner(RemoveSigner data) async {
    final response =
        await NetworkRequester.shared.post(path: API.removeSigner,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }



}