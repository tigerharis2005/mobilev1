import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/api_data_model/invoices_model.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class InvoicesRepository{
  Future<RepoResponse> getInvoicesForNotary(GetInvoicesForNotary data) async {
    final response =
        await NetworkRequester.shared.post(path: API.getInvoicesForNotary,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> cancelInvoiceFlag(CancelInvoiceFlag data) async {
    final response =
        await NetworkRequester.shared.post(path: API.cancelInvoice,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> markAsUncollectable(MarkAsUncollectable data) async {
    final response =
        await NetworkRequester.shared.post(path: API.markAsUncollectable,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> invoicesForContact(InvoicesForContact data) async {
    final response =
        await NetworkRequester.shared.post(path: API.invoicesForContact,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> invoicesForCompany(InvoicesForCompany data) async {
    final response =
        await NetworkRequester.shared.post(path: API.invoicesForCompany,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> searchInvoice(SearchInvoice data) async {
    final response =
        await NetworkRequester.shared.post(path: API.searchInvoice,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> createInvoice(CreateInvoice data) async {
    final response =
        await NetworkRequester.shared.post(path: API.createInvoice,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> editInvoice(EditInvoice data) async {
    final response =
        await NetworkRequester.shared.post(path: API.editInvoice,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> updatePayment(UpdatePayment data) async {
    final response =
        await NetworkRequester.shared.post(path: API.updatePayment,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  
  
}

