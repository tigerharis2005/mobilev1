import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/api_data_model/notorial_model.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class NotarialRepository{
  Future<RepoResponse> markActsNotRequired(MarkActsNotRequired data) async {
    final response =
        await NetworkRequester.shared.post(path: API.markActsNotRequired,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  } 
  
}