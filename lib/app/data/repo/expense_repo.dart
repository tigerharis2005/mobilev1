import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/api_data_model/expense_model.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class ExpenseRepository{
  Future<RepoResponse> markExpenseNotRequired(MarkExpenseNotRequired data) async {
    final response =
        await NetworkRequester.shared.post(path: API.markExpenseNotRequired,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  } 
  Future<RepoResponse> addExpense(AddExpense data) async {
    final response =
        await NetworkRequester.shared.post(path: API.addExpense,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  } 
  
}