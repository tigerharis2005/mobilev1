import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/app/data/models/api_data_model/login.dart';

import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class LoginRepository {
  Future<RepoResponse> login(LoginModel data) async {
    final response = await NetworkRequester.shared.post(path: API.login, data: {
      "email": data.email,
      "uid": data.uid,
      // "firstName": data.firstName,
      "source": data.source, // static
      // "pushToken": {
      //     // "deviceOS": data.deviceOs,
      //     "token": data.token
      // }
    });
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      GetStorage().write('isLoggedIn', true);
      GetStorage().write('userId', response['customer']['_id']);
      GetStorage().write('name', response['customer']['fullName']);
      GetStorage().write('upcomingApts', response['data']['upcomingApts']);
      GetStorage()
          .write('availableServices', response['data']['availableServices']);
      GetStorage().write('companiesList', response['data']['companiesList']);
      GetStorage()
          .write('suggestedExpenses', response['data']['suggestedExpenses']);
      GetStorage().write('profileConfigs', response['data']['profileConfigs']);
      print('uid:   ' + GetStorage().read('userId'));
      //print('name:   '+GetStorage().read('name'));
      print('upcomingApts:   ${GetStorage().read('upcomingApts')}');
      print('availableServices:   ${GetStorage().read('availableServices')}');
      print(
          'companiesList:   ${GetStorage().read('companiesList')}');
      print('suggestedExpenses:   ${GetStorage().read('suggestedExpenses')}');
      print('profileConfigs:   ${GetStorage().read('profileConfigs')}');
      return RepoResponse(data: response);
    }
  }
}
