import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/api_data_model/company_model.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class CompanyRepository{
  Future<RepoResponse> getCompanies(GetCompanies data) async {
    final response =
        await NetworkRequester.shared.post(path: API.getCompanies,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> addACompany(AddACompany data) async {
    final response =
        await NetworkRequester.shared.post(path: API.addACompany,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> editACompany(Map<String,dynamic> data) async {
    final response =
        await NetworkRequester.shared.post(path: API.editCompany,data: data);
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  
}