import 'package:mobilev1/app/data/models/api_data_model/contacts_model.dart';

import '../api/api.dart';
import '../error_handler/error_handler.dart';
import '../models/repo_response/repo_response.dart';
import '../network/network_requester.dart';

class ContactsRepository{
  Future<RepoResponse> getContacts(GetContacts data) async {
    final response =
        await NetworkRequester.shared.post(path: API.getContacts,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> addContacts(AddContacts data) async {
    final response =
        await NetworkRequester.shared.post(path: API.addContacts,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> updateContacts(Map<String, dynamic> data) async {
    final response =
        await NetworkRequester.shared.post(path: API.updateContacts,data: data);
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> deleteContact(DeleteContacts data) async {
    final response =
        await NetworkRequester.shared.post(path: API.deleteContact,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
  Future<RepoResponse> getApptByContacts(GetApptByContact data) async {
    final response =
        await NetworkRequester.shared.post(path: API.getApptByContacts,data: data.toJson());
    if (response is APIException) {
      return RepoResponse(error: response);
    } else {
      return RepoResponse(data: response);
    }
  }
}