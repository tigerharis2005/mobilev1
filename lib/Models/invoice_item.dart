class InvoiceItem {
  String itemName;
  double price;
  double quantity;
  double total;

  InvoiceItem({
    required this.itemName,
    required this.price,
    required this.quantity,
    required this.total,
  });
}
