import 'package:get/get.dart';

import '../app/modules/App-Screens/accountpage/binding/accountpagebinding.dart';
import '../app/modules/App-Screens/accountpage/view/accountpageview.dart';
import '../app/modules/App-Screens/companypage/binding/companypagebinding.dart';
import '../app/modules/App-Screens/companypage/view/companypageview.dart';
import '../app/modules/App-Screens/contactpage/binding/contactpagebinding.dart';
import '../app/modules/App-Screens/contactpage/view/contactpageview.dart';
import '../app/modules/App-Screens/homepage/binding/homepagebinding.dart';
import '../app/modules/App-Screens/homepage/view/homepageview.dart';
import '../app/modules/App-Screens/servicespage/binding/servicespagebinding.dart';
import '../app/modules/App-Screens/servicespage/view/servicespageview.dart';
import '../app/modules/Auth-Screens/loginpage/binding/loginpagebinding.dart';
import '../app/modules/Auth-Screens/loginpage/view/loginpageview.dart';

class AppRoutes {
  static final pages = [
    GetPage(
      name: HomePage.route,
      page: () => const HomePage(),
      binding: HomePageBinding(),
    ),
    GetPage(
      name: LoginPage.route,
      page: () => const LoginPage(),
      binding: LoginPageBinding(),
    ),
    GetPage(
      name: AccountPage.route,
      page: () => const AccountPage(),
      binding: AccountPageBinding(),
    ),
    GetPage(
      name: ContactPage.route,
      page: () => const ContactPage(),
      binding: ContactPageBinding(),
    ),
    GetPage(
      name: ServicesPage.route,
      page: () => const ServicesPage(),
      binding: ServicesPageBinding(),
    ),
    GetPage(
      name: CompanyPage.route,
      page: () => const CompanyPage(),
      binding: CompanyPageBinding(),
    ),
  ];
}
