import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobilev1/services/Internet/Bindings/network_bind.dart';
import 'app/app.dart';
import 'firebase_options.dart';
import 'package:flutter/services.dart';

import 'services/notifications/controller/notification_controller.dart';

Future<void> main() async {
  await GetStorage.init();

  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await NotificationController.initialiseLocalNotification(debug: true);
  //await NotificationController.initialiseRemoteNotification(debug: true);

  NetworkBinder().dependencies();

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
  ));
  runApp(const MyApp());
}
